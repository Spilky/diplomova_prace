<?php

namespace App\Components\DataGrids\GroupsDataGrid;

use App\Components\DataGrids\AbstractDataGrid;
use App\Model\Repository\GroupRepository;
use Exception;
use Nette\Application\ForbiddenRequestException;
use Nette\Security\User as NetteUser;
use Tracy\Debugger;
use Ublaboo\DataGrid\DataGrid;

class GroupsDataGrid extends AbstractDataGrid
{
    /** @var NetteUser */
    private $netteUser;
    /** @var GroupRepository */
    private $groupRepository;

    /**
     * GroupsDataGrid constructor.
     * @param NetteUser $netteUser
     * @param GroupRepository $groupRepository
     */
    public function __construct(NetteUser $netteUser, GroupRepository $groupRepository)
    {
        $this->netteUser = $netteUser;
        $this->groupRepository = $groupRepository;
    }

    /**
     * @return DataGrid
     */
    public function createComponentDataGrid()
    {
        $grid = parent::create();
        $grid->setColumnsHideable();

        $grid->addColumnText('name', 'Název')
            ->setSortable()
            ->setFilterText();

        $grid->addColumnDateTime('dateFrom', 'Trvání od')
            ->setAlign('left')
            ->setFormat('j.n.Y')
            ->setSortable()
            ->setFilterDateRange()
            ->setAttribute('data-date-language', 'cs');;

        $grid->addColumnDateTime('dateTo', 'Trvání do')
            ->setAlign('left')
            ->setFormat('j.n.Y')
            ->setSortable()
            ->setFilterDateRange()
            ->setAttribute('data-date-language', 'cs');

        $grid->addAction('edit', 'Editovat', 'Group:edit')
            ->setIcon('pencil');

        $grid->addAction('delete', 'Smazat', 'delete!')
            ->setClass('ajax btn btn-xs btn-danger')
            ->setIcon('trash')
            ->setConfirm('Opravdu chcete smazat skupinu projektů %s?', 'name');

        $grid->addToolbarButton('Group:add', 'Přidat skupinu')
                ->setIcon('plus');

        $grid->setDataSource($this->groupRepository->getQB('gr')
            ->where('gr.supervisor = :supervisor')
            ->setParameter('supervisor', $this->netteUser->id));

        return $grid;
    }

    /**
     * @param int $id
     */
    public function handleDelete($id)
    {
        $group = $this->groupRepository->getById($id);

        if ($group) {
            if ($this->netteUser->id != $group->getSupervisor()->getId()) {
                new ForbiddenRequestException();
            }

            try {
                $this->groupRepository->delete($group);
                $this->flashMessage('Skupina projektů byla úspěšně smazána.', 'success');
            } catch (Exception $e) {
                Debugger::log($e);
                $this->flashMessage('Skupina projektů se nepodařilo smazat. Zkuste to prosím znovu.', 'danger');
            }

            $this->redrawControl('flashes');
            $this['dataGrid']->reload();
        }
    }
}