<?php

namespace App\Model\Repository;

use App\Model\Entity\MailTemplate;
use Kdyby\Doctrine\EntityManager;

class MailTemplateRepository extends AbstractRepository
{
    /**
     * MailTemplateRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entityRepository = $this->entityManager->getRepository(MailTemplate::class);
    }

    /**
     * @return MailTemplate[]
     */
    public function getAll()
    {
        return parent::getAll();
    }

    /**
     * @param $id
     * @return MailTemplate
     */
    public function getById($id)
    {
        return parent::getById($id);
    }

    /**
     * @param $parameters
     * @return MailTemplate[]
     */
    public function getByParameters($parameters)
    {
        return parent::getByParameters($parameters);
    }

    /**
     * @param $parameters
     * @return MailTemplate
     */
    public function getOneByParameters($parameters)
    {
        return parent::getOneByParameters($parameters);
    }
}