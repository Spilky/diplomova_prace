<?php

namespace App\Presenters;

use App\Components\Forms\ForgottenPasswordForm\Factories\IForgottenPasswordFormFactory;
use App\Components\Forms\ForgottenPasswordForm\ForgottenPasswordForm;
use App\Components\Forms\SetNewPasswordForm\Factories\ISetNewPasswordFormFactory;
use App\Components\Forms\SetNewPasswordForm\SetNewPasswordForm;
use App\Components\Forms\SignInForm\Factories\ISignInFormFactory;
use App\Components\Forms\SignInForm\SignInForm;
use App\Model\Entity\Token;
use App\Model\Service\TokenService;
use Exception;

class SignPresenter extends BasePresenter
{
    /** @var ISignInFormFactory @inject */
    public $signInFormFactory;
    /** @var IForgottenPasswordFormFactory @inject */
    public $forgottenPasswordFormFactory;
    /** @var ISetNewPasswordFormFactory @inject */
    public $setNewPasswordFormFactory;
    /** @var TokenService @inject */
    public $tokenService;

    /** @var null|string */
    private $returnUrl = null;

    /**
     * @param string $returnUrl
     */
    public function actionIn($returnUrl)
    {
        $this->returnUrl = $returnUrl;
    }
    
    public function actionOut()
    {
        $this->getUser()->logout();
        if (!is_null($this->getParameter('backlink'))) {
            $this->redirectUrl($this->getParameter('backlink'));
        } else {
            $this->redirect('Homepage:');
        }
    }

    /**
     * @param null|string $token
     */
    public function actionForgottenPassword($token = null)
    {
        if (!is_null($token)) {
            switch ($this->tokenService->checkTokenValid(
                $this->getParameter('token'),
                Token::TYPE_FORGOTTEN_PASSWORD
            )) {
                case TokenService::TOKEN_EXPIRED:
                    $this->flashMessage('Odkaz pro obnovu hesla je již neplatný, vygenerute si prosím nový.', 'warning');
                    $this->redirect('this');
                    break;
                case TokenService::TOKEN_INVALID:
                    $this->flashMessage('Odkaz pro obnovu hesla je neplatný, vygenerute si prosím nový.', 'danger');
                    $this->redirect('this');
                    break;
                case TokenService::TOKEN_VALID:
                    $this->template->setNewPassword = true;
            }

            $this['setNewPasswordForm']->setDefaults(['token' => $token]);
        }
    }

    /**
     * @return SignInForm
     */
    protected function createComponentSignInForm()
    {
        $form = $this->signInFormFactory->create();
        $form->onSuccess[] = function ($control, $message) {
            $this->flashMessage($message, 'success');
            if (!empty($this->returnUrl)) {
                $this->redirectUrl($this->returnUrl);
            } else {
                $this->redirect('Homepage:');
            }
        };
        $form->onError[] = function ($control, $message) {
            $this->flashMessage($message, 'danger');
        };
        return $form;
    }

    /**
     * @return ForgottenPasswordForm
     */
    protected function createComponentForgottenPasswordForm()
    {
        $form = $this->forgottenPasswordFormFactory->create();
        $form->onSuccess[] = function ($control) {
            $this->flashMessage('Odkaz pro obnovu hesla úspěšně odeslán.', 'success');
            $this->redirect('this');
        };
        $form->onError[] = function ($control, $message) {
            if ($message instanceof Exception) {
                $message = 'Nepodařilo se odeslat odkaz k obnovení. Zkuste to prosím znovu.';
            }
            $this->flashMessage($message, 'danger');
        };
        return $form;
    }

    /**
     * @return SetNewPasswordForm
     */
    protected function createComponentSetNewPasswordForm()
    {
        $form = $this->setNewPasswordFormFactory->create();
        $form->onSuccess[] = function ($control) {
            $this->flashMessage('Heslo bylo úspěšně změněno. Můžete se přihlásit.', 'success');
            $this->redirect('in');
        };
        $form->onError[] = function ($control, $message) {
            if ($message instanceof Exception) {
                $message = 'Nepodařilo se změnit heslo. Zkuste to prosím znovu.';
            }
            $this->flashMessage($message, 'danger');
        };
        return $form;
    }
}
