<?php

namespace App\Components\Forms\ReportProblemForm\Factories;

use App\Components\Forms\ReportProblemForm\ReportProblemForm;

interface IReportProblemFormFactory
{
    /**
     * @return ReportProblemForm
     */
    public function create();
}
