<?php

namespace App\Components\Forms\AddEditUserForm;

use App\Components\Forms\AbstractForm;
use App\Model\Entity\User;
use App\Model\Mail\AddUserMail;
use App\Model\Repository\UserRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Nette\Application\ForbiddenRequestException;
use Nette\Application\UI\Form;
use Nette\Security\Passwords;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;
use Ublaboo\Mailing\MailFactory;

class AddEditUserForm extends AbstractForm
{
    /** @var MailFactory */
    private $mailFactory;
    /** @var NetteUser */
    private $netteUser;
    /** @var User */
    private $user;
    /** @var UserRepository */
    private $userRepository;

    /**
     * AddEditUserForm constructor.
     * @param MailFactory $mailFactory
     * @param NetteUser $netteGroup
     * @param UserRepository $userRepository
     */
    public function __construct(MailFactory $mailFactory, NetteUser $netteGroup, UserRepository $userRepository)
    {
        $this->mailFactory = $mailFactory;
        $this->netteUser = $netteGroup;
        $this->userRepository = $userRepository;
    }

    /**
     * @param int|User $user
     */
    public function setUser($user)
    {
        if (!($user instanceof User)) {
            $this->user = $this->userRepository->getById($user);
        } else {
            $this->user = $user;
        }
    }

    public function render()
    {
        if ($this->user) {
            $data = $this->user->getAsArray();
            unset($data['password']);
            $this->setDefaults($data);
        }

        parent::render();
    }

    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form;
        
        $form->addText('name', 'Jméno:')
            ->setRequired('Vložte prosím jméno.');

        $form->addText('email', 'E-mail:')
            ->setRequired('Vlože prosím e-mail.')
            ->addRule(Form::EMAIL, 'Vložte prosím validní e-mail.');

        $roles = UserRepository::getRoles();
        $roleInput = $form->addSelect('role', 'Role:', $roles)
            ->setRequired('Vyberte prosím roli uživatele.');

        if ($this->user) {
            $roleInput->setDisabled();
        }

        if ($this->user) {
            $form->addPassword('password', 'Současné heslo');

            $form->addPassword('newPassword', 'Nové heslo')
                ->addConditionOn($form['password'], Form::FILLED)
                ->addRule(Form::MIN_LENGTH, 'Heslo musí být minimálně 6 znaků dlouhé.', 6)
                ->setRequired('Vložte prosím nové heslo.');

            $form->addPassword('newPasswordAgain', 'Nové heslo znovu')
                ->addConditionOn($form['password'], Form::FILLED)
                ->setRequired('Vložte prosím nové heslo ještě jednou pro kontrolu.')
                ->addRule(Form::EQUAL, 'Nová hesla se neshodují, vložte prosím stejná hesla.', $form['newPassword']);
        } else {
            $form->addPassword('password', 'Heslo')
                ->addRule(Form::MIN_LENGTH, 'Heslo musí být minimálně 6 znaků dlouhé.', 6)
                ->setRequired('Vložte prosím heslo.');

            $form->addPassword('passwordAgain', 'Heslo znovu')
                ->setRequired('Vložte prosím heslo ještě jednou')
                ->addRule(Form::EQUAL, 'Hesla se neshodují, vložte prosím stejná hesla.', $form['password']);

            $form->addCheckbox('sendPassword', 'Poslat heslo?');
        }

        $form->addHidden('id');

        $form->addSubmit('submit', 'Uložit');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        if ($this->netteUser->id != $values['id'] && !$this->netteUser->isInRole(User::ROLE_ADMIN)) {
            new ForbiddenRequestException();
        }

        $originalPassword = $values['password'];
        if (isset($values['sendPassword'])) {
            $sendPassword = $values['sendPassword'];
        }

        if (empty($values['id'])) {
            $values['password'] = Passwords::hash($originalPassword);
            unset($values['originalPassword'], $values['sendPassword']);
            $user = $this->userRepository->getOneByParameters(['email' => $values['email'], 'role' => User::ROLE_UNREGISTERED]);
            if ($user) {
                $user->setName($values['name']);
                $user->setRole($values['role']);
                $user->setPassword($values['password']);
            } else {
                $user = new User($values);
            }
        } else {
            $user = $this->userRepository->getById($values['id']);
            if (!empty($values['password'])) {
                if (!Passwords::verify($values['password'], $user->getPassword())) {
                    unset($values['password']);
                    $this->flashMessage('Zadané současné heslo nebylo správné, heslo tedy nebylo změněno.', 'warning');
                } else {
                    $values['password'] = Passwords::hash($values['newPassword']);
                }
            } else {
                unset($values['password']);
            }
            unset($values['newPassword'], $values['newPasswordAgain']);
        }

        try {
            if (empty($values['id'])) {
                if ($user->getId()) {
                    $this->userRepository->update($user);
                } else {
                    $this->userRepository->insert($user);
                }
                $params = ['user' => $user];
                if (isset($sendPassword) && $sendPassword) {
                    $params['password'] = $originalPassword;
                }
                $this->mailFactory->createByType(AddUserMail::class, $params)->send();
            } else {
                $this->userRepository->updateWhere($values, ['id' => $values['id']]);
                if ($values['id'] == $this->netteUser->id) {
                    $this->netteUser->identity->name = $values['name'];
                    $this->netteUser->identity->email = $values['email'];
                }
            }
        } catch (UniqueConstraintViolationException $e) {
            $this->onError($this, $e);
            return;
        } catch (\Exception $e) {
            $this->onError($this, $e);
            Debugger::log($e);
            return;
        }

        $this->onSuccess($this);
    }
}
