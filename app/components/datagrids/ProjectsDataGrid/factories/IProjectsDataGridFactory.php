<?php

namespace App\Components\DataGrids\ProjectsDataGrid\Factories;

use App\Components\DataGrids\ProjectsDataGrid\ProjectsDataGrid;

interface IProjectsDataGridFactory
{
    /**
     * @return ProjectsDataGrid
     */
    public function create();
}
