<?php

namespace App\Presenters;

use App\Components\DataGrids\ConsultationsDataGrid\ConsultationsDataGrid;
use App\Components\DataGrids\ConsultationsDataGrid\Factories\IConsultationsDataGridFactory;
use App\Components\Forms\AddEditConsultationForm\AddEditConsultationForm;
use App\Components\Forms\AddEditConsultationForm\Factories\IAddEditConsultationFormFactory;
use App\Components\Forms\ConsultationSettingsForm\ConsultationSettingsForm;
use App\Components\Forms\ConsultationSettingsForm\Factories\IConsultationSettingsFormFactory;
use App\Model\Entity\ConsultationRequest;
use App\Model\Entity\TimeLineItem;
use App\Model\Entity\Token;
use App\Model\Entity\User;
use App\Model\Repository\ConsultationRepository;
use App\Model\Repository\ConsultationRequestRepository;
use App\Model\Repository\GroupRepository;
use App\Model\Repository\TimeLineItemRepository;
use App\Model\Repository\UserRepository;
use App\Model\Service\TokenService;
use Exception;
use Nette\Application\BadRequestException;
use Nette\Application\ForbiddenRequestException;
use Tracy\Debugger;

class ConsultationPresenter extends BasePresenter
{
    /** @var ConsultationRepository @inject */
    public $consultationRepository;
    /** @var ConsultationRequestRepository @inject */
    public $consultationRequestRepository;
    /** @var GroupRepository @inject */
    public $groupRepository;
    /** @var TimeLineItemRepository @inject */
    public $timeLineItemRepository;
    /** @var UserRepository @inject */
    public $userRepository;
    /** @var TokenService @inject */
    public $tokenService;
    /** @var IAddEditConsultationFormFactory @inject */
    public $addEditConsultationFormFactory;
    /** @var IConsultationSettingsFormFactory @inject */
    public $consultationSettingsFormFactory;
    /** @var IConsultationsDataGridFactory @inject */
    public $consultationsDataGridFactory;

    /** @var null|int @persistent */
    private $supervisorId = null;

    /**
     * @param int $supervisorId
     * @throws BadRequestException
     */
    public function actionDefault($supervisorId)
    {
        $this->supervisorId = $supervisorId;
        if (empty($this->supervisorId) && !empty($this->user->identity->group)) {
            $group = $this->groupRepository->getById($this->user->identity->group);
            $this->supervisorId = $group->getSupervisor()->getId();
        } elseif (empty($this->supervisorId) && !$this->user->isInRole(User::ROLE_STUDENT)) {
            $this->supervisorId = $this->user->id;
        } elseif ($this->supervisorId) {
            $supervisor = $this->userRepository->getById($this->supervisorId);
            if (!$supervisor || $supervisor->getRole() == User::ROLE_STUDENT) {
                throw new BadRequestException();
            }
        }
    }

    /**
     * @param int $supervisorId
     * @param string $token
     * @param null|string $login
     * @param null|string $logout
     * @throws BadRequestException
     */
    public function actionPublic($supervisorId, $token, $login = null, $logout = null)
    {
        $this->supervisorId = $supervisorId;
        if (sha1('vedouci-' . $supervisorId) != $token || $this->user->isLoggedIn()) {
            $this->redirect('default');
        }

        $supervisor = $this->userRepository->getById($this->supervisorId);
        if (!$supervisor || $supervisor->getRole() == User::ROLE_STUDENT) {
            throw new BadRequestException();
        }
        $this->template->supervisor = $supervisor;

        if (!empty($login)) {
            switch ($this->tokenService->checkTokenValid(
                $login,
                Token::TYPE_CONSULTATION_LOGIN
            )) {
                case TokenService::TOKEN_EXPIRED:
                    $this->flashMessage('Odkaz pro přihlášení na konzultaci je již neplatný, vygenerute si prosím nový.', 'warning');
                    break;
                case TokenService::TOKEN_INVALID:
                    $this->flashMessage('Odkaz pro přihlášení na konzultaci je neplatný, vygenerute si prosím nový.', 'danger');
                    break;
                case TokenService::TOKEN_VALID:
                    $request = $this->consultationRequestRepository->getByTokenAndType($login, ConsultationRequest::TYPE_LOGIN);
                    if (!$request) {
                        $this->flashMessage('Odkaz pro přihlášení na konzultaci je neplatný, vygenerute si prosím nový.', 'danger');
                        break;
                    }
                    $user = $this->userRepository->getOneByParameters(['email' => $request->getEmail()]);
                    if (!$user) {
                        $user = new User([
                            'name' => $request->getName(),
                            'email' => $request->getEmail(),
                            'role' => User::ROLE_UNREGISTERED
                        ]);
                    }

                    if ($request->getConsultation()->getStudent()) {
                        $this->flashMessage('Přihlášení na konzultaci nemohlo být dokončeno, protože vybraný blok již není volný.', 'warning');
                        break;
                    }

                    $request->getConsultation()->setStudent($user);

                    try {
                        if (!$user->getId()) {
                            $this->userRepository->insert($user);
                        }
                        $this->consultationRepository->update($request->getConsultation());
                    } catch (Exception $e) {
                        Debugger::log($e);
                        $this->flashMessage('Nepodařilo se dokončit přihlášení na konzultaci. Zkuste to znovu prosím.', 'danger');
                        break;
                    }

                    try {
                        $this->tokenService->useToken($login);
                    } catch (Exception $e) {
                        Debugger::log($e);
                    }

                    $this->flashMessage('Přihlášení na konzultaci bylo úspěšně dokončeno.', 'success');
            }

            $this->redirect('this', ['login' => null]);
        }

        if (!empty($logout)) {
            switch ($this->tokenService->checkTokenValid(
                $logout,
                Token::TYPE_CONSULTATION_LOGOUT
            )) {
                case TokenService::TOKEN_EXPIRED:
                    $this->flashMessage('Odkaz pro odhlášení z konzultace je již neplatný, vygenerute si prosím nový.', 'warning');
                    break;
                case TokenService::TOKEN_INVALID:
                    $this->flashMessage('Odkaz pro odhlášení z konzultace je neplatný, vygenerute si prosím nový.', 'danger');
                    break;
                case TokenService::TOKEN_VALID:
                    $request = $this->consultationRequestRepository->getByTokenAndType($logout, ConsultationRequest::TYPE_LOGOUT);
                    if (!$request) {
                        $this->flashMessage('Odkaz pro odhlášení z konzultace je neplatný, vygenerute si prosím nový.', 'danger');
                        break;
                    }

                    if ($request->getConsultation()->getStudent()) {
                        if ($request->getConsultation()->getStudent()->getEmail() != $request->getEmail()) {
                            $this->flashMessage('Bohužel, odhlášení z konzultace nebylo povoleno, protože zadaná e-mailová adresa se neshoduje s e-mailovou adresou přihlášeného.', 'danger');
                            break;
                        }
                    }

                    $request->getConsultation()->setStudent(null);

                    try {
                        $this->consultationRepository->update($request->getConsultation());
                    } catch (Exception $e) {
                        Debugger::log($e);
                        $this->flashMessage('Nepodařilo se dokončit odhlášení z konzultace. Zkuste to znovu prosím.', 'danger');
                        break;
                    }

                    try {
                        $this->tokenService->useToken($logout);
                    } catch (Exception $e) {
                        Debugger::log($e);
                    }

                    $this->flashMessage('Odhlášení z konzultace bylo úspěšně dokončeno.', 'success');
            }

            $this->redirect('this', ['logout' => null]);
        }
    }

    /**
     * @param int $projectId
     * @throws BadRequestException
     */
    public function actionAdd($projectId = 0)
    {
        $project = $this->projectRepository->getById($projectId);
        if (is_null($project)) {
            throw new BadRequestException;
        }

        $this['addEditConsultationForm']->setDefaults(['projectId' => $project->getId()]);
    }

    /**
     * @param int $id
     * @param int $projectId
     * @throws BadRequestException
     */
    public function actionEdit($id = 0, $projectId = 0)
    {
        $timeLineItem = $this->timeLineItemRepository->getById($id);
        $project = $this->projectRepository->getById($projectId);
        if (is_null($project) || is_null($timeLineItem) || $timeLineItem->getType() != TimeLineItem::TYPE_CONSULTATION) {
            throw new BadRequestException;
        }

        $this['addEditConsultationForm']->setTimeLineItem($timeLineItem);
    }

    /**
     * @param int $id
     * @param int $projectId
     * @throws BadRequestException
     */
    public function actionDelete($id = 0, $projectId = 0)
    {
        $timeLineItem = $this->timeLineItemRepository->getById($id);
        $project = $this->projectRepository->getById($projectId);
        if (is_null($project) || is_null($timeLineItem) || $timeLineItem->getType() != TimeLineItem::TYPE_CONSULTATION) {
            throw new BadRequestException;
        }

        $group = $project->getGroup();
        if ($project->getId() != $timeLineItem->getProject()->getId() ||
            ($this->user->isInRole(User::ROLE_STUDENT) && $project->getStudent()->getId() != $this->user->id) ||
            ($this->user->isInRole(User::ROLE_SUPERVISOR) && $group->getSupervisor()->getId() != $this->user->id)) {
            throw new ForbiddenRequestException;
        }

        try {
            $this->timeLineItemRepository->delete($timeLineItem);
        } catch (Exception $e) {
            Debugger::log($e);
            $this->flashMessage('Konzultaci se nepodařilo smazat. Zkuste to znovu prosím.', 'danger');
            return;
        }

        $this->flashMessage('Konzultace byla úspěšně smazána.', 'success');
        $this->redirect('Project:detail', ['id' => $project->getId()]);
    }

    public function renderDefault()
    {
        $parameters = ['role !=' => User::ROLE_STUDENT];
        $this->template->mySupervisor = null;
        if (!empty($this->user->identity->group)) {
            $group = $this->groupRepository->getById($this->user->identity->group);
            $this->template->mySupervisor = $group->getSupervisor();
            $parameters['id !='] = $group->getSupervisor()->getId();
        } elseif (!$this->user->isInRole(User::ROLE_STUDENT)) {
            $this->template->mySupervisor = $this->userRepository->getById($this->user->id);
            $parameters['id !='] = $this->user->id;
        }
        $supervisors = $this->userRepository->getByParameters($parameters);
        $this->template->supervisors = [];
        $this->template->selectedSupervisor = null;
        if ($this->supervisorId) {
            $this->template->selectedSupervisor = $this->userRepository->getById($this->supervisorId);
        }
        foreach ($supervisors as $key => $supervisor) {
            $this->template->supervisors[$supervisor->getName()] = $supervisor;
        }
        ksort($this->template->supervisors);
    }

    /**
     * @return AddEditConsultationForm
     */
    protected function createComponentAddEditConsultationForm()
    {
        $project = $this->projectRepository->getById($this->getParameter('projectId'));
        $form = $this->addEditConsultationFormFactory->create($project);
        $form->setTemplateFile('box');
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'Konzultace byla úspěšně ' .
                ($this->getAction() == 'add' ?  'vložena.' : 'editována.'), 'success');
            $this->redirect('Project:detail', ['id' => $this->getParameter('projectId')]);
        };
        $form->onError[] = function ($form, $error) {
            $this->flashMessage(
                'Konzultaci se nepodařilo ' .
                ($this->getAction() == 'add' ?  'vložit.' : 'editovat.') .
                ' Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }

    /**
     * @return ConsultationSettingsForm
     */
    protected function createComponentConsultationSettingsForm()
    {
        $form = $this->consultationSettingsFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage('Konzultační bloky byly úspěšně vytvořeny.', 'success');
            $this->redirect('Consultation:');
        };
        $form->onError[] = function ($form, $error) {
            $this->flashMessage('Konzultační bloky se nepodařilo vytvořit. Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }

    /**
     * @return ConsultationsDataGrid
     */
    protected function createComponentConsultationsDataGrid()
    {
        $control = $this->consultationsDataGridFactory->create($this->supervisorId);
        return $control;
    }
}
