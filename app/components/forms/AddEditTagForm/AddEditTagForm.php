<?php

namespace App\Components\Forms\AddEditTagForm;

use App\Components\Forms\AbstractForm;
use App\Model\Entity\Tag;
use App\Model\Repository\GroupRepository;
use App\Model\Repository\TagRepository;
use Exception;
use Nette\Application\UI\Form;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class AddEditTagForm extends AbstractForm
{
    /** @var NetteUser */
    private $netteUser;
    /** @var GroupRepository */
    private $groupRepository;
    /** @var TagRepository */
    private $tagRepository;
    /** @var Tag */
    private $tag = null;

    /**
     * AddEditTagForm constructor.
     * @param NetteUser $netteUser
     * @param GroupRepository $groupRepository
     * @param TagRepository $tagRepository
     */
    public function __construct(
        NetteUser $netteUser,
        GroupRepository $groupRepository,
        TagRepository $tagRepository
    ) {
        $this->netteUser = $netteUser;
        $this->groupRepository = $groupRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * @param int|Tag $tag
     */
    public function setTag($tag)
    {
        if (!($tag instanceof Tag)) {
            $tag = $this->tagRepository->getById($tag);
        }

        $this->tag = $tag;
    }

    public function render()
    {
        if ($this->tag) {
            $data = $this->tag->getAsArray();
            $this['form']->setDefaults($data);
        }

        parent::render();
    }

    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form;

        $form->addText('name', 'Název')
            ->setRequired('Vložte prosím jméno.');

        $form->addHidden('id');

        $form->addSubmit('submit', 'Uložit');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        if (empty($values['id'])) {
            $values['group'] = $this->groupRepository->getById($this->netteUser->identity->group);
            $tag = new Tag($values);
        }

        try {
            if (empty($values['id'])) {
                $this->tagRepository->insert($tag);
            } else {
                $this->tagRepository->updateWhere($values, ['id' => $values['id']]);
            }
        } catch (Exception $e) {
            $this->onError($this, $e);
            Debugger::log($e);
            return;
        }

        $this->onSuccess($this);
    }
}
