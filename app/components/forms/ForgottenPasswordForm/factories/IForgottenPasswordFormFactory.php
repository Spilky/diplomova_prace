<?php

namespace App\Components\Forms\ForgottenPasswordForm\Factories;

use App\Components\Forms\ForgottenPasswordForm\ForgottenPasswordForm;

interface IForgottenPasswordFormFactory
{
    /**
     * @return ForgottenPasswordForm
     */
    public function create();
}
