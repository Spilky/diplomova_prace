<?php

namespace App\Components\Forms\AddEditUserForm\Factories;

use App\Components\Forms\AddEditUserForm\AddEditUserForm;

interface IAddEditUserFormFactory
{
    /**
     * @return AddEditUserForm
     */
    public function create();
}
