<?php

namespace App\Components\Forms\AddEditProjectForm\Factories;

use App\Components\Forms\AddEditProjectForm\ImportProjectsForm;

interface IImportProjectsFormFactory
{
    /**
     * @return ImportProjectsForm
     */
    public function create();
}
