<?php

namespace App\Components\Forms\AddEditProjectForm;

use App\Components\Forms\AbstractForm;
use App\Model\Entity\User;
use App\Model\Repository\MailTemplateRepository;
use App\Model\Repository\UserRepository;
use Exception;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class CopyMailTemplatesForm extends AbstractForm
{
    /** @var NetteUser */
    private $netteUser;
    /** @var MailTemplateRepository */
    private $mailTemplateRepository;
    /** @var UserRepository */
    private $userRepository;

    /**
     * AddEditProjectForm constructor.
     * @param NetteUser $netteUser
     * @param MailTemplateRepository $mailTemplateRepository
     * @param UserRepository $userRepository
     */
    public function __construct(
        NetteUser $netteUser,
        MailTemplateRepository $mailTemplateRepository,
        UserRepository $userRepository
    ) {
        $this->netteUser = $netteUser;
        $this->mailTemplateRepository = $mailTemplateRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @return Form
     */
    public function createComponentStep1Form()
    {
        $form = new Form;

        $items = [];
        $supervisors = $this->userRepository->getByParameters(['role !=' => User::ROLE_STUDENT, 'id !=' => $this->netteUser->id]);
        foreach ($supervisors as $supervisor) {
            $items[$supervisor->getName() . '|' . $supervisor->getId()] = $supervisor->getName();
        }
        ksort($items);
        $options = [];
        foreach ($items as $key => $option) {
            $explode = explode('|', $key);
            $id = $explode[count($explode)-1];
            $options[$id] = $option;
        }
        $form->addSelect('supervisor', 'Jméno vedoucího:', ['' => ''] + $options)
            ->setRequired('Vyberte prosím projektovou skupinu');

        $form->addSubmit('submit', 'Zobrazit e-mailové šablony');

        $form->onSuccess[] = array($this, 'step1FormSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function step1FormSucceeded(Form $form, ArrayHash $values)
    {
        $templates = [];
        $replicator = [];
        foreach ($this->mailTemplateRepository->getByParameters(['user' => $values['supervisor'], 'public' => true]) as $template) {
            $templates[$template->getId()] = $template->getAsArray();
            $replicator[$template->getId()] = [
                'copy' => true
            ];
        }

        $this['step2Form']['templates']->setValues($replicator);
        $this->template->templates = $templates;
        $this->redrawControl('formContent');
    }

    /**
     * @return Form
     */
    public function createComponentStep2Form()
    {
        $form = new Form();

        $form->addDynamic('templates', function (Container $container) {
            $container->addCheckbox('copy');
        });

        $form->addSubmit('submit', 'Kopírovat šablony');

        $form->onSuccess[] = array($this, 'step2FormSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function step2FormSucceeded(Form $form, ArrayHash $values)
    {
        $templates = [];
        foreach ($values['templates'] as $id => $copy) {
            if ($copy) {
                $template = $this->mailTemplateRepository->getById($id);
                if ($template->getUser()->getRole() != User::ROLE_STUDENT && $template->getPublic()) {
                    $newTemplate = clone $template;
                    $newTemplate->setUser($this->userRepository->getById($this->netteUser->id));
                    $templates[] = $newTemplate;
                }
            }
        }

        try {
            $this->mailTemplateRepository->insert($templates);
        } catch (Exception $e) {
            $this->onError($this, $e);
            Debugger::log($e);
            return;
        }

        $this->onSuccess($this);
    }

    public function handleBack()
    {
        $this->redrawControl('formContent');
    }
}
