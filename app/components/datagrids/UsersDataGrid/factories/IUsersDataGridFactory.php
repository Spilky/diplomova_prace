<?php

namespace App\Components\DataGrids\UsersDataGrid\Factories;

use App\Components\DataGrids\UsersDataGrid\UsersDataGrid;

interface IUsersDataGridFactory
{
    /**
     * @return UsersDataGrid
     */
    public function create();
}
