$(function(){
    $.nette.init();

    $.nette.ext('redraw', {
        complete: function(jqXHR, status, settings) {
            $.each(jqXHR.snippets, function(id, v) {
                initLibraries('#' + id);
            });
        }
    });

    $.nette.ext('spinner', {
        before: function (jqXHR, settings) {
            if (!settings.nette) {
                return;
            }

            var where = settings.nette.el.data('show-spinner');
            if (where) {
                this.spinner = this.createSpinner();
                this.spinner.appendTo(where);
                this.spinner.show(this.speed);
            }
        },
        complete: function () {
            if (this.spinner !== null) {
                this.spinner.remove();
                this.spinner = null;
            }
        }
    }, {
        createSpinner: function () {
            var spinner = $('<div>', {
                class: 'overlay'
            });
            var icon = $('<i>', {
                class: 'fa fa-refresh fa-spin'
            });
            spinner.append(icon);
            return spinner;
        },
        spinner: null,
        speed: undefined
    });

    $(document).off('click', '.sidebar li a');

    $($.AdminLTE.options.sidebarToggleSelector).on('click', function () {
        $.nette.ajax({
            type: 'POST',
            url: $(this).data('href')
        });
    });

    initLibraries('body');
});

function initLibraries(selector) {
    $(selector + ' .datepicker').datetimepicker({
        locale: 'cs',
        format: 'D.M.YYYY'
    });

    $(selector + ' .timepicker').datetimepicker({
        locale: 'cs',
        format: 'HH:mm'
    });

    $(selector + ' .datepicker.datepickerto').datetimepicker({
        useCurrent: false,
        locale: 'cs',
        format: 'D.M.YYYY'
    });

    $(selector + ' .datepicker.datepickerfrom').on('dp.change', function (e) {
        var to = $(this).data('date-to');
        $(to).data('DateTimePicker').minDate(e.date);
    });

    $(selector + ' .datepicker.datepickerto').on('dp.change', function (e) {
        var from = $(this).data('date-from');
        $(from).data('DateTimePicker').maxDate(e.date);
    });

    $(selector + ' .ckeditor').each(function() {
        $(this).ckeditor({
            language: 'cs',
            skin: 'bootstrapck,/bower-plugins/bootstrapck4-skin/skins/bootstrapck/'
        });
    });

    $(selector + ' .select2').select2();

    $(selector + ' .bootstrap-slider').slider({
        formatter: function(value) {
            return value + '%';
        }
    });
}
