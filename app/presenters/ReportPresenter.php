<?php

namespace App\Presenters;

use App\Components\Forms\AddEditReportForm\AddEditReportForm;
use App\Components\Forms\AddEditReportForm\Factories\IAddEditReportFormFactory;
use App\Model\Entity\Project;
use App\Model\Entity\Report;
use App\Model\Entity\User;
use App\Model\Repository\ReportRepository;
use DateTime;
use Exception;
use Nette\Application\BadRequestException;
use Nette\Application\ForbiddenRequestException;
use Tracy\Debugger;

class ReportPresenter extends BasePresenter
{
    /** @var ReportRepository @inject */
    public $reportRepository;
    /** @var IAddEditReportFormFactory @inject */
    public $addEditReportFormFactory;

    /**
     * @param Project $project
     * @param null|Report $report
     * @throws ForbiddenRequestException
     */
    private function checkRights($project, $report = null)
    {
        $group = $project->getGroup();
        if ((!is_null($report) && $project->getId() != $report->getProject()->getId()) ||
            ($this->user->isInRole(User::ROLE_STUDENT) && $project->getStudent()->getId() != $this->user->id) ||
            ($this->user->isInRole(User::ROLE_SUPERVISOR) && $group->getSupervisor()->getId() != $this->user->id)) {
            throw new ForbiddenRequestException;
        }
    }

    /**
     * @param int $projectId
     * @throws BadRequestException
     */
    public function actionAdd($projectId = 0)
    {
        $project = $this->projectRepository->getById($projectId);
        if (is_null($project)) {
            throw new BadRequestException;
        }

        $this->checkRights($project);

        if ($this->getParameter('date')) {
            if ($date = new DateTime($this->getParameter('date'))) {
                $this['addEditReportForm']->setDefaults(['date' => $date->format('Y-m-d')]);
            }
        }
    }

    /**
     * @param int $id
     * @param int $projectId
     * @throws BadRequestException
     */
    public function actionEdit($id = 0, $projectId = 0)
    {
        $report = $this->reportRepository->getById($id);
        $project = $this->projectRepository->getById($projectId);
        if (is_null($project) || is_null($report)) {
            throw new BadRequestException;
        }

        $this->checkRights($project, $report);

        $this['addEditReportForm']->setReport($report);
    }

    /**
     * @param int $id
     * @param int $projectId
     * @throws BadRequestException
     */
    public function actionDelete($id = 0, $projectId = 0)
    {
        $report = $this->reportRepository->getById($id);
        $project = $this->projectRepository->getById($projectId);
        if (is_null($project) || is_null($report)) {
            throw new BadRequestException;
        }

        $this->checkRights($project, $report);

        try {
            $this->reportRepository->delete($report);
        } catch (Exception $e) {
            Debugger::log($e);
            $this->flashMessage('Report se nepodařilo smazat. Zkuste to znovu prosím.', 'danger');
            return;
        }

        $this->flashMessage('Report byl úspěšně smazán.', 'success');
        $this->redirect('Project:detail', ['id' => $project->getId()]);
    }

    /**
     * @return AddEditReportForm
     */
    protected function createComponentAddEditReportForm()
    {
        $project = $this->projectRepository->getById($this->getParameter('projectId'));
        $form = $this->addEditReportFormFactory->create($project);
        $form->setTemplateFile('box');
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'Report byl úspěšně ' .
                ($this->getAction() == 'add' ?  'vložen.' : 'editován.'), 'success');
            $this->redirect('Project:detail', ['id' => $this->getParameter('projectId')]);
        };
        $form->onError[] = function ($form, $error) {
            $this->flashMessage(
                'Report se nepodařilo ' .
                ($this->getAction() == 'add' ?  'vložit.' : 'editovat.') .
                ' Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }
}
