<?php

namespace App\Presenters;

use App\Components\DataGrids\MailingDataGrid\MailingDataGrid;
use App\Components\DataGrids\MailingDataGrid\Factories\IMailingDataGridFactory;
use App\Components\DataGrids\MailTemplatesDataGrid\Factories\IMailTemplatesDataGridFactory;
use App\Components\DataGrids\MailTemplatesDataGrid\MailTemplatesDataGrid;
use App\Components\Forms\AddEditMailTemplateForm\AddEditMailTemplateForm;
use App\Components\Forms\AddEditMailTemplateForm\Factories\IAddEditMailTemplateFormFactory;
use App\Components\Forms\AddEditProjectForm\CopyMailTemplatesForm;
use App\Components\Forms\AddEditProjectForm\Factories\ICopyMailTemplatesFormFactory;
use App\Model\Repository\MailTemplateRepository;

class MailingPresenter extends BasePresenter
{
    /** @var IMailingDataGridFactory @inject */
    public $mailingDataGridFactory;
    /** @var IMailTemplatesDataGridFactory @inject */
    public $mailTemplatesDataGridFactory;
    /** @var IAddEditMailTemplateFormFactory @inject */
    public $addEditMailTemplateFormFactory;
    /** @var ICopyMailTemplatesFormFactory @inject */
    public $copyMailTemplatesFormFactory;
    /** @var MailTemplateRepository @inject */
    public $mailTemplateRepository;

    /**
     * @return MailingDataGrid
     */
    protected function createComponentMailingDataGrid()
    {
        $control = $this->mailingDataGridFactory->create();
        return $control;
    }

    /**
     * @return MailTemplatesDataGrid
     */
    protected function createComponentMailTemplatesDataGrid()
    {
        $control = $this->mailTemplatesDataGridFactory->create();
        $control->onAddClicked[] = function () {
            $this->template->showAddTemplateModal = true;
            $this->redrawControl('addEditTemplateModal');
        };
        $control->onCopyClicked[] = function () {
            $this->template->copyTemplatesModal = true;
            $this->redrawControl('copyTemplatesModal');
        };
        $control->onEditClicked[] = function ($id) {
            $template = $this->mailTemplateRepository->getById($id);
            $this['addEditMailTemplateForm']->setDefaults($template->getAsArray());
            $this->template->showEditTemplateModal = true;
            $this->redrawControl('addEditTemplateModal');
        };
        return $control;
    }

    /**
     * @return AddEditMailTemplateForm
     */
    protected function createComponentAddEditMailTemplateForm()
    {
        $form = $this->addEditMailTemplateFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'E-mailová šablona byla úspěšně ' .
                (empty($form['form']->getValues()['id']) ?  'vložena.' : 'editována.'), 'success');
            $this->redirect('Mailing:templates');
        };
        $form->onError[] = function ($form, $error) {
            $this->flashMessage(
                'E-mailovou šablonu se nepodařilo ' .
                (empty($form['form']->getValues()['id']) ?  'vložit.' : 'editovat.') .
                ' Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }

    /**
     * @return CopyMailTemplatesForm
     */
    protected function createComponentCopyMailTemplatesForm()
    {
        $form = $this->copyMailTemplatesFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'E-mailové šablony byly úspěšně zkopírovány.', 'success');
            $this->redirect('Mailing:templates');
        };
        $form->onError[] = function ($form, $error) {
            $this->flashMessage(
                'E-mailové šablony se nepodařilo zkopírovat. Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }
}
