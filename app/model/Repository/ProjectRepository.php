<?php

namespace App\Model\Repository;

use App\Model\Entity\Project;
use Kdyby\Doctrine\EntityManager;

class ProjectRepository extends AbstractRepository
{
    /**
     * ProjectRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entityRepository = $this->entityManager->getRepository(Project::class);
    }

    /**
     * @return Project[]
     */
    public function getAll()
    {
        return parent::getAll();
    }

    /**
     * @param $id
     * @return Project
     */
    public function getById($id)
    {
        return parent::getById($id);
    }

    /**
     * @param $parameters
     * @return Project[]
     */
    public function getByParameters($parameters)
    {
        return parent::getByParameters($parameters);
    }

    /**
     * @param $parameters
     * @return Project
     */
    public function getOneByParameters($parameters)
    {
        return parent::getOneByParameters($parameters);
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            Project::TYPE_BACHELOR_THESIS => 'Bakalářská práce',
            Project::TYPE_MASTER_THESIS => 'Diplomová práce',
            Project::TYPE_PROJECT => 'Projekt'
        ];
    }
}