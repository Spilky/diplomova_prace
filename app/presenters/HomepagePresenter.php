<?php

namespace App\Presenters;

use App\Components\Forms\SendMessageForm\Factories\ISendMessageFormFactory;
use App\Components\Forms\SendMessageForm\SendMessageForm;
use App\Model\Entity\Group;
use App\Model\Entity\Project;
use App\Model\Entity\TimeLineItem;
use App\Model\Repository\GroupRepository;
use App\Model\Repository\MailRepository;
use App\Model\Repository\ReportRepository;
use App\Model\Repository\TagRepository;
use App\Model\Repository\TermRepository;
use App\Model\Repository\TimeLineItemRepository;
use DateTime;
use Kdyby\Doctrine\EntityManager;
use Ublaboo\Mailing\MailFactory;

class HomepagePresenter extends BasePresenter
{
    /** @var MailRepository @inject */
    public $mailRepository;
    /** @var ReportRepository @inject */
    public $reportRepository;
    /** @var TagRepository @inject */
    public $tagRepository;
    /** @var TermRepository @inject */
    public $termRepository;
    /** @var TimeLineItemRepository @inject */
    public $timeLineItemRepository;
    /** @var MailFactory @inject */
    public $mailFactory;
    /** @var EntityManager @inject */
    public $entityManager;
    /** @var ISendMessageFormFactory @inject */
    public $sendMessageFormFactory;

    public function renderDefault()
    {
        $group = $this->groupRepository->getById($this->user->identity->group);
        $projects = $this->getOrderedProjectsInGroup($group, true);
        $reports = [];
        $consultations = [];
        foreach ($projects as $project) {
            $reports[$project->getId()] = [];
            foreach ($this->reportRepository->getByParameters(['project' => $project->getId()]) as $report) {
                $reports[$project->getId()][$report->getDate()->format('Y-m-d')] = $report;
            }

            $consultations[$project->getId()] = [];
            foreach ($this->timeLineItemRepository->getByParameters([
                'project' => $project->getId(),
                'type' => TimeLineItem::TYPE_CONSULTATION
            ]) as $consultation) {
                $consultations[$project->getId()][$consultation->getDate()->format('Y-m-d')] = $consultation;
            }
        }
        $terms = [];
        foreach (GroupRepository::getListOfWeeksByGroup($group, 'Y-m-d') as $monday => $term) {
            $terms[$monday] = [];
        }
        foreach ($this->termRepository->getByParameters(['group' => $group->getId()]) as $term) {
            $date = clone $term->getDate();
            while ($date->format('w') != 1) {
                $date->modify( '-1 day' );
            }

            $terms[$date->format('Y-m-d')][$term->getDate()->format('Y-m-d')] = $term;
            ksort($terms[$date->format('Y-m-d')]);
        }
        $date = new DateTime();
        while ($date->format('w') != 1) {
            $date->modify( '-1 day' );
        }

        $this->template->tags = $this->tagRepository->getByParameters(['group' => $group->getId()]);
        $this->template->currentMonday = $date->format('Y-m-d');
        $this->template->weeks = GroupRepository::getListOfWeeksByGroup($group, 'j.n.');
        $this->template->projects = $projects;
        $this->template->reports = $reports;
        $this->template->consultations = $consultations;
        $this->template->terms = $terms;
    }

    /**
     * @return SendMessageForm
     */
    protected function createComponentSendMessageForm()
    {
        $form = $this->sendMessageFormFactory->create();
        $projects = $this->getOrderedProjectsInGroup($this->user->identity->group, true);
        $receivers = [];
        foreach ($projects as $project) {
            $student = $project->getStudent();
            $receivers[$student->getName() . ' <' . $student->getEmail() . '>'] = $student->getName();
        }
        $form->setReceivers($receivers);
        $form->onSuccess[] = function ($form) {
            $this->flashMessage('Zpráva byla úspěšně odeslána.', 'success');
            $this->redirect('Homepage:');
        };
        $form->onError[] = function ($form, $error) {
            $this->flashMessage('Zpráva se nepodařilo odeslat. Zkuste to znovu prosím.', 'danger');
        };

        return $form;
    }

    /**
     * @param int|Group $group
     * @param bool $notActiveLast
     * @return Project[]
     */
    private function getOrderedProjectsInGroup($group, $notActiveLast = false)
    {
        $projects = [];
        if ($notActiveLast) {
            $notActiveProjects = [];
        }
        $qb = $this->projectRepository->getQB('pr')
            ->join('pr.student', 'us')
            ->where('pr.group = :group')
            ->setParameter('group', $group)
            ->orderBy('us.name');
        foreach ($qb->getQuery()->getResult() as $project) {
            if ($project->getStudent()->getId() == $this->user->id) {
                $studentProject = $project;
                continue;
            }
            if ($notActiveLast && $project->getNotActive()) {
                $notActiveProjects[$project->getStudent()->getName()] = $project;
                continue;
            }
            $projects[$project->getStudent()->getName()] = $project;
        }
        if (isset($studentProject)) {
            array_unshift($projects, $studentProject);
        }
        if (isset($notActiveProjects)) {
            $projects = array_merge($projects, $notActiveProjects);
        }
        return $projects;
    }
}
