<?php

namespace App\Model\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`group`", options={"collate"="utf8_czech_ci"})
 */
class Group extends AbstractEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="supervisor_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $supervisor;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="date")
     */
    protected $dateFrom;

    /**
     * @ORM\Column(type="date")
     */
    protected $dateTo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $reminderConsultationsWeeks;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $reminderReportsWeeks;

    /**
     * @return User
     */
    public function getSupervisor()
    {
        return $this->supervisor;
    }

    /**
     * @param User $supervisor
     */
    public function setSupervisor($supervisor)
    {
        $this->supervisor = $supervisor;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return DateTime
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * @param DateTime $dateFrom
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;
    }

    /**
     * @return DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * @param DateTime $dateTo
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;
    }

    /**
     * @return int
     */
    public function getReminderConsultationsWeeks()
    {
        return $this->reminderConsultationsWeeks;
    }

    /**
     * @param int $reminderConsultationsWeeks
     */
    public function setReminderConsultationsWeeks($reminderConsultationsWeeks)
    {
        $this->reminderConsultationsWeeks = $reminderConsultationsWeeks;
    }

    /**
     * @return int
     */
    public function getReminderReportsWeeks()
    {
        return $this->reminderReportsWeeks;
    }

    /**
     * @param int $reminderReportsWeeks
     */
    public function setReminderReportsWeeks($reminderReportsWeeks)
    {
        $this->reminderReportsWeeks = $reminderReportsWeeks;
    }
}
