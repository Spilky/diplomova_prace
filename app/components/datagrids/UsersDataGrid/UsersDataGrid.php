<?php

namespace App\Components\DataGrids\UsersDataGrid;

use App\Components\DataGrids\AbstractDataGrid;
use App\Model\Entity\User;
use App\Model\Repository\UserRepository;
use Exception;
use Nette\Application\ForbiddenRequestException;
use Nette\Security\User as NetteUser;
use Tracy\Debugger;
use Ublaboo\DataGrid\DataGrid;

class UsersDataGrid extends AbstractDataGrid
{
    /** @var NetteUser */
    private $netteUser;
    /** @var UserRepository */
    private $userRepository;

    /**
     * UsersDataGrid constructor.
     * @param NetteUser $netteUser
     * @param UserRepository $userRepository
     */
    public function __construct(NetteUser $netteUser, UserRepository $userRepository)
    {
        $this->netteUser = $netteUser;
        $this->userRepository = $userRepository;
    }

    /**
     * @return DataGrid
     */
    public function createComponentDataGrid()
    {
        $grid = parent::create();

        $grid->addColumnText('email', 'E-mail')
            ->setSortable()
            ->setFilterText();

        $grid->addColumnText('name', 'Jméno a příjmení')
            ->setSortable()
            ->setFilterText();

        $roles = UserRepository::getRoles();
        $grid->addColumnText('role', 'Status')
            ->setReplacement($roles)
            ->setSortable()
            ->setFilterSelect(['' => '-'] + $roles);

        $grid->addAction('settings', 'Editovat', 'User:edit')
            ->setIcon('pencil');

        $grid->addAction('delete', 'Smazat', 'delete!')
            ->setClass('ajax btn btn-xs btn-danger')
            ->setIcon('trash')
            ->setConfirm('Opravdu chcete smazat uživatele %s?', 'name');

        $grid->setDataSource($this->userRepository->getQB('u')
            ->where('u.role != :role')
            ->setParameter('role', User::ROLE_UNREGISTERED));

        return $grid;
    }

    /**
     * @param int $id
     */
    public function handleDelete($id)
    {
        $user = $this->userRepository->getById($id);

        if ($user) {
            if (!$this->netteUser->isInRole(User::ROLE_ADMIN)) {
                new ForbiddenRequestException();
            }

            try {
                $this->userRepository->deleteWhere(['id' => $id]);
                $this->flashMessage('Uživatel byl úspěšně smazán.', 'success');
            } catch (Exception $e) {
                Debugger::log($e);
                $this->flashMessage('Uživatele se nepodařilo smazat. Zkuste to prosím znovu.', 'danger');
            }

            $this->redrawControl('flashes');
            $this['dataGrid']->reload();
        }
    }
}