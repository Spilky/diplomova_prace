<?php

namespace App\Model\Repository;

use App\Model\Entity\Group;
use Kdyby\Doctrine\EntityManager;

class GroupRepository extends AbstractRepository
{
    /**
     * GroupRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entityRepository = $this->entityManager->getRepository(Group::class);
    }

    /**
     * @return Group[]
     */
    public function getAll()
    {
        return parent::getAll();
    }

    /**
     * @param $id
     * @return Group
     */
    public function getById($id)
    {
        return parent::getById($id);
    }

    /**
     * @param $parameters
     * @return Group[]
     */
    public function getByParameters($parameters)
    {
        return parent::getByParameters($parameters);
    }

    /**
     * @param $parameters
     * @return Group
     */
    public function getOneByParameters($parameters)
    {
        return parent::getOneByParameters($parameters);
    }

    /**
     * @param Group $group
     * @param string $format
     * @return array
     */
    public static function getListOfWeeksByGroup(Group $group, $format = 'j.n.Y')
    {
        $weeks = [];
        $begin = $group->getDateFrom();
        $end = $group->getDateTo();
        $end = $end->modify( '+1 day' );

        $interval = new \DateInterval('P1D');
        $period = new \DatePeriod($begin, $interval ,$end);

        $i = 0;
        $count = iterator_count($period);
        $lastKey = null;
        foreach($period as $date){
            $dateClone = clone $date;
            if ($i == 0 || $dateClone->format('w') == 1) {
                if ($i == 0 && $dateClone->format('w') != 1) {
                    do {
                        $dateClone->modify( '-1 day' );
                    } while($dateClone->format('w') != 1);
                }
                $lastKey = $dateClone->format('Y-m-d');
                $weeks[$lastKey] = $dateClone->format($format);
            }

            $i++;

            $dateClone = clone $date;
            if ($i == $count || $dateClone->format('w') == 0) {
                if ($i == $count && $dateClone->format('w') != 0) {
                    do {
                        $dateClone->modify( '+1 day' );
                    } while($dateClone->format('w') != 0);
                }
                $weeks[$lastKey] .= ' - ' . $dateClone->format($format);
            }
        }

        return $weeks;
    }
}