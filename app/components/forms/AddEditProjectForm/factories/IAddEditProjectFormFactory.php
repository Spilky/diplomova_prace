<?php

namespace App\Components\Forms\AddEditProjectForm\Factories;

use App\Components\Forms\AddEditProjectForm\AddEditProjectForm;

interface IAddEditProjectFormFactory
{
    /**
     * @return AddEditProjectForm
     */
    public function create();
}
