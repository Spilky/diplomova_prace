<?php

namespace App\Model\Mail;

use App\Model\Entity\User;
use Nette\Mail\Message;
use Ublaboo\Mailing\IComposableMail;

class SendMessageMail extends AbstractMail implements IComposableMail
{
    /**
     * @param Message $message
     * @param null|array $params
     */
    public function compose(Message $message, $params = NULL)
    {
        /** @var User $sender */
        $sender = $params['sender'];

        $message->setFrom($sender->getEmail(), $sender->getName());
        $message->addReplyTo($sender->getEmail(), $sender->getName());
        foreach ($params['receivers'] as $receiver) {
            $message->addTo($receiver);
        }
        if ($params['copy']) {
            $message->addBcc($sender->getEmail(), $sender->getName());
        }
        $message->setSubject($params['subject'] . self::SUBJECT_TAIL);
    }
}
