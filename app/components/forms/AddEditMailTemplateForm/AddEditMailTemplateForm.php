<?php

namespace App\Components\Forms\AddEditMailTemplateForm;

use App\Components\Forms\AbstractForm;
use App\Model\Entity\MailTemplate;
use App\Model\Entity\User;
use App\Model\Repository\MailTemplateRepository;
use App\Model\Repository\UserRepository;
use Exception;
use Nette\Application\UI\Form;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class AddEditMailTemplateForm extends AbstractForm
{
    /** @var MailTemplateRepository */
    private $mailTemplateRepository;
    /** @var UserRepository */
    private $userRepository;
    /** @var NetteUser */
    private $netteUser;

    /**
     * AddEditMailTemplateForm constructor.
     * @param MailTemplateRepository $mailTemplateRepository
     * @param UserRepository $userRepository
     * @param NetteUser $netteUser
     */
    public function __construct(
        MailTemplateRepository $mailTemplateRepository,
        UserRepository $userRepository,
        NetteUser $netteUser
    ) {
        $this->mailTemplateRepository = $mailTemplateRepository;
        $this->userRepository = $userRepository;
        $this->netteUser = $netteUser;
    }

    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form;

        $form->addText('name', 'Název šablony')
            ->setRequired('Vložte prosím název šablony.');

        $form->addText('subject', 'Předmět');
        
        $form->addTextArea('message', 'Zpráva');

        if (!$this->netteUser->isInRole(User::ROLE_STUDENT)) {
            $form->addCheckbox('public', 'Veřejná šablona - mohou si ji zkopírovat ostatní vedoucí')
                ->setDefaultValue(true);
        }

        $form->addHidden('id');

        $form->addSubmit('submit', 'Uložit');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        $values['user'] = $this->userRepository->getById($this->netteUser->id);

        if (!isset($values['public'])) {
            $values['public'] = false;
        }

        if (empty($values['id'])) {
            $template = new MailTemplate($values);
        }

        try {
            if (empty($values['id'])) {
                $this->mailTemplateRepository->insert($template);
            } else {
                $this->mailTemplateRepository->updateWhere($values, ['id' => $values['id']]);
            }
        } catch (Exception $e) {
            Debugger::log($e);
            $this->onError($this, $e);
            return;
        }

        $this->onSuccess($this);
    }
}
