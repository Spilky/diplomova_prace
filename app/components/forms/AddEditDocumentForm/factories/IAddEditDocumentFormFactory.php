<?php

namespace App\Components\Forms\AddEditDocumentForm\Factories;

use App\Components\Forms\AddEditDocumentForm\AddEditDocumentForm;

interface IAddEditDocumentFormFactory
{
    /**
     * @return AddEditDocumentForm
     */
    public function create();
}
