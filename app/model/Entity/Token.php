<?php

namespace App\Model\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * @ORM\Entity
 * @ORM\Table(name="`token`", options={"collate"="utf8_czech_ci"})
 */
class Token extends AbstractEntity
{
    const TYPE_FORGOTTEN_PASSWORD = 'forgotten_password',
        TYPE_CONSULTATION_LOGIN = 'consultation_login',
        TYPE_CONSULTATION_LOGOUT = 'consultation_logout';

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    protected $user;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    protected $token;

    /**
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $date_valid_until;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $number_use;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $date_used;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $ip;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $hostname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $user_agent;

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        if (!in_array($type, array(self::TYPE_FORGOTTEN_PASSWORD, self::TYPE_CONSULTATION_LOGIN, self::TYPE_CONSULTATION_LOGOUT))) {
            throw new InvalidArgumentException("Invalid type");
        }
        $this->type = $type;
    }

    /**
     * @return DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param DateTime $date_created
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
    }

    /**
     * @return DateTime
     */
    public function getDateValidUntil()
    {
        return $this->date_valid_until;
    }

    /**
     * @param DateTime $date_valid_until
     */
    public function setDateValidUntil($date_valid_until)
    {
        $this->date_valid_until = $date_valid_until;
    }

    /**
     * @return integer
     */
    public function getNumberUse()
    {
        return $this->number_use;
    }

    /**
     * @param integer $number_use
     */
    public function setNumberUse($number_use)
    {
        $this->number_use = $number_use;
    }

    /**
     * @return DateTime
     */
    public function getDateUsed()
    {
        return $this->date_used;
    }

    /**
     * @param DateTime $date_used
     */
    public function setDateUsed($date_used)
    {
        $this->date_used = $date_used;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getHostname()
    {
        return $this->hostname;
    }

    /**
     * @param string $hostname
     */
    public function setHostname($hostname)
    {
        $this->hostname = $hostname;
    }

    /**
     * @return string
     */
    public function getUserAgent()
    {
        return $this->user_agent;
    }

    /**
     * @param string $user_agent
     */
    public function setUserAgent($user_agent)
    {
        $this->user_agent = $user_agent;
    }
}