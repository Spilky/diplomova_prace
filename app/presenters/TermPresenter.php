<?php

namespace App\Presenters;

use App\Components\DataGrids\TermsDataGrid\Factories\ITermsDataGridFactory;
use App\Components\DataGrids\TermsDataGrid\TermsDataGrid;
use App\Components\Forms\AddEditProjectForm\CopyTermsForm;
use App\Components\Forms\AddEditProjectForm\Factories\ICopyTermsFormFactory;
use App\Components\Forms\AddEditTermForm\AddEditTermForm;
use App\Components\Forms\AddEditTermForm\Factories\IAddEditTermFormFactory;
use App\Model\Entity\Term;
use App\Model\Repository\GroupRepository;
use App\Model\Repository\TermRepository;
use Exception;
use Nette\Application\BadRequestException;
use Nette\Application\ForbiddenRequestException;
use Tracy\Debugger;

class TermPresenter extends BasePresenter
{
    /** @var TermRepository @inject */
    public $termRepository;
    /** @var ITermsDataGridFactory @inject */
    public $termsDataGridFactory;
    /** @var IAddEditTermFormFactory @inject */
    public $addEditTermFormFactory;
    /** @var ICopyTermsFormFactory @inject */
    public $copyTermsFormFactory;

    /**
     * @return TermsDataGrid
     */
    protected function createComponentTermsDataGrid()
    {
        $control = $this->termsDataGridFactory->create();
        return $control;
    }

    /**
     * @param Term $term
     * @throws ForbiddenRequestException
     */
    private function checkRights($term)
    {
        $groupsIds = array_keys(GroupRepository::getIdIndexedArrayOfNames(
            $this->groupRepository->getByParameters(['supervisor' => $this->user->id])
        ));
        if (!in_array($term->getGroup()->getId(), $groupsIds)) {
            throw new ForbiddenRequestException;
        }
    }

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionEdit($id = 0)
    {
        $term = $this->termRepository->getById($id);
        if (is_null($term)) {
            throw new BadRequestException;
        }

        $this->checkRights($term);

        $this['addEditTermForm']->setTerm($term);
    }

    /**
     * @param int $id
     * @param int $projectId
     * @throws BadRequestException
     */
    public function actionDelete($id = 0, $projectId = 0)
    {
        $term = $this->termRepository->getById($id);
        $project = $this->projectRepository->getById($projectId);
        if (is_null($project) || is_null($term)) {
            throw new BadRequestException;
        }

        $this->checkRights($term);

        try {
            $this->termRepository->delete($term);
        } catch (Exception $e) {
            Debugger::log($e);
            $this->flashMessage('Termín se nepodařilo smazat. Zkuste to znovu prosím.', 'danger');
            return;
        }

        $this->flashMessage('Termín byl úspěšně smazán.', 'success');
        $this->redirect('Project:detail', ['id' => $project->getId()]);
    }

    /**
     * @return AddEditTermForm
     */
    protected function createComponentAddEditTermForm()
    {
        $form = $this->addEditTermFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'Termín byl úspěšně ' .
                ($this->getAction() == 'add' ?  'vložen.' : 'editován.'), 'success');
            $this->redirect('Term:');
        };
        $form->onError[] = function ($form, $error) {
            $this->flashMessage(
                'Termín se nepodařilo ' .
                ($this->getAction() == 'add' ?  'vložit.' : 'editovat.') .
                ' Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }

    /**
    * @return CopyTermsForm
    */
    protected function createComponentCopyTermsForm()
    {
        $form = $this->copyTermsFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'Termíny byl úspěšně zkopírovány.', 'success');
            $this->redirect('Term:');
        };
        $form->onError[] = function ($form, $error) {
            $this->flashMessage(
                'Termín se nepodařilo zkopírovat. Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }
}
