<?php

namespace App\Components\Forms\AddEditMilestoneForm;

use App\Components\Forms\AbstractForm;
use App\Model\Entity\Project;
use App\Model\Entity\TimeLineItem;
use App\Model\Repository\ProjectRepository;
use App\Model\Repository\TimeLineItemRepository;
use DateTime;
use Exception;
use Nette\Application\UI\Form;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Tracy\Debugger;

class AddEditMilestoneForm extends AbstractForm
{
    /** @var NetteUser */
    private $netteUser;
    /** @var ProjectRepository */
    private $projectRepository;
    /** @var TimeLineItemRepository */
    private $timeLineItemRepository;
    /** @var Project */
    private $project;
    /** @var TimeLineItem */
    private $timeLineItem = null;
    /** @var Html */
    private $cancelButton = null;

    /**
     * AddEditMilestoneForm constructor.
     * @param Project $project
     * @param NetteUser $netteUser
     * @param ProjectRepository $projectRepository
     * @param TimeLineItemRepository $timeLineItemRepository
     */
    public function __construct(
        Project $project,
        NetteUser $netteUser,
        ProjectRepository $projectRepository,
        TimeLineItemRepository $timeLineItemRepository
    ) {
        $this->project = $project;
        $this->netteUser = $netteUser;
        $this->projectRepository = $projectRepository;
        $this->timeLineItemRepository = $timeLineItemRepository;
    }

    /**
     * @param int|TimeLineItem $timeLineItem
     */
    public function setTimeLineItem($timeLineItem)
    {
        if (!($timeLineItem instanceof TimeLineItem)) {
            $timeLineItem = $this->timeLineItemRepository->getById($timeLineItem);
        }

        $this->timeLineItem = $timeLineItem;
    }

    /**
     * @param Html $cancelButton
     */
    public function setCancelButton($cancelButton)
    {
        $this->cancelButton = $cancelButton;
    }

    public function render()
    {
        $data = [];
        if ($this->timeLineItem) {
            $data = $this->timeLineItem->getAsArray();
            $data['date'] = $this->timeLineItem->getDate()->format('j.n.Y');
        }

        $data['projectId'] = $this->project->getId();
        $this['form']->setDefaults($data);

        if ($this->cancelButton) {
            $this->template->cancelButton = $this->cancelButton;
        }

        parent::render();
    }

    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form;

        $form->addText('date', 'Den')
            ->setRequired('Vyberte si prosím datum.');

        $form->addTextArea('text', 'Text');

        $form->addHidden('projectId');

        $form->addHidden('id');

        $form->addSubmit('submit', 'Uložit');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        $project = $this->projectRepository->getById($values['projectId']);
        $values['date'] = new DateTime($values['date']);
        unset($values['projectId']);

        if (empty($values['id'])) {
            $values['project'] = $project;
            $values['type'] = TimeLineItem::TYPE_MILESTONE;
            $report = new TimeLineItem($values);
        }

        try {
            if (empty($values['id'])) {
                $this->timeLineItemRepository->insert($report);
            } else {
                $this->timeLineItemRepository->updateWhere($values, ['id' => $values['id']]);
            }
        } catch (Exception $e) {
            $this->onError($this, $e);
            Debugger::log($e);
            return;
        }

        $this->onSuccess($this);
    }
}
