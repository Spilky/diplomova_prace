<?php

namespace App\Components\Forms\AddEditFeedbackForm\Factories;

use App\Components\Forms\AddEditFeedbackForm\AddEditFeedbackForm;
use App\Model\Entity\Report;
use Nette\Utils\Html;

interface IAddEditFeedbackFormFactory
{
    /**
     * @param int|Report $report
     * @param Html $cancelButton
     * @return AddEditFeedbackForm
     */
    public function create($report, $cancelButton);
}
