<?php

namespace App\Components\Forms\AddEditGroupForm\Factories;

use App\Components\Forms\AddEditGroupForm\AddEditGroupForm;

interface IAddEditGroupFormFactory
{
    /**
     * @return AddEditGroupForm
     */
    public function create();
}
