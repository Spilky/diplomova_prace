<?php

namespace App\Components\Forms\AddEditConsultationForm\Factories;

use App\Components\Forms\AddEditConsultationForm\AddEditConsultationForm;
use App\Model\Entity\Project;

interface IAddEditConsultationFormFactory
{
    /**
     * @param Project $project
     * @return AddEditConsultationForm
     */
    public function create(Project $project);
}
