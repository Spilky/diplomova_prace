<?php

namespace App\Components\Forms\ConsultationSettingsForm\Factories;

use App\Components\Forms\ConsultationSettingsForm\ConsultationSettingsForm;

interface IConsultationSettingsFormFactory
{
    /**
     * @return ConsultationSettingsForm
     */
    public function create();
}
