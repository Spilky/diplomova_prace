<?php

namespace App\Components\DataGrids\MailingDataGrid;

use App\Components\DataGrids\AbstractDataGrid;
use App\Model\Repository\MailRepository;
use Nette\Security\User as NetteUser;
use Ublaboo\DataGrid\DataGrid;

class MailingDataGrid extends AbstractDataGrid
{
    /** @var NetteUser */
    private $netteUser;
    /** @var MailRepository */
    private $mailRepository;

    /**
     * GroupsDataGrid constructor.
     * @param NetteUser $netteUser
     * @param MailRepository $mailRepository
     */
    public function __construct(NetteUser $netteUser, MailRepository $mailRepository)
    {
        $this->netteUser = $netteUser;
        $this->mailRepository = $mailRepository;
    }

    /**
     * @return DataGrid
     */
    public function createComponentDataGrid()
    {
        $grid = parent::create();

        $grid->setColumnsHideable();

        $grid->addColumnText('sender', 'Odesílatel')
            ->setSortable()
            ->setFilterText();

        $grid->addColumnText('receivers', 'Příjemci')
            ->setSortable()
            ->setFilterText();

        $grid->addColumnText('subject', 'Předmět')
            ->setSortable()
            ->setFilterText();

        $grid->addColumnText('message', 'Zpráva')
            ->setDefaultHide()
            ->setTemplateEscaping(false)
            ->setFilterText();

        $grid->addColumnDateTime('sent', 'Datum odeslání')
            ->setAlign('left')
            ->setFormat('j.n.Y')
            ->setSortable()
            ->setFilterDateRange()
            ->setAttribute('data-date-language', 'cs');

        $grid->setItemsDetail();
        $grid->setTemplateFile(__DIR__ . '/templates/datagrid_default.latte');

        $grid->setDataSource($this->mailRepository->getQB('ma')
            ->where('ma.group = :group')
            ->setParameter('group', $this->netteUser->identity->group));

        return $grid;
    }
}