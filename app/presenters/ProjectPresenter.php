<?php

namespace App\Presenters;

use App\Components\DataGrids\ProjectsDataGrid\Factories\IProjectsDataGridFactory;
use App\Components\DataGrids\ProjectsDataGrid\ProjectsDataGrid;
use App\Components\Forms\AddEditConsultationForm\Factories\IAddEditConsultationFormFactory;
use App\Components\Forms\AddEditFeedbackForm\Factories\IAddEditFeedbackFormFactory;
use App\Components\Forms\AddEditMilestoneForm\Factories\IAddEditMilestoneFormFactory;
use App\Components\Forms\AddEditProjectForm\AddEditProjectForm;
use App\Components\Forms\AddEditProjectForm\Factories\IAddEditProjectFormFactory;
use App\Components\Forms\AddEditProjectForm\Factories\IImportProjectsFormFactory;
use App\Components\Forms\AddEditProjectForm\ImportProjectsForm;
use App\Components\Forms\AddEditReportForm\AddEditReportForm;
use App\Components\Forms\AddEditReportForm\Factories\IAddEditReportFormFactory;
use App\Components\Forms\SendMessageForm\Factories\ISendMessageFormFactory;
use App\Components\Forms\SendMessageForm\SendMessageForm;
use App\Model\Entity\TimeLineItem;
use App\Model\Entity\User;
use App\Model\Repository\DocumentRepository;
use App\Model\Repository\ProjectRepository;
use App\Model\Repository\ReportRepository;
use App\Model\Repository\TermRepository;
use App\Model\Repository\TimeLineItemRepository;
use DateTime;
use Exception;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Multiplier;
use Nette\Utils\Html;
use Tracy\Debugger;

class ProjectPresenter extends BasePresenter
{
    /** @var DocumentRepository @inject */
    public $documentRepository;
    /** @var ReportRepository @inject */
    public $reportRepository;
    /** @var TermRepository @inject */
    public $termRepository;
    /** @var TimeLineItemRepository @inject */
    public $timeLineRepository;
    /** @var IProjectsDataGridFactory @inject */
    public $projectsDataGridFactory;
    /** @var IAddEditFeedbackFormFactory @inject */
    public $addEditFeedbackFormFactory;
    /** @var IAddEditProjectFormFactory @inject */
    public $addEditProjectFormFactory;
    /** @var IImportProjectsFormFactory @inject */
    public $importProjectsFormFactory;
    /** @var ISendMessageFormFactory @inject */
    public $sendMessageFormFactory;
    /** @var IAddEditMilestoneFormFactory @inject */
    public $addEditMilestoneFormFactory;
    /** @var IAddEditReportFormFactory @inject */
    public $addEditReportFormFactory;
    /** @var IAddEditConsultationFormFactory @inject */
    public $addEditConsultationFormFactory;

    /** @var boolean */
    private $showAllItems = false;

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionEdit($id = 0)
    {
        $project = $this->projectRepository->getById($id);
        if (is_null($project)) {
            throw new BadRequestException;
        }

        $data['id'] = $project->getId();
        $data['project'] = $project->getAsArray();
        $data['project']['secretTask'] = intval($project->getSecretTask());
        $data['project']['notActive'] = intval($project->getNotActive());
        $this['addEditProjectForm']->setDefaults($data);
        $this['addEditProjectForm']->setEditing();
    }

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionDetail($id = 0)
    {
        $project = $this->projectRepository->getById($id);
        if (is_null($project)) {
            throw new BadRequestException;
        }
    }

    /**
     * @param int $id
     */
    public function renderDetail($id)
    {
        $today = new DateTime();
        $project = $this->projectRepository->getById($id);

        $timeLine = [];
        $reports = $this->reportRepository->getByParameters(['project' => $project->getId()]);
        foreach ($reports as $report) {
            if ($report->getDate() > $today && !$this->showAllItems) {
                continue;
            }

            $timeLine[$report->getDate()->format('Ymd') . 'report'] = $report;
        }

        $consultations = $this->timeLineRepository->getByParameters([
            'project' => $project->getId(),
            'type' => TimeLineItem::TYPE_CONSULTATION
        ]);
        foreach ($consultations as $consultation) {
            if ($consultation->getDate() > $today && !$this->showAllItems) {
                continue;
            }

            $timeLine[$consultation->getDate()->format('Ymd') . 'consultation'] = $consultation;
        }

        $milestones = $this->timeLineRepository->getByParameters([
            'project' => $project->getId(),
            'type' => TimeLineItem::TYPE_MILESTONE
        ]);
        foreach ($milestones as $milestone) {
            if ($milestone->getDate() > $today && !$this->showAllItems) {
                continue;
            }

            $timeLine[$milestone->getDate()->format('Ymd') . $milestone->getId() . 'milestone'] = $milestone;
        }

        $terms = $this->termRepository->getByParameters([
            'group' => $project->getGroup()->getId()
        ]);
        foreach ($terms as $term) {
            if ($term->getDate() > $today && !$this->showAllItems) {
                continue;
            }

            $timeLine[$term->getDate()->format('Ymd') . $term->getId() . 'term'] = $term;
        }

        $documents = $this->documentRepository->getByParameters(['project' => $project->getId()]);

        if ($this->user->identity->timeLineSort == User::TIME_LINE_SORT_DESC) {
            krsort($timeLine);
        } else {
            ksort($timeLine);
        }

        $this->template->project = $project;
        $this->template->projectTypes = ProjectRepository::getTypes();
        $this->template->timeLine = $timeLine;
        $this->template->timeLineSort = $this->user->identity->timeLineSort;
        $this->template->showAllItems = $this->showAllItems;
        $this->template->documents = $documents;
}

    /**
     * @param integer $id
     * @param string $note
     */
    public function handleSaveNote($id, $note)
    {
        try {
            $project = $this->projectRepository->getById($id);
            if ($this->user->isInRole(User::ROLE_STUDENT)) {
                $project->setPrivateNote($note);
            } else {
                $project->setSecretNote($note);
            }
            $this->projectRepository->update($project);
            $this->flashMessage('Poznámka byla úspěšně uložena.', 'success');
        } catch (Exception $e) {
            $this->flashMessage('Poznámku se bohužel nepodařilo uložit. Zkuste to prosím znovu.', 'danger');
            Debugger::log($e);
        }

        $this->redirect('Project:detail', ['id' => $id]);
    }

    /**
     * @param string $selector
     * @param boolean $display
     */
    public function handleTimeLineHidden($selector, $display)
    {
        $user = $this->userRepository->getById($this->user->id);
        $timeLineHidden = $user->getTimeLineHidden();
        if ($display) {
            unset($timeLineHidden[$selector]);
        } else {
            $timeLineHidden[$selector] = true;
        }
        $user->setTimeLineHidden($timeLineHidden);
        $this->user->identity->timeLineHidden = $timeLineHidden;
        $this->userRepository->update($user);
        $this->redirect('this');
    }

    /**
     * @param string $direction
     */
    public function handleTimeLineSort($direction)
    {
        $user = $this->userRepository->getById($this->user->id);
        $user->setTimeLineSort($direction);
        $this->user->identity->timeLineSort = $direction;
        $this->userRepository->update($user);
        $this->redirect('this');
    }

    public function handleShowAllItems()
    {
        $this->showAllItems = true;
        $this->redrawControl('timeLineItems');
    }

    /**
     * @return ProjectsDataGrid
     */
    protected function createComponentProjectsDataGrid()
    {
        $control = $this->projectsDataGridFactory->create();
        return $control;
    }

    /**
     * @return AddEditProjectForm
     */
    protected function createComponentAddEditProjectForm()
    {
        $form = $this->addEditProjectFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'Projekt byl úspěšně ' .
                ($this->getAction() == 'add' ?  'vložen.' : 'editován.'), 'success');
            $this->redirect('Project:');
        };
        $form->onError[] = function ($form, $error) {
            $this->flashMessage(
                'Projekt se nepodařilo ' .
                ($this->getAction() == 'add' ?  'vložit.' : 'editovat.') .
                ' Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }

    /**
     * @return ImportProjectsForm
     */
    protected function createComponentImportProjectsForm()
    {
        $form = $this->importProjectsFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage('Projekty byly úspěšně importovány.', 'success');
            $this->redirect('Project:');
        };
        $form->onError[] = function ($form, $error) {
            $this->flashMessage('Projekty se nepodařilo importovat. Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }

    /**
     * @return SendMessageForm
     */
    protected function createComponentSendMessageForm()
    {
        $form = $this->sendMessageFormFactory->create();
        $project = $this->projectRepository->getById($this->getParameter('id'));
        $student = $project->getStudent();
        $form->setReceivers([$student->getName() . ' <' . $student->getEmail() . '>' => $student->getName()]);
        $form->setSingleReceiver();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage('Zpráva byla úspěšně odeslána.', 'success');
            $this->redirect('this');
        };
        $form->onError[] = function ($form, $error) {
            $this->flashMessage('Zpráva se nepodařilo odeslat. Zkuste to znovu prosím.', 'danger');
        };

        return $form;
    }

    /**
     * @return Multiplier
     */
    protected function createComponentAddEditMilestoneForm()
    {
        return new Multiplier(function ($milestone) {
            $project = $this->projectRepository->getById($this->getParameter('id'));
            $form = $this->addEditMilestoneFormFactory->create($project);
            if ($milestone) {
                $cancelButton = Html::el('a', [
                    'href' => '#milestone-text-' . $milestone,
                    'aria-controls' => 'milestone-text-' . $milestone,
                    'role' => 'tab',
                    'class' => ['btn btn-danger']
                ])
                    ->data('toggle', 'tab');
                $form->setTimeLineItem($milestone);
            } else {
                $cancelButton = Html::el('a', [
                    'href' => '#add-milestone-form',
                    'class' => ['btn btn-danger cancel-btn']
                ]);
            }
            $cancelButton->setText('Zrušit');
            $form->setCancelButton($cancelButton);
            $form->onSuccess[] = function ($form) {
                $this->flashMessage(
                    'Milník byl úspěšně uložen.', 'success');
                $this->redirect('this');
            };
            $form->onError[] = function ($form, $error) {
                $this->flashMessage(
                    'Milník se nepodařilo uložit. Zkuste to prosím znovu.', 'danger');
            };
            return $form;
        });
    }

    /**
     * @return AddEditReportForm
     */
    protected function createComponentAddEditReportForm()
    {
        return new Multiplier(function ($report) {
            $project = $this->projectRepository->getById($this->getParameter('id'));
            $form = $this->addEditReportFormFactory->create($project);
            if ($report) {
                $cancelButton = Html::el('a', [
                    'href' => '#report-text-' . $report,
                    'aria-controls' => 'report-text-' . $report,
                    'role' => 'tab',
                    'class' => ['btn btn-danger']
                ])
                    ->data('toggle', 'tab');
                $form->setReport($report);
            } else {
                $cancelButton = Html::el('a', [
                    'href' => '#add-report-form',
                    'class' => ['btn btn-danger cancel-btn']
                ]);
            }
            $cancelButton->setText('Zrušit');
            $form->setCancelButton($cancelButton);
            $form->onSuccess[] = function ($form) {
                $this->flashMessage('Report byl úspěšně uložen', 'success');
                $this->redirect('this');
            };
            $form->onError[] = function ($form, $error) {
                $this->flashMessage('Report se nepodařilo uložit. Zkuste to prosím znovu.', 'danger');
            };
            return $form;
        });
    }

    /**
     * @return Multiplier
     */
    protected function createComponentAddEditConsultationForm()
    {
        return new Multiplier(function ($consultation) {
            $project = $this->projectRepository->getById($this->getParameter('id'));
            $form = $this->addEditConsultationFormFactory->create($project);
            if ($consultation) {
                $cancelButton = Html::el('a', [
                    'href' => '#consultation-text-' . $consultation,
                    'aria-controls' => 'consultation-text-' . $consultation,
                    'role' => 'tab',
                    'class' => ['btn btn-danger']
                ])
                    ->data('toggle', 'tab');
                $form->setTimeLineItem($consultation);
            } else {
                $cancelButton = Html::el('a', [
                    'href' => '#add-consultation-form',
                    'class' => ['btn btn-danger cancel-btn']
                ]);
            }
            $cancelButton->setText('Zrušit');
            $form->setCancelButton($cancelButton);
            $form->onSuccess[] = function ($form) {
                $this->flashMessage('Konzultace byla úspěšně uložena.', 'success');
                $this->redirect('this');
            };
            $form->onError[] = function ($form, $error) {
                $this->flashMessage('Konzultaci se nepodařilo uložit. Zkuste to prosím znovu.', 'danger');
            };
            return $form;
        });
    }

    /**
     * @return Multiplier
     */
    protected function createComponentAddEditFeedbackForm()
    {
        return new Multiplier(function ($report) {
            $cancelButton = Html::el('a', [
                'href' => '#feedback-text-' . $report,
                'aria-controls' => 'feedback-text-' . $report,
                'role' => 'tab',
                'class' => ['btn btn-danger']
            ])
                ->data('toggle', 'tab')
                ->setText('Zrušit');
            $form = $this->addEditFeedbackFormFactory->create($report, $cancelButton);
            $form->onSuccess[] = function ($form) {
                $this->flashMessage('Zpětná vazba byla úspěšně uložena.', 'success');
                $this->redirect('this');
            };
            $form->onError[] = function ($form, $error) {
                $this->flashMessage('Zpětnou vazbu se nepodařilo uložit. Zkuste to prosím znovu.', 'danger');
            };
            return $form;
        });
    }
}
