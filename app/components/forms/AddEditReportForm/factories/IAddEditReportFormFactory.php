<?php

namespace App\Components\Forms\AddEditReportForm\Factories;

use App\Components\Forms\AddEditReportForm\AddEditReportForm;
use App\Model\Entity\Project;

interface IAddEditReportFormFactory
{
    /**
     * @param Project $project
     * @return AddEditReportForm
     */
    public function create(Project $project);
}
