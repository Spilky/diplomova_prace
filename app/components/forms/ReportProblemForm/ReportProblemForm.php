<?php

namespace App\Components\Forms\ReportProblemForm;

use App\Components\Forms\AbstractForm;
use App\Model\Mail\ReportProblemMail;
use Nette\Application\UI\Form;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;
use Ublaboo\Mailing\MailFactory;

class ReportProblemForm extends AbstractForm
{
    /** @var MailFactory */
    private $mailFactory;
    /** @var NetteUser */
    private $netteUser;

    /**
     * ReportProblemForm constructor.
     * @param MailFactory $mailFactory
     * @param NetteUser $netteUser
     */
    public function __construct(MailFactory $mailFactory, NetteUser $netteUser)
    {
        $this->mailFactory = $mailFactory;
        $this->netteUser = $netteUser;
    }

    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form;
        
        $form->addTextArea('text', 'Popis problému:');

        $form->addSubmit('send', 'Odeslat');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        $params = [
            'sender' => $this->netteUser->identity->name . ' <' . $this->netteUser->identity->email . '>',
            'text' => $values['text']
        ];

        try {
            $this->mailFactory->createByType(ReportProblemMail::class, $params)->send();
        } catch (\Exception $e) {
            $this->onError($this, $e);
            Debugger::log($e);
            return;
        }

        $this->onSuccess($this);
    }
}
