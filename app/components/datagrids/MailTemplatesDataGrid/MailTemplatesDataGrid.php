<?php

namespace App\Components\DataGrids\MailTemplatesDataGrid;

use App\Components\DataGrids\AbstractDataGrid;
use App\Model\Repository\MailTemplateRepository;
use Exception;
use Nette\Application\ForbiddenRequestException;
use Nette\Security\User as NetteUser;
use Tracy\Debugger;
use Ublaboo\DataGrid\DataGrid;

class MailTemplatesDataGrid extends AbstractDataGrid
{
    /** @var array */
    public $onAddClicked;
    /** @var array */
    public $onCopyClicked;
    /** @var array */
    public $onEditClicked;

    /** @var NetteUser */
    private $netteUser;
    /** @var MailTemplateRepository */
    private $mailTemplateRepository;

    /**
     * GroupsDataGrid constructor.
     * @param NetteUser $netteUser
     * @param MailTemplateRepository $mailTemplateRepository
     */
    public function __construct(NetteUser $netteUser, MailTemplateRepository $mailTemplateRepository)
    {
        $this->netteUser = $netteUser;
        $this->mailTemplateRepository = $mailTemplateRepository;
    }

    /**
     * @return DataGrid
     */
    public function createComponentDataGrid()
    {
        $grid = parent::create();

        $grid->setColumnsHideable();

        $grid->addColumnText('name', 'Název')
            ->setSortable()
            ->setFilterText();

        $grid->addColumnText('subject', 'Předmět')
            ->setSortable()
            ->setFilterText();

        $grid->addColumnText('message', 'Zpráva')
            ->setDefaultHide()
            ->setTemplateEscaping(false)
            ->setFilterText();

        $replacements = [true => 'Ano', false => 'Ne'];
        $grid->addColumnText('public', 'Veřejná')
            ->setReplacement($replacements)
            ->setSortable()
            ->setFilterSelect(array_merge([null => '-'], $replacements));

        $grid->addAction('edit', 'Editovat')
            ->setClass('ajax btn btn-xs btn-default')
            ->setIcon('pencil');

        $grid->addAction('delete', 'Smazat', 'delete!')
            ->setClass('ajax btn btn-xs btn-danger')
            ->setIcon('trash')
            ->setConfirm('Opravdu chcete smazat e-mailovou šablonu %s?', 'name');

        $grid->addToolbarButton('add!', 'Přidat šablonu')
            ->setIcon('plus')
            ->setClass('ajax btn btn-default btn-xs');

        $grid->addToolbarButton('copy!', 'Kopírovat šablony')
            ->setIcon('copy')
            ->setClass('ajax btn btn-default btn-xs');

        $grid->setItemsDetail();
        $grid->setTemplateFile(__DIR__ . '/templates/datagrid_default.latte');

        $grid->setDataSource($this->mailTemplateRepository->getQB('ma')
            ->where('ma.user = :user')
            ->setParameter('user', $this->netteUser->id));

        return $grid;
    }

    public function handleAdd()
    {
        $this->onAddClicked();
    }

    public function handleCopy()
    {
        $this->onCopyClicked();
    }

    /**
     * @param int $id
     */
    public function handleEdit($id)
    {
        $this->onEditClicked($id);
    }

    /**
     * @param int $id
     */
    public function handleDelete($id)
    {
        $template = $this->mailTemplateRepository->getById($id);

        if ($template) {
            if ($this->netteUser->id != $template->getUser()->getId()) {
                new ForbiddenRequestException();
            }

            try {
                $this->mailTemplateRepository->deleteWhere(['id' => $id]);
                $this->flashMessage('E-mailová šablona byla úspěšně smazán.', 'success');
            } catch (Exception $e) {
                Debugger::log($e);
                $this->flashMessage('E-mailovou šablonu se nepodařilo smazat. Zkuste to prosím znovu.', 'danger');
            }

            $this->redrawControl('flashes');
            $this['dataGrid']->reload();
        }
    }
}