<?php

namespace App\Components\DataGrids\TagsDataGrid;

use App\Components\DataGrids\AbstractDataGrid;
use App\Model\Repository\TagRepository;
use Exception;
use Nette\Application\ForbiddenRequestException;
use Nette\Security\User as NetteUser;
use Tracy\Debugger;
use Ublaboo\DataGrid\DataGrid;

class TagsDataGrid extends AbstractDataGrid
{
    /** @var NetteUser */
    private $netteUser;
    /** @var TagRepository */
    private $tagRepository;

    /**
     * TagsDataGrid constructor.
     * @param NetteUser $netteUser
     * @param TagRepository $tagRepository
     */
    public function __construct(NetteUser $netteUser, TagRepository $tagRepository)
    {
        $this->netteUser = $netteUser;
        $this->tagRepository = $tagRepository;
    }

    /**
     * @return DataGrid
     */
    public function createComponentDataGrid()
    {
        $grid = parent::create();

        $grid->addColumnText('name', 'Název')
            ->setSortable()
            ->setFilterText();

        $grid->addAction('edit', 'Editovat', 'Term:edit')
            ->setIcon('pencil');

        $grid->addAction('delete', 'Smazat', 'delete!')
            ->setClass('ajax btn btn-xs btn-danger')
            ->setIcon('trash')
            ->setConfirm('Opravdu chcete smazat štítek %s?', 'name');

        $grid->addToolbarButton('Tag:add', 'Přidat štítek')
            ->setIcon('plus');

        $grid->setDataSource($this->tagRepository->getQB('ta')
            ->join('ta.group', 'gr')
            ->where('gr.id = :group')
            ->setParameter('group', $this->netteUser->identity->group));

        return $grid;
    }

    /**
     * @param int $id
     */
    public function handleDelete($id)
    {
        $tag = $this->tagRepository->getById($id);

        if ($tag) {
            if ($this->netteUser->id != $tag->getGroup()->getSupervisor()->getId()) {
                new ForbiddenRequestException();
            }

            try {
                $this->tagRepository->deleteWhere(['id' => $id]);
                $this->flashMessage('Štítek byl úspěšně smazán.', 'success');
            } catch (Exception $e) {
                Debugger::log($e);
                $this->flashMessage('Štítek se nepodařilo smazat. Zkuste to prosím znovu.', 'danger');
            }

            $this->redrawControl('flashes');
            $this['dataGrid']->reload();
        }
    }
}