<?php

namespace App\Model\Repository;

use App\Model\Entity\Project;
use App\Model\Entity\User;
use Kdyby\Doctrine\EntityManager;

class UserRepository extends AbstractRepository
{
    /**
     * UserRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entityRepository = $this->entityManager->getRepository(User::class);
    }

    /**
     * @return User[]
     */
    public function getAll()
    {
        return parent::getAll();
    }

    /**
     * @param $id
     * @return User
     */
    public function getById($id)
    {
        return parent::getById($id);
    }

    /**
     * @param $parameters
     * @return User[]
     */
    public function getByParameters($parameters)
    {
        return parent::getByParameters($parameters);
    }

    /**
     * @param $parameters
     * @return User
     */
    public function getOneByParameters($parameters)
    {
        return parent::getOneByParameters($parameters);
    }

    /**
     * @param int|User $supervisor
     * @return User[]
     */
    public function getStudentsBySupervisor($supervisor)
    {
        $subQb = $this->entityManager->createQueryBuilder();
        $subQb->select('st.id')->from(Project::class, 'pr')
            ->join('pr.group', 'gr')
            ->join('pr.student', 'st')
            ->join('gr.supervisor', 'su')
            ->where('su.id = :supervisor');

        $qb = $this->getQB('us');
        $qb->where($qb->expr()->in('us.id', $subQb->getDQL()))
            ->setParameter('supervisor', $supervisor);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return string[]
     */
    public static function getRoles()
    {
        return [
            User::ROLE_ADMIN => 'Administrátor',
            User::ROLE_SUPERVISOR => 'Vedoucí',
            User::ROLE_STUDENT => 'Student'
        ];
    }
}