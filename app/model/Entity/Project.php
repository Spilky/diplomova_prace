<?php

namespace App\Model\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * @ORM\Entity
 * @ORM\Table(name="`project`", options={"collate"="utf8_czech_ci"})
 */
class Project extends AbstractEntity
{
    const TYPE_BACHELOR_THESIS = 'bachelor_thesis',
        TYPE_MASTER_THESIS = 'master_thesis',
        TYPE_PROJECT = 'project';

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="student_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $student;

    /**
     * @ORM\ManyToOne(targetEntity="Group")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $group;

    /**
     * @ORM\ManyToMany(targetEntity="Tag")
     */
    protected $tags;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="text")
     */
    protected $task;

    /**
     * @ORM\Column(type="text")
     */
    protected $literature;

    /**
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @ORM\Column(type="string")
     */
    protected $secretNote = '';

    /**
     * @ORM\Column(type="boolean")
     */
    protected $secretTask = false;

    /**
     * @ORM\Column(type="string")
     */
    protected $privateNote = '';

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $wisId;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $notActive = false;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $reminderConsultationsSent;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $reminderReportsSent;

    /**
     * Project constructor.
     * @param null|array $parameters
     */
    public function __construct($parameters = null)
    {
        $this->tags = new ArrayCollection();
        parent::__construct($parameters);
    }

    /**
     * @return User
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * @param User $student
     */
    public function setStudent($student)
    {
        $this->student = $student;
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param Group $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return ArrayCollection|Tag[]
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param ArrayCollection $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param string $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }

    /**
     * @return string
     */
    public function getLiterature()
    {
        return $this->literature;
    }

    /**
     * @param string $literature
     */
    public function setLiterature($literature)
    {
        $this->literature = $literature;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        if (!in_array($type, array(self::TYPE_BACHELOR_THESIS, self::TYPE_MASTER_THESIS, self::TYPE_PROJECT))) {
            throw new InvalidArgumentException("Invalid type");
        }
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getSecretNote()
    {
        return $this->secretNote;
    }

    /**
     * @param string $secretNote
     */
    public function setSecretNote($secretNote)
    {
        $this->secretNote = $secretNote;
    }

    /**
     * @return boolean
     */
    public function getSecretTask()
    {
        return $this->secretTask;
    }

    /**
     * @param boolean $secretTask
     */
    public function setSecretTask($secretTask)
    {
        $this->secretTask = $secretTask;
    }

    /**
     * @return string
     */
    public function getPrivateNote()
    {
        return $this->privateNote;
    }

    /**
     * @param string $privateNote
     */
    public function setPrivateNote($privateNote)
    {
        $this->privateNote = $privateNote;
    }

    /**
     * @return integer
     */
    public function getWisId()
    {
        return $this->wisId;
    }

    /**
     * @param integer $wisId
     */
    public function setWisId($wisId)
    {
        $this->wisId = $wisId;
    }

    /**
     * @return boolean
     */
    public function getNotActive()
    {
        return $this->notActive;
    }

    /**
     * @param boolean $notActive
     */
    public function setNotActive($notActive)
    {
        $this->notActive = $notActive;
    }

    /**
     * @return DateTime
     */
    public function getReminderConsultationsSent()
    {
        return $this->reminderConsultationsSent;
    }

    /**
     * @param DateTime $reminderConsultationsSent
     */
    public function setReminderConsultationsSent($reminderConsultationsSent)
    {
        $this->reminderConsultationsSent = $reminderConsultationsSent;
    }

    /**
     * @return DateTime
     */
    public function getReminderReportsSent()
    {
        return $this->reminderReportsSent;
    }

    /**
     * @param DateTime $reminderReportsSent
     */
    public function setReminderReportsSent($reminderReportsSent)
    {
        $this->reminderReportsSent = $reminderReportsSent;
    }
}
