<?php

namespace App\Components\Forms;

use Nette\Application\UI\Control;
use ReflectionClass;

abstract class AbstractForm extends Control
{
    /** @var array */
    public $onSuccess;
    /** @var array */
    public $onError;
    /** @var boolean */
    protected $ajax = false;
    /** @var string */
    protected $templateFile = null;

    /**
     * @param mixed $ajax
     */
    public function setAjax($ajax = true)
    {
        $this->ajax = $ajax;
    }

    /**
     * @param string $templateFile
     */
    public function setTemplateFile($templateFile)
    {
        $this->templateFile = $templateFile;
    }

    public function render()
    {
        $rc = new ReflectionClass($this);
        $template = $this->template;
        if ($this->templateFile && is_file(__DIR__ . '/' . $rc->getShortName() . '/templates/' . $this->templateFile . '.latte')) {
            $template->setFile(__DIR__ . '/' . $rc->getShortName() . '/templates/' . $this->templateFile . '.latte');
        } elseif (is_file(__DIR__ . '/' . $rc->getShortName() . '/templates/default.latte')) {
            $template->setFile(__DIR__ . '/' . $rc->getShortName() . '/templates/default.latte');
        } else {
            $template->setFile(__DIR__ . '/AbstractForm.latte');
        }
        $this->template->ajax = $this->ajax;
        $template->render();
    }

    /**
     * @param array $defaults
     */
    public function setDefaults($defaults)
    {
        $rc = new ReflectionClass($this);
        if (isset($this['form'])) {
            $this['form']->setDefaults($defaults);
        } elseif (isset($this[lcfirst($rc->getShortName())])) {
            $this[lcfirst($rc->getShortName())]->setDefaults($defaults);
        }
    }
}