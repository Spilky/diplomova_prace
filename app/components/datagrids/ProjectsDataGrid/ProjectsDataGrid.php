<?php

namespace App\Components\DataGrids\ProjectsDataGrid;

use App\Components\DataGrids\AbstractDataGrid;
use App\Model\Entity\Project;
use App\Model\Repository\ProjectRepository;
use App\Model\Repository\TagRepository;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Nette\Application\ForbiddenRequestException;
use Nette\Security\User as NetteUser;
use Tracy\Debugger;
use Ublaboo\DataGrid\DataGrid;

class ProjectsDataGrid extends AbstractDataGrid
{
    /** @var NetteUser */
    private $netteUser;
    /** @var ProjectRepository */
    private $projectRepository;
    /** @var TagRepository */
    private $tagRepository;

    /**
     * ProjectsDataGrid constructor.
     * @param NetteUser $netteUser
     * @param ProjectRepository $projectRepository
     * @param TagRepository $tagRepository
     */
    public function __construct(NetteUser $netteUser, ProjectRepository $projectRepository, TagRepository $tagRepository)
    {
        $this->netteUser = $netteUser;
        $this->projectRepository = $projectRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * @return DataGrid
     */
    public function createComponentDataGrid()
    {
        $grid = parent::create();
        $grid->setColumnsHideable();

        $grid->addColumnText('name', 'Název')
            ->setSortable()
            ->setFilterText();

        $grid->addColumnText('student', 'Student', 'student.name')
            ->setSortable('st.name')
            ->setFilterText('st.name');

        $tags = TagRepository::getIdIndexedArrayOfNames(
            $this->tagRepository->getByParameters(['group' => $this->netteUser->identity->group])
        );
        $grid->addColumnText('tags', 'Tagy')
            ->setRenderer(function (Project $project) {
                $tags = array_map(function ($tag) {return $tag->getName();}, $project->getTags()->toArray());
                return implode(', ', $tags);
            })
            ->setFilterMultiSelect($tags)
            ->setCondition(function (QueryBuilder $qb, $tags) {
                $qb->andWhere('ta.id IN (:tags)')->setParameter('tags', $tags);
            });

        $grid->addColumnText('type', 'Typ práce')
            ->setReplacement(ProjectRepository::getTypes())
            ->setDefaultHide()
            ->setSortable()
            ->setFilterSelect(array_merge([null => '-'], ProjectRepository::getTypes()));

        $typesSecretTask = [false => 'Veřejná práce', true => 'Tajná práce'];
        $grid->addColumnText('secretTask', 'Viditelnost')
            ->setReplacement($typesSecretTask)
            ->setDefaultHide()
            ->setSortable()
            ->setFilterSelect(array_merge([null => '-'], $typesSecretTask));

        $typesActiveTask = [false => 'Ano', true => 'Ne'];
        $grid->addColumnText('notActive', 'Aktivní')
            ->setReplacement($typesActiveTask)
            ->setSortable()
            ->setFilterSelect(array_merge([null => '-'], $typesActiveTask));

        $grid->addAction('detail', 'Detail', 'Project:detail')
            ->setIcon('eye');

        $grid->addAction('edit', 'Editovat', 'Project:edit')
            ->setIcon('pencil');

        $grid->addAction('delete', 'Smazat', 'delete!')
            ->setClass('ajax btn btn-xs btn-danger')
            ->setIcon('trash')
            ->setConfirm('Opravdu chcete smazat projekt %s?', 'name');

        $grid->addToolbarButton('Project:add', 'Přidat projekt')
            ->setIcon('plus');

        $grid->addToolbarButton('Project:importovat', 'Importovat projekty')
            ->setIcon('share-square-o');

        $grid->setDataSource($this->projectRepository->getQB('pr')
            ->join('pr.student', 'st')
            ->join('pr.group', 'gr')
            ->leftJoin('pr.tags', 'ta')
            ->where('gr.id = :group')
            ->setParameter('group', $this->netteUser->identity->group));

        return $grid;
    }

    /**
     * @param int $id
     */
    public function handleDelete($id)
    {
        $project = $this->projectRepository->getById($id);

        if ($project) {
            if ($this->netteUser->id != $project->getGroup()->getSupervisor()->getId()) {
                new ForbiddenRequestException();
            }

            try {
                $this->projectRepository->deleteWhere(['id' => $id]);
                $this->flashMessage('Projekt byl úspěšně smazán.', 'success');
            } catch (Exception $e) {
                Debugger::log($e);
                $this->flashMessage('Projekt se nepodařilo smazat. Zkuste to prosím znovu.', 'danger');
            }

            $this->redrawControl('flashes');
            $this['dataGrid']->reload();
        }
    }
}