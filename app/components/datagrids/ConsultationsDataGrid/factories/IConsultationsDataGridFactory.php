<?php

namespace App\Components\DataGrids\ConsultationsDataGrid\Factories;

use App\Components\DataGrids\ConsultationsDataGrid\ConsultationsDataGrid;

interface IConsultationsDataGridFactory
{
    /**
     * @param int $supervisorId
     * @return ConsultationsDataGrid
     */
    public function create($supervisorId);
}
