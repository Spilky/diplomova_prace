<?php

namespace App\Model\Mail;

use Nette\Mail\Message;
use Ublaboo\Mailing\IComposableMail;

class ReportProblemMail extends AbstractMail implements IComposableMail
{
    /**
     * @param Message $message
     * @param null|array $params
     */
    public function compose(Message $message, $params = NULL)
    {
        $message->setFrom($params['sender']);
        $message->addReplyTo($params['sender']);
        $message->addTo(self::ADMIN_MAIL);
        $message->setSubject('Hlášení problému' . self::SUBJECT_TAIL);
    }
}
