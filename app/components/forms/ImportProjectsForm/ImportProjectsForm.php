<?php

namespace App\Components\Forms\AddEditProjectForm;

use App\Components\Forms\AbstractForm;
use App\Model\Entity\Project;
use App\Model\Entity\User;
use App\Model\Mail\AddUserMail;
use App\Model\Mail\ProjectAssignedMail;
use App\Model\Repository\GroupRepository;
use App\Model\Repository\ProjectRepository;
use App\Model\Repository\UserRepository;
use Exception;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Security\Passwords;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Json;
use Nette\Utils\Random;
use Tracy\Debugger;
use Ublaboo\Mailing\MailFactory;

class ImportProjectsForm extends AbstractForm
{
    /** @var NetteUser */
    private $netteUser;
    /** @var GroupRepository */
    private $groupRepository;
    /** @var ProjectRepository */
    private $projectRepository;
    /** @var UserRepository */
    private $userRepository;
    /** @var MailFactory */
    private $mailFactory;

    /**
     * AddEditProjectForm constructor.
     * @param NetteUser $netteUser
     * @param GroupRepository $groupRepository
     * @param ProjectRepository $projectRepository
     * @param UserRepository $userRepository
     * @param MailFactory $mailFactory
     */
    public function __construct(
        NetteUser $netteUser,
        GroupRepository $groupRepository,
        ProjectRepository $projectRepository,
        UserRepository $userRepository,
        MailFactory $mailFactory
    ) {
        $this->netteUser = $netteUser;
        $this->groupRepository = $groupRepository;
        $this->projectRepository = $projectRepository;
        $this->userRepository = $userRepository;
        $this->mailFactory = $mailFactory;
    }

    /**
     * @return Form
     */
    public function createComponentStep1Form()
    {
        $form = new Form;

        $form->addTextArea('text');

        $form->addUpload('file', 'Soubor')
            ->addConditionOn($form['text'], ~Form::FILLED)
                ->setRequired('Pokud není vyplněný obsah JSON souboru, musí být zvolen soubor.')
                ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 10 MB.', 64 * 1024 * 1024 /* v bytech */);

        $form->addSubmit('submit', 'Importovat');

        $form->onValidate[] = array($this, 'step1FormValidate');
        $form->onSuccess[] = array($this, 'step1FormSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     */
    public function step1FormValidate(Form $form)
    {
        $values = $form->getValues();

        if ($values['file']->isOk()) {
            $jsonText = $values['file']->getContents();
        } elseif ($values['file']->getError() == UPLOAD_ERR_NO_FILE) {
            $jsonText = $values['text'];
        } else {
            $form->addError('Zadaný JSON soubor není validní, vložte prosím validní JSON.');
            return;
        }

        if (is_null($this->checkAndDecodeJson($jsonText))) {
            $form->addError('Zadaný obsah JSON souboru není validní, vložte prosím validní JSON.');
        }
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function step1FormSucceeded(Form $form, ArrayHash $values)
    {
        if ($values['file']->isOk()) {
            $jsonText = $values['file']->getContents();
        } else {
            $jsonText = $values['text'];
        }

        $json = $this->checkAndDecodeJson($jsonText);

        $newProjects = [];
        $updateProjects = [];
        $replicator = [];
        foreach ($json['data'] as $project) {
            if (is_null($this->projectRepository->getOneByParameters([
                'group' => $this->netteUser->identity->group,
                'wisId' => $project['id']
            ]))) {
                $newProjects[$project['id']] = $project;
            } else {
                $updateProjects[$project['id']] = $project;
            }
            $replicator[$project['id']] = [
                'import' => true,
                'json' => Json::encode($project)
            ];
        }

        $this['step2Form']['projects']->setValues($replicator);
        $this->template->newProjects = $newProjects;
        $this->template->updateProjects = $updateProjects;
    }

    public function createComponentStep2Form()
    {
        $form = new Form();

        $form->addDynamic('projects', function (Container $container) {
            $container->addCheckbox('import');

            $container->addHidden('json');
        });

        $form->addSubmit('submit', 'Importovat');

        $form->onSuccess[] = array($this, 'step2FormSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function step2FormSucceeded(Form $form, ArrayHash $values)
    {
        $newProjects = [];
        $updateProjects = [];
        $mailProjects = [];
        $students = [];
        $unregistered = [];
        $passwords = [];
        foreach ($values['projects'] as $id => $project) {
            if ($project['import']) {
                $json = Json::decode($project['json'], Json::FORCE_ARRAY);
                $student = $this->userRepository->getOneByParameters(['email' => $json['login'].'@stud.fit.vutbr.cz']);
                if (is_null($student)) {
                    $email = $json['login'].'@stud.fit.vutbr.cz';
                    $passwords[$email] = Random::generate();
                    $student = new User([
                        'name' => $json['student'],
                        'email' => $email,
                        'role' => User::ROLE_STUDENT,
                        'password' => Passwords::hash($passwords[$email])
                    ]);
                    $students[] = $student;
                } elseif ($student->getRole() == User::ROLE_UNREGISTERED) {
                    $passwords[$student->getEmail()] = Random::generate();
                    $student->setName($json['student']);
                    $student->setRole(User::ROLE_STUDENT);
                    $student->setPassword(Passwords::hash($passwords[$student->getEmail()]));
                    $unregistered[] = $student;
                }

                $existingProject = $this->projectRepository->getOneByParameters([
                    'group' => $this->netteUser->identity->group,
                    'wisId' => $id
                ]);
                if (is_null($existingProject)) {
                    $newProjects[] = new Project([
                        'student' => $student,
                        'group' => $this->groupRepository->getById($this->netteUser->identity->group),
                        'name' => $json['title'],
                        'task' => $json['text'],
                        'literature' => isset($json['literature']) ? $json['literature'] : '',
                        'type' => $this->getDatabaseProjectType($json['type']),
                        'wisId' => $json['id']
                    ]);
                } else {
                    if ($student->getId() != $existingProject->getStudent()->getId()) {
                        $mailProjects[] = $existingProject;
                    }
                    $existingProject->populateData([
                        'student' => $student,
                        'name' => $json['title'],
                        'task' => $json['text'],
                        'literature' => isset($json['literature']) ? $json['literature'] : '',
                        'type' => $this->getDatabaseProjectType($json['type'])
                    ]);
                    $updateProjects[] = $existingProject;
                }
            }
        }
        $mailProjects = array_merge($mailProjects, $newProjects);

        try {
            $this->userRepository->insert($students);
            $this->userRepository->update($unregistered);
            foreach (array_merge($students, $unregistered) as $student) {
                $this->mailFactory->createByType(AddUserMail::class, [
                    'user' => $student,
                    'password' => $passwords[$student->getEmail()]
                ])->send();
            }
            $this->projectRepository->insert($newProjects);
            $this->projectRepository->update($updateProjects);
            foreach ($mailProjects as $project) {
                $this->mailFactory->createByType(ProjectAssignedMail::class, ['project' => $project])->send();
            }
        } catch (Exception $e) {
            $this->onError($this, $e);
            Debugger::log($e);
            return;
        }

        $this->onSuccess($this);
    }

    /**
     * @param string $json
     * @return string|null
     */
    private function checkAndDecodeJson($json)
    {
        try {
            $decode = Json::decode($json, Json::FORCE_ARRAY);
        } catch (Exception $e) {
            try {
                //replace white chars
                $pattern = '/\s+/';
                $replace = ' ';
                $json = preg_replace($pattern, $replace, $json);
                //replace open braces
                $pattern = '/"([^"]*)"(\s*){/';
                $replace = '"${1}": {';
                $json = preg_replace($pattern, $replace, $json);
                //replace values
                $pattern = '/"([^"]*)"\s*"([^"]*)"/';
                $replace = '"${1}": "${2}",';
                $json = preg_replace($pattern, $replace, $json);
                //remove trailing commas
                $pattern = '/,(\s*[}\]])/';
                $replace = '${1}';
                $json = preg_replace($pattern, $replace, $json);
                //add commas
                $pattern = '/([}\]])(\s*)("[^"]*":\s*)?([{\[])/';
                $replace = '${1},${2}${3}${4}';
                $json = preg_replace($pattern, $replace, $json);
                //object as value
                $pattern = '/}(\s*"[^"]*":)/';
                $replace = '},${1}';
                $json = preg_replace($pattern, $replace, $json);

                $decode = Json::decode($json, Json::FORCE_ARRAY);
            } catch (Exception $e) {
                return null;
            }
        }

        if (!isset($decode['data']) || count($decode['data']) == 0) {
            return null;
        }

        $required = ['student', 'login', 'title', 'text'];
        foreach ($decode['data'] as $project) {
            if(count(array_intersect_key(array_flip($required), $project)) !== count($required)) {
                return null;
            }
        }

        return $decode;
    }

    /**
     * @param string $jsonType
     * @return string
     */
    private function getDatabaseProjectType($jsonType)
    {
        switch ($jsonType) {
            case 'DP':
                return Project::TYPE_MASTER_THESIS;
            case 'BP':
                return Project::TYPE_BACHELOR_THESIS;
            default:
                return Project::TYPE_PROJECT;
        }
    }
}
