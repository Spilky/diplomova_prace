<?php

namespace App\Model\Mail;

use App\Model\Entity\User;
use Nette\Mail\Message;
use Ublaboo\Mailing\IComposableMail;

class ReminderConsultationsMail extends AbstractMail implements IComposableMail
{
    /**
     * @param Message $message
     * @param null|array $params
     */
    public function compose(Message $message, $params = NULL)
    {
        /** @var User $user */
        $user = $params['project']->getStudent();

        $message->setFrom(self::NO_REPLY_MAIL);
        $message->addReplyTo(self::NO_REPLY_MAIL);
        $message->addTo($user->getEmail(), $user->getName());
        $message->setSubject('Dlouho jste nebyl(a) na konzultaci' . self::SUBJECT_TAIL);
    }
}
