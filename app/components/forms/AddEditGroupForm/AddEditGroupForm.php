<?php

namespace App\Components\Forms\AddEditGroupForm;

use App\Components\Forms\AbstractForm;
use App\Model\Entity\Group;
use App\Model\Repository\GroupRepository;
use App\Model\Repository\UserRepository;
use DateTime;
use Exception;
use Nette\Application\UI\Form;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class AddEditGroupForm extends AbstractForm
{
    /** @var NetteUser */
    private $netteUser;
    /** @var GroupRepository */
    private $groupRepository;
    /** @var UserRepository */
    private $userRepository;

    /**
     * AddEditGroupForm constructor.
     * @param NetteUser $netteUser
     * @param GroupRepository $groupRepository
     * @param UserRepository $userRepository
     */
    public function __construct(NetteUser $netteUser, GroupRepository $groupRepository, UserRepository $userRepository)
    {
        $this->netteUser = $netteUser;
        $this->groupRepository = $groupRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form;
        
        $form->addText('name', 'Jméno:')
            ->setRequired('Vložte prosím jméno.');

        $form->addText('dateFrom', 'Termín od:')
            ->setRequired('Vložte prosím termín od.');

        $form->addText('dateTo', 'Termín do:')
            ->setRequired('Vložte prosím termín do.');

        $form->addCheckbox('reminderConsultations', 'Upozorňovat studenta, pokud několik týdnů nebyl na konzultaci?')
            ->addCondition(Form::EQUAL, true)
            ->toggle('reminder-consultations');

        $form->addText('reminderConsultationsWeeks', 'Počet týdnů')
            ->addConditionOn($form['reminderConsultations'], Form::EQUAL, true)
                ->setRequired('Vložte prosím počet týdnů.')
                ->addRule(Form::INTEGER, 'Vložte prosím celé číslo jako počet týdnů.');

        $form->addCheckbox('reminderReports', 'Upozorňovat studenta, pokud několik týdnů nezadal report?')
            ->addCondition(Form::EQUAL, true)
            ->toggle('reminder-reports');

        $form->addText('reminderReportsWeeks', 'Počet týdnů')
            ->addConditionOn($form['reminderReports'], Form::EQUAL, true)
            ->setRequired('Vložte prosím počet týdnů.')
            ->addRule(Form::INTEGER, 'Vložte prosím celé číslo jako počet týdnů.');

        $form->addHidden('id');

        $form->addSubmit('submit', 'Uložit');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        $values['dateFrom'] = new DateTime($values['dateFrom']);
        $values['dateTo'] = new DateTime($values['dateTo']);

        if (!$values['reminderConsultations']) {
            $values['reminderConsultationsWeeks'] = null;
        }

        if (!$values['reminderReports']) {
            $values['reminderReportsWeeks'] = null;
        }

        unset($values['reminderConsultations'], $values['reminderReports']);

        if (empty($values['id'])) {
            $supervisor = $this->userRepository->getById($this->netteUser->id);
            $values['supervisor'] = $supervisor;
            $group = new Group($values);
        }

        try {
            if (empty($values['id'])) {
                $this->groupRepository->insert($group);
            } else {
                $this->groupRepository->updateWhere($values, ['id' => $values['id']]);
            }
        } catch (Exception $e) {
            $this->onError($this, $e);
            Debugger::log($e);
            return;
        }

        $this->onSuccess($this);
    }
}
