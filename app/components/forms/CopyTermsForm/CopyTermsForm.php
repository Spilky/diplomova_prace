<?php

namespace App\Components\Forms\AddEditProjectForm;

use App\Components\Forms\AbstractForm;
use App\Model\Repository\GroupRepository;
use App\Model\Repository\TermRepository;
use Exception;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class CopyTermsForm extends AbstractForm
{
    /** @var NetteUser */
    private $netteUser;
    /** @var GroupRepository */
    private $groupRepository;
    /** @var TermRepository */
    private $termRepository;

    /**
     * AddEditProjectForm constructor.
     * @param NetteUser $netteUser
     * @param GroupRepository $groupRepository
     * @param TermRepository $termRepository
     */
    public function __construct(
        NetteUser $netteUser,
        GroupRepository $groupRepository,
        TermRepository $termRepository
    ) {
        $this->netteUser = $netteUser;
        $this->groupRepository = $groupRepository;
        $this->termRepository = $termRepository;
    }

    /**
     * @return Form
     */
    public function createComponentStep1Form()
    {
        $form = new Form;

        $items = [];
        $groups = $this->groupRepository->getAll();
        foreach ($groups as $group) {
            $items[$group->getSupervisor()->getName()][$group->getId()] = $group->getName();
        }
        ksort($items);
        $form->addSelect('group', 'Projektová skupina:', ['' => ''] + $items)
            ->setRequired('Vyberte prosím projektovou skupinu');

        $form->addSubmit('submit', 'Zobrazit termíny');

        $form->onSuccess[] = array($this, 'step1FormSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function step1FormSucceeded(Form $form, ArrayHash $values)
    {
        $terms = [];
        $replicator = [];
        $group = $this->groupRepository->getById($this->netteUser->identity->group);
        foreach ($this->termRepository->getByParameters(['group' => $values['group'], 'public' => true]) as $term) {
            if ($term->getDate() < $term->getGroup()->getDateFrom()) {
                continue;
            }

            $interval = $term->getDate()->diff($term->getGroup()->getDateFrom());
            $newDate = clone $group->getDateFrom();
            $newDate->modify('+' . $interval->days . ' days');
            if ($newDate > $group->getDateTo()) {
                break;
            }

            $terms[$term->getId()] = $term->getAsArray();
            $replicator[$term->getId()] = [
                'copy' => true
            ];
            $terms[$term->getId()]['date'] = $newDate;
        }

        $this['step2Form']['terms']->setValues($replicator);
        $this->template->terms = $terms;
    }

    /**
     * @return Form
     */
    public function createComponentStep2Form()
    {
        $form = new Form();

        $form->addDynamic('terms', function (Container $container) {
            $container->addCheckbox('copy');
        });

        $form->addSubmit('submit', 'Kopírovat termíny');

        $form->onSuccess[] = array($this, 'step2FormSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function step2FormSucceeded(Form $form, ArrayHash $values)
    {
        $terms = [];
        $group = $this->groupRepository->getById($this->netteUser->identity->group);
        foreach ($values['terms'] as $id => $copy) {
            if ($copy) {
                $term = $this->termRepository->getById($id);
                $interval = $term->getDate()->diff($term->getGroup()->getDateFrom());
                $newDate = clone $group->getDateFrom();
                $newDate->modify('+' . $interval->days . ' days');

                $newTerm = clone $term;
                $newTerm->setGroup($group);
                $newTerm->setDate($newDate);
                $terms[] = $newTerm;
            }
        }

        try {
            $this->termRepository->insert($terms);
        } catch (Exception $e) {
            $this->onError($this, $e);
            Debugger::log($e);
            return;
        }

        $this->onSuccess($this);
    }
}
