<?php

namespace App\Presenters;

use App\Components\DataGrids\GroupsDataGrid\Factories\IGroupsDataGridFactory;
use App\Components\DataGrids\GroupsDataGrid\GroupsDataGrid;
use App\Components\Forms\AddEditGroupForm\AddEditGroupForm;
use App\Components\Forms\AddEditGroupForm\Factories\IAddEditGroupFormFactory;
use App\Model\Repository\GroupRepository;
use Nette\Application\BadRequestException;

class GroupPresenter extends BasePresenter
{
    /** @var GroupRepository @inject */
    public $groupRepository;
    /** @var IGroupsDataGridFactory @inject */
    public $groupsDataGridFactory;
    /** @var IAddEditGroupFormFactory @inject */
    public $addEditGroupFormFactory;

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionEdit($id = 0)
    {
        $group = $this->groupRepository->getById($id);
        if (is_null($group)) {
            throw new BadRequestException;
        }

        $data = $group->getAsArray();
        $data['dateFrom'] = $group->getDateFrom()->format('j.n.Y');
        $data['dateTo'] = $group->getDateTo()->format('j.n.Y');
        $data['reminderConsultations'] = boolval($group->getReminderConsultationsWeeks());
        $data['reminderReports'] = boolval($group->getReminderReportsWeeks());
        $this['addEditGroupForm']->setDefaults($data);
    }

    /**
     * @return GroupsDataGrid
     */
    protected function createComponentGroupsDataGrid()
    {
        $control = $this->groupsDataGridFactory->create();
        return $control;
    }

    /**
     * @return AddEditGroupForm
     */
    protected function createComponentAddEditGroupForm()
    {
        $form = $this->addEditGroupFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'Skupina projektů byla úspěšně ' .
                ($this->getAction() == 'add' ?  'vložena.' : 'editována.'), 'success');
            $this->redirect('Group:');
        };
        $form->onError[] = function ($form, $error) {
            $this->flashMessage(
                'Skupinu projektů se nepodařilo ' .
                ($this->getAction() == 'add' ?  'vložit.' : 'editovat.') .
                ' Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }
}
