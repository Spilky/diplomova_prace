<?php

namespace App\Model\Repository;

use App\Model\Entity\Report;
use Kdyby\Doctrine\EntityManager;

class ReportRepository extends AbstractRepository
{
    /**
     * ReportRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entityRepository = $this->entityManager->getRepository(Report::class);
    }

    /**
     * @return Report[]
     */
    public function getAll()
    {
        return parent::getAll();
    }

    /**
     * @param $id
     * @return Report
     */
    public function getById($id)
    {
        return parent::getById($id);
    }

    /**
     * @param $parameters
     * @return Report[]
     */
    public function getByParameters($parameters)
    {
        return parent::getByParameters($parameters);
    }

    /**
     * @param $parameters
     * @return Report
     */
    public function getOneByParameters($parameters)
    {
        return parent::getOneByParameters($parameters);
    }

    public static function getSmileys()
    {
        return [
            Report::SMILEY_HAPPY => 'happy',
            Report::SMILEY_NEUTRAL => 'neutral',
            Report::SMILEY_SAD => 'sad',
            Report::SMILEY_HOLIDAYS => 'holidays'
        ];
    }
}