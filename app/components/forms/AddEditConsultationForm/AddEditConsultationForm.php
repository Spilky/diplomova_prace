<?php

namespace App\Components\Forms\AddEditConsultationForm;

use App\Components\Forms\AbstractForm;
use App\Model\Entity\Project;
use App\Model\Entity\TimeLineItem;
use App\Model\Repository\GroupRepository;
use App\Model\Repository\ProjectRepository;
use App\Model\Repository\TimeLineItemRepository;
use DateTime;
use Exception;
use Nette\Application\UI\Form;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Tracy\Debugger;

class AddEditConsultationForm extends AbstractForm
{
    /** @var NetteUser */
    private $netteUser;
    /** @var ProjectRepository */
    private $projectRepository;
    /** @var TimeLineItemRepository */
    private $timeLineItemRepository;
    /** @var Project */
    private $project;
    /** @var TimeLineItem */
    private $timeLineItem = null;
    /** @var Html */
    private $cancelButton;

    /**
     * AddEditConsultationForm constructor.
     * @param Project $project
     * @param NetteUser $netteUser
     * @param ProjectRepository $projectRepository
     * @param TimeLineItemRepository $timeLineItemRepository
     */
    public function __construct(
        Project $project,
        NetteUser $netteUser,
        ProjectRepository $projectRepository,
        TimeLineItemRepository $timeLineItemRepository
    ) {
        $this->project = $project;
        $this->netteUser = $netteUser;
        $this->projectRepository = $projectRepository;
        $this->timeLineItemRepository = $timeLineItemRepository;
    }

    /**
     * @param int|TimeLineItem $timeLineItem
     */
    public function setTimeLineItem($timeLineItem)
    {
        if (!($timeLineItem instanceof TimeLineItem)) {
            $timeLineItem = $this->timeLineItemRepository->getById($timeLineItem);
        }

        $this->timeLineItem = $timeLineItem;
    }

    /**
     * @param Html $cancelButton
     */
    public function setCancelButton($cancelButton)
    {
        $this->cancelButton = $cancelButton;
    }

    public function render()
    {
        $data = [];
        if ($this->timeLineItem) {
            $begin = $this->timeLineItem->getDate();
            $end = clone $begin;
            $end->modify('+7 days');
            $this['form']['date']->setItems([
                $begin->format('Y-m-d') => $begin->format('j.n.Y') . ' - ' . $end->format('j.n.Y')
            ])->setDisabled();
            $data = $this->timeLineItem->getAsArray();
            unset($data['date']);
        }

        $data['projectId'] = $this->project->getId();
        $this['form']->setDefaults($data);

        if ($this->cancelButton) {
            $this->template->cancelButton = $this->cancelButton;
        }

        parent::render();
    }

    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form;

        $date = new DateTime();
        while ($date->format('w') != 1) {
            $date->modify( '-1 day' );
        }
        $weeks = GroupRepository::getListOfWeeksByGroup($this->project->getGroup());
        $form->addSelect('date', 'Týden', $weeks)
            ->setRequired('Vyberte si prosím týden.')
            ->setDefaultValue(in_array($date->format('Y-m-d'), array_keys($weeks)) ? $date->format('Y-m-d') : null);

        $form->addTextArea('text', 'Text');

        $form->addHidden('projectId');

        $form->addHidden('id');

        $form->addSubmit('submit', 'Uložit');

        $form->onSuccess[] = array($this, 'formSucceeded');
        $form->onValidate[] = array($this, 'formValidate');
        return $form;
    }

    /**
     * @param Form $form
     */
    public function formValidate(Form $form)
    {
        if ($this->timeLineItem) {
            $form->getComponent('date')->cleanErrors();
        }
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        $project = $this->projectRepository->getById($values['projectId']);
        unset($values['projectId']);

        if (empty($values['id'])) {
            $values['project'] = $project;
            $values['date'] = new DateTime($values['date']);
            $values['type'] = TimeLineItem::TYPE_CONSULTATION;
            $report = new TimeLineItem($values);
        } else {
            unset($values['date']);
        }

        try {
            if (empty($values['id'])) {
                $this->timeLineItemRepository->insert($report);
            } else {
                $this->timeLineItemRepository->updateWhere($values, ['id' => $values['id']]);
            }
        } catch (Exception $e) {
            $this->onError($this, $e);
            Debugger::log($e);
            return;
        }

        $this->onSuccess($this);
    }
}
