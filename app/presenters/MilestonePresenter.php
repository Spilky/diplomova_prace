<?php

namespace App\Presenters;

use App\Components\Forms\AddEditConsultationForm\AddEditConsultationForm;
use App\Components\Forms\AddEditMilestoneForm\Factories\IAddEditMilestoneFormFactory;
use App\Model\Entity\Project;
use App\Model\Entity\TimeLineItem;
use App\Model\Entity\User;
use App\Model\Repository\TimeLineItemRepository;
use Exception;
use Nette\Application\BadRequestException;
use Nette\Application\ForbiddenRequestException;
use Tracy\Debugger;

class MilestonePresenter extends BasePresenter
{
    /** @var TimeLineItemRepository @inject */
    public $timeLineItemRepository;
    /** @var IAddEditMilestoneFormFactory @inject */
    public $addEditMilestoneFormFactory;

    /**
     * @param Project $project
     * @param null|TimeLineItem $milestone
     * @throws ForbiddenRequestException
     */
    private function checkRights($project, $milestone = null)
    {
        $group = $project->getGroup();
        if ((!is_null($milestone) && $project->getId() != $milestone->getProject()->getId()) ||
            ($this->user->isInRole(User::ROLE_STUDENT) && $project->getStudent()->getId() != $this->user->id) ||
            ($this->user->isInRole(User::ROLE_SUPERVISOR) && $group->getSupervisor()->getId() != $this->user->id)) {
            throw new ForbiddenRequestException;
        }
    }

    /**
     * @param int $projectId
     * @throws BadRequestException
     */
    public function actionAdd($projectId = 0)
    {
        $project = $this->projectRepository->getById($projectId);
        if (is_null($project)) {
            throw new BadRequestException;
        }

        $this->checkRights($project);

        $this['addEditMilestoneForm']->setDefaults(['projectId' => $project->getId()]);
    }

    /**
     * @param int $id
     * @param int $projectId
     * @throws BadRequestException
     */
    public function actionEdit($id = 0, $projectId = 0)
    {
        $timeLineItem = $this->timeLineItemRepository->getById($id);
        $project = $this->projectRepository->getById($projectId);
        if (is_null($project) || is_null($timeLineItem) || $timeLineItem->getType() != TimeLineItem::TYPE_MILESTONE) {
            throw new BadRequestException;
        }

        $this->checkRights($project, $timeLineItem);

        $this['addEditMilestoneForm']->setTimeLineItem($timeLineItem);
    }

    /**
     * @param int $id
     * @param int $projectId
     * @throws BadRequestException
     */
    public function actionDelete($id = 0, $projectId = 0)
    {
        $timeLineItem = $this->timeLineItemRepository->getById($id);
        $project = $this->projectRepository->getById($projectId);
        if (is_null($project) || is_null($timeLineItem) || $timeLineItem->getType() != TimeLineItem::TYPE_MILESTONE) {
            throw new BadRequestException;
        }

        $this->checkRights($project, $timeLineItem);

        try {
            $this->timeLineItemRepository->delete($timeLineItem);
        } catch (Exception $e) {
            Debugger::log($e);
            $this->flashMessage('Milník se nepodařilo smazat. Zkuste to znovu prosím.', 'danger');
            return;
        }

        $this->flashMessage('Milník byl úspěšně smazán.', 'success');
        $this->redirect('Project:detail', ['id' => $project->getId()]);
    }

    /**
     * @return AddEditConsultationForm
     */
    protected function createComponentAddEditMilestoneForm()
    {
        $project = $this->projectRepository->getById($this->getParameter('projectId'));
        $form = $this->addEditMilestoneFormFactory->create($project);
        $form->setTemplateFile('box');
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'Milník byl úspěšně ' .
                ($this->getAction() == 'add' ?  'vložen.' : 'editován.'), 'success');
            $this->redirect('Project:detail', ['id' => $this->getParameter('projectId')]);
        };
        $form->onError[] = function ($form, $error) {
            $this->flashMessage(
                'Milník se nepodařilo ' .
                ($this->getAction() == 'add' ?  'vložit.' : 'editovat.') .
                ' Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }
}
