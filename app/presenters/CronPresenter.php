<?php

namespace App\Presenters;

use App\Model\Entity\TimeLineItem;
use App\Model\Mail\ReminderConsultationsMail;
use App\Model\Mail\ReminderReportsMail;
use App\Model\Repository\GroupRepository;
use App\Model\Repository\ProjectRepository;
use App\Model\Repository\ReportRepository;
use App\Model\Repository\TimeLineItemRepository;
use DateTime;
use Exception;
use Nette\Application\ForbiddenRequestException;
use Nette\Application\UI\Presenter;
use Tracy\Debugger;
use Ublaboo\Mailing\MailFactory;

class CronPresenter extends Presenter
{
    const TOKEN = '6e9EKDK8s7XSvauAhHjxDusCBWF7qxmTZpBJH5bq';

    /** @var GroupRepository @inject */
    public $groupRepository;
    /** @var ProjectRepository @inject */
    public $projectRepository;
    /** @var ReportRepository @inject */
    public $reportRepository;
    /** @var TimeLineItemRepository @inject */
    public $timeLineItemRepository;
    /** @var MailFactory @inject */
    public $mailFactory;

    /**
     * @param int $id
     * @throws ForbiddenRequestException
     */
    public function actionSendReminders($id)
    {
        if ($id != self::TOKEN) {
            throw new ForbiddenRequestException;
        }

        $now = new DateTime();
        $monday = clone $now;
        while($monday->format('N') != 1) {
            $monday->modify('-1 day');
        }
        $activeGroups = $this->groupRepository->getByParameters(['dateFrom <=' => $now, 'dateTo >=' => $now]);

        foreach ($activeGroups as $group) {
            foreach ($this->projectRepository->getByParameters(['group' => $group->getId()]) as $project) {
                if ($project->getNotActive()) {
                    continue;
                }

                if ($group->getReminderConsultationsWeeks()) {
                    $conversationsOk = false;
                    $conversationDate = clone $monday;
                    $conversationDate->modify('-14 days');
                    for ($i = 0; $i < $group->getReminderConsultationsWeeks(); $i++) {
                        if ($conversationDate < $group->getDateFrom()) {
                            $conversationsOk = true;
                            break;
                        }
                        if ($this->timeLineItemRepository->getOneByParameters(['project' => $project->getId(), 'type' => TimeLineItem::TYPE_CONSULTATION, 'date' => $conversationDate])) {
                            $conversationsOk = true;
                            break;
                        }
                        $conversationDate->modify('-7 days');
                    }

                    $params = [
                        'group' => $group,
                        'project' => $project
                    ];

                    $lastMonday = clone $monday;
                    $lastMonday->modify('-7 days');
                    if (!$conversationsOk &&
                        (is_null($project->getReminderConsultationsSent()) ||
                            $lastMonday->format('Y-m-d') == $project->getReminderConsultationsSent()->format('Y-m-d') ||
                            $project->getReminderConsultationsSent() < $lastMonday)) {
                        try {
                            $this->mailFactory->createByType(ReminderConsultationsMail::class, $params)->send();
                            $project->setReminderConsultationsSent($monday);
                            $this->projectRepository->update($project);
                        } catch (Exception $e) {
                            Debugger::log($e);
                            return;
                        }
                    }
                }

                if ($group->getReminderReportsWeeks()) {
                    $reportsOk = false;
                    $reportDate = clone $monday;
                    $reportDate->modify('-14 days');
                    for ($i = 0; $i < $group->getReminderReportsWeeks(); $i++) {
                        if ($reportDate < $group->getDateFrom()) {
                            $reportsOk = true;
                            break;
                        }
                        if ($this->reportRepository->getOneByParameters(['project' => $project->getId(), 'date' => $reportDate])) {
                            $reportsOk = true;
                            break;
                        }
                        $reportDate->modify('-7 days');
                    }

                    $params = [
                        'group' => $group,
                        'project' => $project
                    ];

                    $lastMonday = clone $monday;
                    $lastMonday->modify('-7 days');
                    if (!$reportsOk &&
                        (is_null($project->getReminderReportsSent()) ||
                            $lastMonday->format('Y-m-d') == $project->getReminderReportsSent()->format('Y-m-d') ||
                            $project->getReminderReportsSent() < $lastMonday)) {
                        try {
                            $this->mailFactory->createByType(ReminderReportsMail::class, $params)->send();
                            $project->setReminderReportsSent($monday);
                            $this->projectRepository->update($project);
                        } catch (Exception $e) {
                            Debugger::log($e);
                            return;
                        }
                    }
                }
            }
        }

        exit;
    }
}
