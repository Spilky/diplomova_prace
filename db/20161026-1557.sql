ALTER TABLE user ADD time_line_sort VARCHAR(255) NOT NULL;
UPDATE user SET time_line_sort = 'asc';
ALTER TABLE user ADD time_line_hidden LONGTEXT NOT NULL COMMENT '(DC2Type:json_array)';
