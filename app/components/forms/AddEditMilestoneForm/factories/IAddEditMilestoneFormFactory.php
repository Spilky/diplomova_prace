<?php

namespace App\Components\Forms\AddEditMilestoneForm\Factories;

use App\Components\Forms\AddEditMilestoneForm\AddEditMilestoneForm;
use App\Model\Entity\Project;

interface IAddEditMilestoneFormFactory
{
    /**
     * @param Project $project
     * @return AddEditMilestoneForm
     */
    public function create(Project $project);
}
