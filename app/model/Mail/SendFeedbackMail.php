<?php

namespace App\Model\Mail;

use App\Model\Entity\User;
use Nette\Mail\Message;
use Ublaboo\Mailing\IComposableMail;

class SendFeedbackMail extends AbstractMail implements IComposableMail
{
    /**
     * @param Message $message
     * @param null|array $params
     */
    public function compose(Message $message, $params = NULL)
    {
        /** @var User $sender */
        $sender = $params['sender'];

        /** @var User $receiver */
        $receiver = $params['receiver'];

        $message->setFrom($sender->getEmail(), $sender->getName());
        $message->addReplyTo($sender->getEmail(), $sender->getName());
        $message->addTo($receiver->getEmail(), $receiver->getName());
        $message->setSubject('Zpětná vazba k reportu' . self::SUBJECT_TAIL);
    }
}
