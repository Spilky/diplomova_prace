<?php

namespace App\Components\Forms\ConsultationSettingsForm;

use App\Components\Forms\AbstractForm;
use App\Model\Entity\Consultation;
use App\Model\Repository\ConsultationRepository;
use App\Model\Repository\UserRepository;
use DateInterval;
use DateTime;
use Exception;
use Nette\Application\UI\Form;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class ConsultationSettingsForm extends AbstractForm
{
    /** @var NetteUser */
    private $netteUser;
    /** @var ConsultationRepository */
    private $consultationRepository;
    /** @var UserRepository */
    private $userRepository;

    /**
     * ConsultationSettingsForm constructor.
     * @param NetteUser $netteUser
     * @param ConsultationRepository $consultationRepository
     * @param UserRepository $userRepository
     */
    public function __construct(
        NetteUser $netteUser,
        ConsultationRepository $consultationRepository,
        UserRepository $userRepository
    ) {
        $this->netteUser = $netteUser;
        $this->consultationRepository = $consultationRepository;
        $this->userRepository = $userRepository;
    }

    public function render()
    {
        parent::render();
    }

    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form;

        $form->addText('dateFrom', 'Datum od:')
            ->setRequired('Vložte datum od.');

        $form->addText('dateTo', 'Datum do:')
            ->setRequired('Vložte datum do.');

        $form->addText('timeFrom', 'Začátek konzultačních hodin:')
            ->setRequired('Vložte začátek konzultačních hodin.');

        $form->addText('count', 'Počet bloků:')
            ->addRule(Form::INTEGER, 'Vložte prosím validní celé číslo jako počet bloků.')
            ->addRule(Form::MIN, 'Vložte prosím kladné celé číslo jako počet bloků.', 1)
            ->setRequired('Vložte prosím počet bloků.');

        $form->addText('length', 'Délka bloku v min:')
            ->addRule(Form::INTEGER, 'Vložte prosím validní celé číslo jako délku bloku v minutách.')
            ->addRule(Form::MIN, 'Vložte prosím kladné celé číslo jako délku bloku v minutách.', 1)
            ->setRequired('Vložte prosím délku bloku v minutách.');

        $form->addText('repeat', 'Opakovat po počtu dní:')
            ->addRule(Form::INTEGER, 'Vložte prosím validní celé číslo jako počet dní po kterých se budou bloky opakovat.')
            ->addRule(Form::MIN, 'Vložte prosím kladné celé číslo jako počet dní po kterých se budou bloky opakovat.', 0)
            ->setRequired('Vložte prosím opakvání po počtu dní.');

        $form->addSubmit('submit', 'Uložit');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        $blocks = [];
        $from = DateTime::createFromFormat('d.m.Y H:i:s', $values['dateFrom'] . ' ' . $values['timeFrom'] . ':00');
        $to = DateTime::createFromFormat('d.m.Y H:i:s', $values['dateTo'] . ' 23:59:59');
        $intervalAdd = new DateInterval('PT' . $values['length'] . 'M');
        $intervalSub = new DateInterval('PT' . ($values['length'] * $values['count']) . 'M');
        $supervisor = $this->userRepository->getById($this->netteUser->id);

        while ($from < $to) {
            for ($i = 0; $i < $values['count']; $i++) {
                $block = new Consultation([
                    'supervisor' => $supervisor,
                    'dateFrom' => clone $from
                ]);

                $from->add($intervalAdd);
                $block->setDateTo(clone $from);
                $blocks[] = $block;
            }

            $from->sub($intervalSub);
            $from->modify('+' . $values['repeat'] . ' day');
            if ($values['repeat'] == 0) {
                break;
            }
        }

        try {
            $this->consultationRepository->insert($blocks);
        } catch (Exception $e) {
            $this->onError($this, $e);
            Debugger::log($e);
            return;
        }

        $this->onSuccess($this);
    }
}
