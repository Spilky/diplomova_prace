<?php

namespace App\Components\DataGrids\TagsDataGrid\Factories;

use App\Components\DataGrids\TagsDataGrid\TagsDataGrid;

interface ITagsDataGridFactory
{
    /**
     * @return TagsDataGrid
     */
    public function create();
}
