<?php

namespace App\Components\DataGrids\MailTemplatesDataGrid\Factories;

use App\Components\DataGrids\MailTemplatesDataGrid\MailTemplatesDataGrid;

interface IMailTemplatesDataGridFactory
{
    /**
     * @return MailTemplatesDataGrid
     */
    public function create();
}
