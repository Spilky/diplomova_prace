<?php

namespace App\Components\Forms\AddEditTermForm\Factories;

use App\Components\Forms\AddEditTermForm\AddEditTermForm;

interface IAddEditTermFormFactory
{
    /**
     * @return AddEditTermForm
     */
    public function create();
}
