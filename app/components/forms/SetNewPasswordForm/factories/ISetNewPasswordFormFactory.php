<?php

namespace App\Components\Forms\SetNewPasswordForm\Factories;

use App\Components\Forms\SetNewPasswordForm\SetNewPasswordForm;

interface ISetNewPasswordFormFactory
{
    /**
     * @return SetNewPasswordForm
     */
    public function create();
}
