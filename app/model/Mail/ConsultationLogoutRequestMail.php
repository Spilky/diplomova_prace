<?php

namespace App\Model\Mail;

use App\Model\Entity\ConsultationRequest;
use Nette\Mail\Message;
use Ublaboo\Mailing\IComposableMail;

class ConsultationLogoutRequestMail extends AbstractMail implements IComposableMail
{
    /**
     * @param Message $message
     * @param null|array $params
     */
    public function compose(Message $message, $params = NULL)
    {
        /** @var ConsultationRequest $request */
        $request = $params['request'];

        $message->setFrom(self::NO_REPLY_MAIL);
        $message->addReplyTo(self::NO_REPLY_MAIL);
        $message->addTo($request->getEmail(), $request->getName());
        $message->setSubject('Ověření odhlášení z konzultace' . self::SUBJECT_TAIL);
    }
}
