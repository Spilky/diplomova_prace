<?php

namespace App\Components\DataGrids\ConsultationsDataGrid;

use App\Components\DataGrids\AbstractDataGrid;
use App\Model\Entity\Consultation;
use App\Model\Entity\ConsultationRequest;
use App\Model\Mail\ConsultationLoginRequestMail;
use App\Model\Mail\ConsultationLogoutMail;
use App\Model\Mail\ConsultationLogoutRequestMail;
use App\Model\Repository\ConsultationRepository;
use App\Model\Repository\ConsultationRequestRepository;
use App\Model\Repository\GroupRepository;
use App\Model\Repository\ProjectRepository;
use App\Model\Repository\UserRepository;
use App\Model\Service\TokenService;
use DateTime;
use Exception;
use Nette\Application\ForbiddenRequestException;
use Nette\Application\UI\Form;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Tracy\Debugger;
use Ublaboo\DataGrid\DataGrid;
use Ublaboo\DataGrid\Localization\SimpleTranslator;
use Ublaboo\Mailing\MailFactory;

class ConsultationsDataGrid extends AbstractDataGrid
{
    const UNREGISTERED_EMAIL_POSTFIX = 'fit.vutbr.cz';

    /** @var null|DataGrid */
    private $grid = null;
    /** @var int */
    private $supervisorId;
    /** @var NetteUser */
    private $netteUser;
    /** @var ConsultationRepository */
    private $consultationRepository;
    /** @var ConsultationRequestRepository */
    private $consultationRequestRepository;
    /** @var GroupRepository */
    private $groupRepository;
    /** @var ProjectRepository */
    private $projectRepository;
    /** @var UserRepository */
    private $userRepository;
    /** @var MailFactory */
    private $mailFactory;
    /** @var TokenService */
    private $tokenService;

    /**
     * ConsultationsDataGrid constructor.
     * @param int $supervisorId
     * @param NetteUser $netteUser
     * @param ConsultationRepository $consultationRepository
     * @param ConsultationRequestRepository $consultationRequestRepository
     * @param GroupRepository $groupRepository
     * @param ProjectRepository $projectRepository
     * @param UserRepository $userRepository
     * @param MailFactory $mailFactory
     * @param TokenService $tokenService
     */
    public function __construct(
        $supervisorId,
        NetteUser $netteUser,
        ConsultationRepository $consultationRepository,
        ConsultationRequestRepository $consultationRequestRepository,
        GroupRepository $groupRepository,
        ProjectRepository $projectRepository,
        UserRepository $userRepository,
        MailFactory $mailFactory,
        TokenService $tokenService
    ) {
        $this->supervisorId = $supervisorId;
        $this->netteUser = $netteUser;
        $this->consultationRepository = $consultationRepository;
        $this->consultationRequestRepository = $consultationRequestRepository;
        $this->groupRepository = $groupRepository;
        $this->projectRepository = $projectRepository;
        $this->userRepository = $userRepository;
        $this->mailFactory = $mailFactory;
        $this->tokenService = $tokenService;
    }

    /**
     * @return DataGrid
     */
    public function createComponentDataGrid()
    {
        $grid = parent::create();

        $grid->setRememberState(false);
        $grid->setItemsPerPageList(['all']);
        $grid->setDefaultPerPage('all');

        $today = new DateTime();
        $grid->addColumnText('time', 'Čas konzultace')
            ->setFilterDateRange()
            ->setCondition(function($data, $value) {
                $from = isset($value['from']) ? DateTime::createFromFormat('j. n. Y', $value['from']) : null;
                $to = isset($value['to']) ? DateTime::createFromFormat('j. n. Y', $value['to']) : null;
                return $this->getDataSource($from, $to);
            })
            ->setAttribute('data-date-language', 'cs');

        $grid->addColumnText('name', 'Student');

        $grid->addAction('login', 'Přihlásit', 'login!')
            ->setClass('ajax btn btn-xs btn-default')
            ->setIcon('sign-in');

        $grid->allowRowsAction('login', function($item) {
            return $item['id'] > 0 &&
                ($this->netteUser->id != $this->supervisorId || !$this->netteUser->isLoggedIn()) &&
                $item['student_id'] == 0;
        });

        $grid->addAction('logout', 'Odhlásit', 'logout!')
            ->setClass('ajax btn btn-xs btn-warning')
            ->setIcon('sign-out');

        $grid->allowRowsAction('logout', function($item) {
            return $item['id'] > 0 &&
                ($this->netteUser->isLoggedIn() && $item['student_id'] == $this->netteUser->id ||
                    $this->netteUser->isLoggedIn() && $item['student_id'] != 0 && $this->netteUser->id == $this->supervisorId ||
                    !$this->netteUser->isLoggedIn() && $item['student_id'] != 0);
        });

        $grid->addAction('delete', 'Smazat', 'delete!')
            ->setClass('ajax btn btn-xs btn-danger')
            ->setIcon('trash');

        $grid->allowRowsAction('delete', function($item) {
            return $item['id'] > 0 && $this->netteUser->isLoggedIn() && $this->supervisorId == $this->netteUser->id;
        });

        if ($this->netteUser->isLoggedIn()) {
            if ($this->supervisorId != $this->netteUser->id) {
                $grid->addGroupAction('Přihlásit na vybrané')->onSelect[] = [$this, 'handleGroupLogin'];
            }
            $grid->addGroupAction('Odlásit z vybraných')->onSelect[] = [$this, 'handleGroupLogout'];
            if ($this->supervisorId == $this->netteUser->id) {
                $grid->addGroupAction('Smazat vybrané')->onSelect[] = [$this, 'handleGroupDelete'];
            }
        }

        $grid->setRowCallback(function($item, $tr) {
            if ($item['id'] < 0) {
                $tr->addClass('active-row');
                $tr->addClass('date-row');
            } else {
                $tr->addClass('time-row');
            }
        });

        if ($this->netteUser->isLoggedIn() && $this->netteUser->id == $this->supervisorId) {
            $grid->addToolbarButton('Consultation:addBlocks', 'Přidat konzultační bloky')
                ->setIcon('plus');
        }

        $grid->setDefaultFilter(['time' => ['from' => $today->format('j. n. Y'), 'to' => null]]);

        $dictionary = parent::$dictionary;
        $dictionary['ublaboo_datagrid.no_item_found_reset'] = 'Žádné položky nenalezeny. Filtr můžete uvést do defaultního nastavení';
        $dictionary['ublaboo_datagrid.reset_filter'] = 'Defaultní nastavení filtru';
        $grid->setTranslator(new SimpleTranslator($dictionary));

        $grid->setDataSource($this->getDataSource());

        $this->grid = $grid;

        return $this->grid;
    }

    /**
     * @param null|DateTime $fromDate
     * @param null|DateTime $toDate
     * @return array
     */
    private function getDataSource($fromDate = null, $toDate = null)
    {
        if ($this->netteUser->isLoggedIn() && $this->netteUser->identity->group) {
            $group = $this->groupRepository->getById($this->netteUser->identity->group);
            $projects = $this->projectRepository->getByParameters(['group' => $group->getId()]);
        }

        $qb = $this->consultationRepository->getQB('co')
            ->leftJoin('co.student', 'st')
            ->where('co.supervisor = :supervisor')
            ->setParameter('supervisor', $this->supervisorId)
            ->orderBy('co.dateFrom');

        if ($fromDate) {
            $fromDate->setTime(0, 0, 0);
            $qb->andWhere('co.dateFrom >= :from')->setParameter('from', $fromDate);
        }

        if ($toDate) {
            $toDate->setTime(23, 59, 59);
            $qb->andWhere('co.dateTo <= :to')->setParameter('to', $toDate);
        }

        $consultations = $qb->getQuery()->getResult();

        $dataSource = [];
        $date = null;
        $i = -1;
        foreach ($consultations as $consultation) {
            /** @var Consultation $consultation */
            if ($date == null || $date != $consultation->getDateFrom()->format('j.n.Y')) {
                $date = $consultation->getDateFrom()->format('j.n.Y');
                $dataSource[] = [
                    'id' => $i--,
                    'time' => $date,
                    'name' => '',
                    'student_id' => 0
                ];
            }

            $name = '';
            if ($consultation->getStudent()) {
                if (isset($projects)) {
                    foreach ($projects as $project) {
                        if ($project->getStudent()->getId() == $consultation->getStudent()->getId()) {
                            $name = Html::el('a')
                                ->href($this->presenter->link('Project:detail', ['id' => $project->getId()]))
                                ->setText($consultation->getStudent()->getName());
                            break;
                        }
                    }
                }

                if (empty($name)) {
                    $name = $consultation->getStudent()->getName();
                }
            }

            $dataSource[] = [
                'id' => $consultation->getId(),
                'time' => $consultation->getDateFrom()->format('H:i') . ' - ' . $consultation->getDateTo()->format('H:i'),
                'name' => $name,
                'student_id' => $consultation->getStudent() ? $consultation->getStudent()->getId() : 0
            ];
        }

        return $dataSource;
    }

    public function createComponentLoginForm()
    {
        $form = new Form();

        $form->addText('name', 'Jméno a příjmení')
            ->setRequired('Vložte prosím jméno a příjmení.');

        $form->addText('email', 'E-mail')
            ->setRequired('Vložte prosím e-mail.')
            ->addRule(Form::EMAIL, 'Vložte prosím validní e-mail.')
            ->addRule(Form::PATTERN, 'Vložte prosím e-mail končící na ' . self::UNREGISTERED_EMAIL_POSTFIX . '.', '.*' . self::UNREGISTERED_EMAIL_POSTFIX);

        $form->addHidden('consultationId');

        $form->addSubmit('submit', 'Přihlásit se');

        $form->onSuccess[] = array($this, 'loginFormSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function loginFormSucceeded(Form $form, $values)
    {
        $consultation = $this->consultationRepository->getById($values['consultationId']);
        unset($values['consultationId']);
        if (!$consultation || $consultation->getSupervisor()->getId() != $this->supervisorId) {
            $this->flashMessage('Bohužel, přihlášení na konzultaci nebylo povoleno nebo se nezdařilo.', 'danger');
            return;
        }

        if ($consultation->getSupervisor()->getEmail() == $values['email']) {
            $this->flashMessage('Bohužel, přihlášení na konzultaci se nezdařilo, protože zadaný e-mail je shodný s e-mailem vypisovatele konzultací.', 'warning');
            return;
        }

        if ($consultation->getStudent()) {
            $this->flashMessage('Bohužel, přihlášení na konzultaci se nezdařilo, protože vybraný blok již není volný.', 'warning');
            return;
        }

        $values['token'] = $this->tokenService->makeConsultationLoginToken();
        $values['consultation'] = $consultation;
        $values['type'] = ConsultationRequest::TYPE_LOGIN;
        $request = new ConsultationRequest($values);

        try {
            $this->consultationRequestRepository->insert($request);
            $this->mailFactory->createByType(ConsultationLoginRequestMail::class, ['request' => $request])->send();
        } catch (Exception $e) {
            $this->flashMessage('Bohužel, přihlášení na konzultaci se nezdařilo.', 'danger');
            Debugger::log($e);
            return;
        }

        $this->flashMessage('Na zadaný e-mail byla posláná zpráva s výzvou k potvrzení přihlášení na konzultaci. Po potvrzení bude přihlášení dokončeno.', 'success');
    }

    public function createComponentLogoutForm()
    {
        $form = new Form();

        $form->addText('email', 'E-mail')
            ->setRequired('Vložte prosím e-mail.')
            ->addRule(Form::EMAIL, 'Vložte prosím validní e-mail.');

        $form->addHidden('consultationId');

        $form->addSubmit('submit', 'Odhlásit se');

        $form->onSuccess[] = array($this, 'logoutFormSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function logoutFormSucceeded(Form $form, $values)
    {
        $consultation = $this->consultationRepository->getById($values['consultationId']);
        if (!$consultation || !$consultation->getStudent() || $consultation->getSupervisor()->getId() != $this->supervisorId) {
            $this->flashMessage('Bohužel, odhlášení z konzultace nebylo povoleno nebo se nezdařilo.', 'danger');
            return;
        }

        if ($consultation->getStudent()->getEmail() != $values['email']) {
            $this->flashMessage('Bohužel, odhlášení z konzultace nebylo povoleno, protože zadaná e-mailová adresa se neshoduje s e-mailovou adresou přihlášeného.', 'danger');
            return;
        }

        $values['token'] = $this->tokenService->makeConsultationLogoutToken();
        $values['consultation'] = $consultation;
        $values['type'] = ConsultationRequest::TYPE_LOGOUT;
        $request = new ConsultationRequest($values);

        try {
            $this->consultationRequestRepository->insert($request);
            $this->mailFactory->createByType(ConsultationLogoutRequestMail::class, ['request' => $request])->send();
        } catch (Exception $e) {
            $this->flashMessage('Bohužel, odhlášení z konzultace se nezdařilo.', 'danger');
            Debugger::log($e);
            return;
        }

        $this->flashMessage('Na zadaný e-mail byla posláná zpráva s výzvou k potvrzení odhlášení z konzultace. Po potvrzení bude odhlášení dokončeno.', 'success');
    }

    /**
     * @param int $id
     */
    public function handleLogin($id)
    {
        if (!$this->netteUser->isLoggedIn()) {
            $this->template->showLoginForm = true;
            $this['loginForm']['consultationId']->setValue($id);
            $this->redrawControl('forms');
            return;
        }

        if ($this->loginConsultation($id)) {
            $this->flashMessage('Přihlášení na konzultaci proběhlo úspěšně.', 'success');
        } else {
            $this->flashMessage('Bohužel, přihlášení na konzultaci nebylo povoleno nebo se nezdařilo.', 'danger');
        }

        $this->redrawControl('flashes');
        $this['dataGrid']->reload();
    }

    /**
     * @param int[] $ids
     */
    public function handleGroupLogin($ids)
    {
        $ids = array_filter($ids, function ($id) {
            return $id > 0;
        });

        if (count($ids) == 0) {
            return;
        }

        $unsuccessful = [];
        foreach ($ids as $id) {
            if (!$this->loginConsultation($id)) {
                $unsuccessful[] = $id;
            }
        }

        if (count($unsuccessful) == 0) {
            $this->flashMessage('Přihlášení na konzultace proběhlo úspěšně.', 'success');
        } elseif (count($unsuccessful) == count($ids)) {
            $this->flashMessage('Bohužel, přihlášení na konzultace nebylo povoleno nebo se nezdařilo.', 'danger');
        } else {
            $this->flashMessage('Přihlášení na některé konzultace proběhlo úspěšně. U některých nebylo povoleno nebo se nezdařilo.', 'warning');
        }

        if ($this->grid) {
            $this->grid->setDataSource($this->getDataSource());
        }
        $this->redrawControl('flashes');
        $this['dataGrid']->reload();
    }

    /**
     * @param int $id
     * @return bool
     */
    private function loginConsultation($id)
    {
        /** @var Consultation $consultation */
        $consultation = $this->consultationRepository->getById($id);
        if ($this->netteUser->id != $this->supervisorId && !$consultation->getStudent()) {
            $student = $this->userRepository->getById($this->netteUser->id);
            $consultation->setStudent($student);
            try {
                $this->consultationRepository->update($consultation);
                return true;
            } catch (Exception $e) {
                Debugger::log($e);
                return false;
            }
        } elseif ($consultation->getStudent()->getId() == $this->netteUser->id) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param int $id
     */
    public function handleLogout($id)
    {
        if (!$this->netteUser->isLoggedIn()) {
            $this->template->showLogoutForm = true;
            $this['logoutForm']['consultationId']->setValue($id);
            $this->redrawControl('forms');
            return;
        }

        if ($this->logoutConsultation($id)) {
            $this->flashMessage('Odhlášení z konzultace proběhlo úspěšně.', 'success');
        } else {
            $this->flashMessage('Bohužel, odhlášení z konzultace nebylo povoleno nebo se nezdařilo.', 'danger');
        }

        $this->redrawControl('flashes');
        $this['dataGrid']->reload();
    }

    /**
     * @param int[] $ids
     */
    public function handleGroupLogout($ids)
    {
        $ids = array_filter($ids, function ($id) {
            return $id > 0;
        });

        if (count($ids) == 0) {
            return;
        }

        $unsuccessful = [];
        foreach ($ids as $id) {
            if (!$this->logoutConsultation($id)) {
                $unsuccessful[] = $id;
            }
        }

        if (count($unsuccessful) == 0) {
            $this->flashMessage('Odhlášení z konzultací proběhlo úspěšně.', 'success');
        } elseif (count($unsuccessful) == count($ids)) {
            $this->flashMessage('Bohužel, odhlášení z konzultací nebylo povoleno nebo se nezdařilo.', 'danger');
        } else {
            $this->flashMessage('Odhlášení z některých konzultací proběhlo úspěšně. U některých nebylo povoleno nebo se nezdařilo.', 'warning');
        }

        if ($this->grid) {
            $this->grid->setDataSource($this->getDataSource());
        }
        $this->redrawControl('flashes');
        $this['dataGrid']->reload();
    }

    /**
     * @param int $id
     * @return bool
     */
    private function logoutConsultation($id)
    {
        /** @var Consultation $consultation */
        $consultation = $this->consultationRepository->getById($id);
        if ((!$this->netteUser->isInRole('student') && $this->netteUser->id == $this->supervisorId) ||
            ($consultation->getStudent() &&
            $consultation->getStudent()->getId() == $this->netteUser->id)) {
            if ($consultation->getStudent()) {
                $student = clone $consultation->getStudent();
            }
            $consultation->setStudent(null);
            try {
                $this->consultationRepository->update($consultation);
                if (isset($student)) {
                    $this->mailFactory->createByType(ConsultationLogoutMail::class, [
                        'user' => $student,
                        'consultation' => $consultation
                    ])->send();
                }
                return true;
            } catch (Exception $e) {
                Debugger::log($e);
                return false;
            }
        } elseif ($this->netteUser->isInRole('student') && !$consultation->getStudent()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param int $id
     */
    public function handleDelete($id)
    {
        if ($this->deleteConsultation($id)) {
            $this->flashMessage('Odstranění konzultace proběhlo úspěšně.', 'success');
        } else {
            $this->flashMessage('Bohužel, odstranění konzultace se nezdařilo.', 'danger');
        }

        $this->redrawControl('flashes');
        $this['dataGrid']->reload();
    }

    /**
     * @param int[] $ids
     */
    public function handleGroupDelete($ids)
    {
        $ids = array_filter($ids, function ($id) {
            return $id > 0;
        });

        if (count($ids) == 0) {
            return;
        }

        $unsuccessful = [];
        foreach ($ids as $id) {
            if (!$this->deleteConsultation($id)) {
                $unsuccessful[] = $id;
            }
        }

        if (count($unsuccessful) == 0) {
            $this->flashMessage('Odstranění konzultací proběhlo úspěšně.', 'success');
        } elseif (count($unsuccessful) == count($ids)) {
            $this->flashMessage('Bohužel, odstranění konzultací se nezdařilo.', 'danger');
        } else {
            $this->flashMessage('Odstranění některých konzultací proběhlo úspěšně. U některých se nezdařilo.', 'warning');
        }

        if ($this->grid) {
            $this->grid->setDataSource($this->getDataSource());
        }
        $this->redrawControl('flashes');
        $this['dataGrid']->reload();
    }

    /**
     * @param int $id
     * @return bool
     */
    private function deleteConsultation($id)
    {
        $consultation = $this->consultationRepository->getById($id);

        if ($consultation) {
            if ($this->netteUser->id != $consultation->getSupervisor()->getId()) {
                new ForbiddenRequestException();
            }

            if ($consultation->getStudent()) {
                $student = clone $consultation->getStudent();
                $tmpConsultation = clone $consultation;
            }
            $consultation->setStudent(null);

            try {
                $this->consultationRepository->deleteWhere(['id' => $id]);
                if (isset($student)) {
                    $this->mailFactory->createByType(ConsultationLogoutMail::class, [
                        'user' => $student,
                        'consultation' => $tmpConsultation
                    ])->send();
                }
                return true;
            } catch (Exception $e) {
                Debugger::log($e);
                return false;
            }
        }

        return true;
    }
}