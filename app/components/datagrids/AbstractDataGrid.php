<?php

namespace App\Components\DataGrids;

use Nette\Application\UI\Control;
use ReflectionClass;
use Ublaboo\DataGrid\DataGrid;
use Ublaboo\DataGrid\Localization\SimpleTranslator;

abstract class AbstractDataGrid extends Control
{
    public static $dictionary = [
        'ublaboo_datagrid.no_item_found_reset' => 'Žádné položky nenalezeny. Filtr můžete vynulovat',
        'ublaboo_datagrid.no_item_found' => 'Žádné položky nenalezeny.',
        'ublaboo_datagrid.here' => 'zde',
        'ublaboo_datagrid.items' => 'Položky',
        'ublaboo_datagrid.all' => 'všechny',
        'ublaboo_datagrid.from' => 'z',
        'ublaboo_datagrid.reset_filter' => 'Resetovat filtr',
        'ublaboo_datagrid.group_actions' => 'Hromadné akce',
        'ublaboo_datagrid.show' => 'Zobrazit',
        'ublaboo_datagrid.add' => 'Přidat',
        'ublaboo_datagrid.edit' => 'Editovat',
        'ublaboo_datagrid.show_all_columns' => 'Zobrazit všechny sloupce',
        'ublaboo_datagrid.show_default_columns' => 'Zobrazit výchozí sloupce',
        'ublaboo_datagrid.hide_column' => 'Skrýt sloupec',
        'ublaboo_datagrid.action' => 'Akce',
        'ublaboo_datagrid.previous' => 'Předchozí',
        'ublaboo_datagrid.next' => 'Další',
        'ublaboo_datagrid.choose' => 'Vyberte',
        'ublaboo_datagrid.choose_input_required' => 'Vložte prosím nějaký text',
        'ublaboo_datagrid.execute' => 'Provést',
        'ublaboo_datagrid.save' => 'Uložit',
        'ublaboo_datagrid.cancel' => 'Zrušit',
        'ublaboo_datagrid.multiselect_choose' => 'Vybrat',
        'ublaboo_datagrid.multiselect_selected' => '{0} vybraných',
        'ublaboo_datagrid.filter_submit_button' => 'Filtrovat',
        'ublaboo_datagrid.show_filter' => 'Zobrazit filter',
        'ublaboo_datagrid.per_page_submit' => 'Změnit'
    ];

    /**
     * @return DataGrid
     */
    public function create()
    {
        $grid = new DataGrid();

        $translator = new SimpleTranslator(self::$dictionary);

        $grid->setTranslator($translator);
        $grid->setStrictSessionFilterValues(false);

        return $grid;
    }

    public function render()
    {
        $rc = new ReflectionClass($this);
        $template = $this->template;
        if (is_file(__DIR__ . '/' . $rc->getShortName() . '/templates/default.latte')) {
            $template->setFile(__DIR__ . '/' . $rc->getShortName() . '/templates/default.latte');
        } else {
            $template->setFile(__DIR__ . '/AbstractDataGrid.latte');
        }
        $template->render();
    }
}