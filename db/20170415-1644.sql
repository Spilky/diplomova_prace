ALTER TABLE `group` ADD reminder_consultations_weeks INT DEFAULT NULL, ADD reminder_reports_weeks INT DEFAULT NULL;
ALTER TABLE `project` ADD reminder_consultations_sent DATE DEFAULT NULL, ADD reminder_reports_sent DATE DEFAULT NULL;
