<?php

namespace App\Components\Forms\SendMessageForm;

use App\Components\Forms\AbstractForm;
use App\Model\Entity\Mail;
use App\Model\Entity\MailTemplate;
use App\Model\Mail\SendMessageMail;
use App\Model\Repository\GroupRepository;
use App\Model\Repository\MailRepository;
use App\Model\Repository\MailTemplateRepository;
use App\Model\Repository\UserRepository;
use DateTime;
use Exception;
use Nette\Application\UI\Form;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;
use Ublaboo\Mailing\MailFactory;

class SendMessageForm extends AbstractForm
{
    /** @var GroupRepository */
    private $groupRepository;
    /** @var MailRepository */
    private $mailRepository;
    /** @var MailTemplateRepository */
    private $mailTemplateRepository;
    /** @var UserRepository */
    private $userRepository;
    /** @var MailFactory */
    private $mailFactory;
    /** @var NetteUser */
    private $netteUser;

    /** @var string[] */
    private $receivers = [];
    /** @var boolean */
    private $singleReceiver = false;


    /**
     * SendMessageForm constructor.
     * @param GroupRepository $groupRepository
     * @param MailRepository $mailRepository
     * @param MailTemplateRepository $mailTemplateRepository
     * @param UserRepository $userRepository
     * @param MailFactory $mailFactory
     * @param NetteUser $netteUser
     */
    public function __construct(
        GroupRepository $groupRepository,
        MailRepository $mailRepository,
        MailTemplateRepository $mailTemplateRepository,
        UserRepository $userRepository,
        MailFactory $mailFactory,
        NetteUser $netteUser
    ) {
        $this->groupRepository = $groupRepository;
        $this->mailRepository = $mailRepository;
        $this->mailTemplateRepository = $mailTemplateRepository;
        $this->userRepository = $userRepository;
        $this->mailFactory = $mailFactory;
        $this->netteUser = $netteUser;
    }

    /**
     * @param string[] $receivers
     */
    public function setReceivers($receivers)
    {
        $this->receivers = $receivers;
    }

    /**
     * @param boolean $singleReceiver
     */
    public function setSingleReceiver($singleReceiver = true)
    {
        $this->singleReceiver = $singleReceiver;
    }

    public function render()
    {
        $this->template->singleReceiver = $this->singleReceiver;
        $this->template->_form = $this['form'];

        parent::render();
    }

    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form;

        $receivers = $form->addCheckboxList('receivers', 'Příjemci', $this->receivers);

        if ($this->singleReceiver) {
            $receivers->setDefaultValue(array_keys($this->receivers));
        }

        $templates = $this->mailTemplateRepository->getByParameters(['user' => $this->netteUser->id]);
        $form->addSelect('template', 'Vybrat z šablon', ['' => ''] + MailTemplateRepository::getIdIndexedArrayOfNames($templates));

        $form->addText('subject', 'Předmět');
        
        $form->addTextArea('message', 'Zpráva');

        $form->addCheckbox('copy', 'Kopie na váš e-mail?');

        $form->addCheckbox('templateSave', 'Uložit text e-mailu jako šablonu pro další použití?')
            ->addCondition(Form::EQUAL, true)
            ->toggle('template-name-container');

        $form->addText('templateName', 'Název šablony')
            ->addConditionOn($form['template'], Form::EQUAL, true)
            ->setRequired('Vložte prosím název šablony.');

        $form->addSubmit('send', 'Odeslat');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        $user = $this->userRepository->getById($this->netteUser->id);

        $values['sender'] = $user;

        try {
            $this->mailFactory->createByType(SendMessageMail::class, $values)->send();

            $values['group'] = $this->groupRepository->getById($this->netteUser->identity->group);
            $values['sender'] = $user->getName() . ' <' . $user->getEmail() . '>';
            $values['receivers'] = implode(', ', $values['receivers']);
            $values['sent'] = new DateTime();
            $mail = new Mail($values);
            $this->mailRepository->insert($mail);
        } catch (Exception $e) {
            Debugger::log($e);
            $this->onError($this, $e);
            return;
        }

        if ($values['templateSave']) {
            try {
                $template = new MailTemplate([
                    'user' => $user,
                    'name' => $values['templateName'],
                    'subject' => $values['subject'],
                    'message' => $values['message']
                ]);
                $this->mailTemplateRepository->insert($template);
            } catch (Exception $e) {
                Debugger::log($e);
            }
        }

        $this->onSuccess($this);
    }

    /**
     * @param int $templateId
     */
    public function handleGetMailTemplateData($templateId)
    {
        $template = $this->mailTemplateRepository->getById($templateId);

        if ($template) {
            $this['form']['subject']->setValue($template->getSubject());
            $this['form']['message']->setValue($template->getMessage());
        } else {
            $this['form']['subject']->setValue('');
            $this['form']['message']->setValue('');
        }

        $this->redrawControl('mailSnippet');
    }
}
