<?php

namespace App\Components\Forms\SendMessageForm\Factories;

use App\Components\Forms\SendMessageForm\SendMessageForm;

interface ISendMessageFormFactory
{
    /**
     * @return SendMessageForm
     */
    public function create();
}
