<?php

namespace App\Components\DataGrids\TermsDataGrid;

use App\Components\DataGrids\AbstractDataGrid;
use App\Model\Repository\TermRepository;
use Exception;
use Nette\Application\ForbiddenRequestException;
use Nette\Security\User as NetteUser;
use Tracy\Debugger;
use Ublaboo\DataGrid\DataGrid;

class TermsDataGrid extends AbstractDataGrid
{
    /** @var NetteUser */
    private $netteUser;
    /** @var TermRepository */
    private $termRepository;

    /**
     * TermsDataGrid constructor.
     * @param NetteUser $netteUser
     * @param TermRepository $tagRepository
     */
    public function __construct(NetteUser $netteUser, TermRepository $tagRepository)
    {
        $this->netteUser = $netteUser;
        $this->termRepository = $tagRepository;
    }

    /**
     * @return DataGrid
     */
    public function createComponentDataGrid()
    {
        $grid = parent::create();

        $grid->setColumnsHideable();

        $grid->addColumnText('name', 'Název')
            ->setSortable()
            ->setFilterText();

        $grid->addColumnDateTime('date', 'Datum')
            ->setAlign('left')
            ->setFormat('j.n.Y')
            ->setSortable()
            ->setFilterDateRange()
            ->setAttribute('data-date-language', 'cs');

        $grid->addColumnText('description', 'Popis')
            ->setDefaultHide()
            ->setTemplateEscaping(false)
            ->setFilterText();

        $replacements = [true => 'Ano', false => 'Ne'];
        $grid->addColumnText('public', 'Veřejný')
            ->setReplacement($replacements)
            ->setSortable()
            ->setFilterSelect(array_merge([null => '-'], $replacements));

        $grid->addAction('edit', 'Editovat', 'Term:edit')
            ->setIcon('pencil');

        $grid->addAction('delete', 'Smazat', 'delete!')
            ->setClass('ajax btn btn-xs btn-danger')
            ->setIcon('trash')
            ->setConfirm('Opravdu chcete smazat termín %s?', 'name');

        $grid->addToolbarButton('Term:add', 'Přidat termín')
            ->setIcon('plus');

        $grid->addToolbarButton('Term:copy', 'Kopírovat termíny')
            ->setIcon('copy');

        $grid->setItemsDetail();
        $grid->setTemplateFile(__DIR__ . '/templates/datagrid_default.latte');

        $grid->setDataSource($this->termRepository->getQB('te')
            ->join('te.group', 'gr')
            ->where('gr.id = :group')
            ->setParameter('group', $this->netteUser->identity->group));

        return $grid;
    }

    /**
     * @param int $id
     */
    public function handleDelete($id)
    {
        $term = $this->termRepository->getById($id);

        if ($term) {
            if ($this->netteUser->id != $term->getGroup()->getSupervisor()->getId()) {
                new ForbiddenRequestException();
            }

            try {
                $this->termRepository->deleteWhere(['id' => $id]);
                $this->flashMessage('Termín byl úspěšně smazán.', 'success');
            } catch (Exception $e) {
                Debugger::log($e);
                $this->flashMessage('Termín se nepodařilo smazat. Zkuste to prosím znovu.', 'danger');
            }

            $this->redrawControl('flashes');
            $this['dataGrid']->reload();
        }
    }
}