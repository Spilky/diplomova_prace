<?php

namespace App\Presenters;

use App\Components\DataGrids\TagsDataGrid\Factories\ITagsDataGridFactory;
use App\Components\DataGrids\TagsDataGrid\TagsDataGrid;
use App\Components\Forms\AddEditTagForm\AddEditTagForm;
use App\Components\Forms\AddEditTagForm\Factories\IAddEditTagFormFactory;
use App\Model\Entity\Tag;
use App\Model\Repository\GroupRepository;
use App\Model\Repository\TagRepository;
use Nette\Application\BadRequestException;
use Nette\Application\ForbiddenRequestException;

class TagPresenter extends BasePresenter
{
    /** @var TagRepository @inject */
    public $tagRepository;
    /** @var ITagsDataGridFactory @inject */
    public $tagsDataGridFactory;
    /** @var IAddEditTagFormFactory @inject */
    public $addEditTagFormFactory;

    /**
     * @param Tag $tag
     * @throws ForbiddenRequestException
     */
    private function checkRights($tag)
    {
        $groupsIds = array_keys(GroupRepository::getIdIndexedArrayOfNames(
            $this->groupRepository->getByParameters(['supervisor' => $this->user->id])
        ));
        if (!in_array($tag->getGroup()->getId(), $groupsIds)) {
            throw new ForbiddenRequestException;
        }
    }

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionEdit($id = 0)
    {
        $tag = $this->tagRepository->getById($id);
        if (is_null($tag)) {
            throw new BadRequestException;
        }

        $this->checkRights($tag);

        $this['addEditTagForm']->setTerm($tag);
    }

    /**
     * @return AddEditTagForm
     */
    protected function createComponentAddEditTagForm()
    {
        $form = $this->addEditTagFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'Štítek byl úspěšně ' .
                ($this->getAction() == 'add' ?  'vložen.' : 'editován.'), 'success');
            $this->redirect('Tag:');
        };
        $form->onError[] = function ($form, $error) {
            $this->flashMessage(
                'Štítek se nepodařilo ' .
                ($this->getAction() == 'add' ?  'vložit.' : 'editovat.') .
                ' Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }

    /**
     * @return TagsDataGrid
     */
    protected function createComponentTagsDataGrid()
    {
        $control = $this->tagsDataGridFactory->create();
        return $control;
    }
}
