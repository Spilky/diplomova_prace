<?php

namespace App\Components\DataGrids\TermsDataGrid\Factories;

use App\Components\DataGrids\TermsDataGrid\TermsDataGrid;

interface ITermsDataGridFactory
{
    /**
     * @return TermsDataGrid
     */
    public function create();
}
