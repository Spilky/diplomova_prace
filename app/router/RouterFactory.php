<?php

namespace App;

use Nette\Application\IRouter;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;

class RouterFactory
{
    /**
     * @return IRouter
     */
    public static function createRouter()
    {
        $router = new RouteList();

        $filterTable = [
            'presenter' => [
                Route::VALUE => 'Homepage',
                Route::FILTER_TABLE => [
                    'konzultace' => 'Consultation',
                    'dokumenty' => 'Document',
                    'skupiny' => 'Group',
                    'mailing' => 'Mailing',
                    'milniky' => 'Milestone',
                    'projekty' => 'Project',
                    'reporty' => 'Report',
                    'stitky' => 'Tags',
                    'terminy' => 'Term',
                    'uzivatele' => 'User'
                ],
            ],
            'action' => [
                Route::VALUE => 'default',
                Route::FILTER_TABLE => [
                    'pridat' => 'add',
                    'pridat-bloky' => 'addBlocks',
                    'kopirovat' => 'copy',
                    'smazat' => 'delete',
                    'detail' => 'detail',
                    'stahnout' => 'download',
                    'editovat' => 'edit',
                    'importovat' => 'import',
                    'profil' => 'profile',
                    'sablony' => 'templates'
                ]
            ],
            'id' => NULL,
        ];

        $router[] = new Route('prihlasit', 'Sign:in');

        $router[] = new Route('odhlasit', 'Sign:out');

        $router[] = new Route('zapomenute-heslo[/<token>]', 'Sign:forgottenPassword');

        $router[] = new Route('konzultace/vedouci-<supervisorId>', 'Consultation:default');

        $router[] = new Route('konzultace-verejne/vedouci-<supervisorId>/<token>', 'Consultation:public');

        $documentFilterTable = $filterTable;
        $documentFilterTable['presenter'] = 'Document';
        $router[] = new Route('dokumenty/projekt-<projectId>/<action>[/<id>]', $documentFilterTable);

        $consultationFilterTable = $filterTable;
        $consultationFilterTable['presenter'] = 'Consultation';
        $router[] = new Route('konzultace/projekt-<projectId>/<action>[/<id>]', $consultationFilterTable);

        $milestoneFilterTable = $filterTable;
        $milestoneFilterTable['presenter'] = 'Milestone';
        $router[] = new Route('milniky/projekt-<projectId>/<action>[/<id>]', $milestoneFilterTable);

        $reportFilterTable = $filterTable;
        $reportFilterTable['presenter'] = 'Report';
        $router[] = new Route('reporty/projekt-<projectId>/<action>[/<id>]', $reportFilterTable);

        $router[] = new Route('bez-skupiny', 'Homepage:noGroup');

        $router[] = new Route('<presenter>/<action>[/<id>]', $filterTable);

        return $router;
    }
}
