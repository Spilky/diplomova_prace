<?php

namespace App\Components\Forms\AddEditTermForm;

use App\Components\Forms\AbstractForm;
use App\Model\Entity\Term;
use App\Model\Repository\GroupRepository;
use App\Model\Repository\TermRepository;
use DateTime;
use Exception;
use Nette\Application\UI\Form;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class AddEditTermForm extends AbstractForm
{
    /** @var NetteUser */
    private $netteUser;
    /** @var GroupRepository */
    private $groupRepository;
    /** @var TermRepository */
    private $termRepository;
    /** @var Term */
    private $term = null;

    /**
     * AddEditTermForm constructor.
     * @param NetteUser $netteUser
     * @param GroupRepository $groupRepository
     * @param TermRepository $tagRepository
     */
    public function __construct(
        NetteUser $netteUser,
        GroupRepository $groupRepository,
        TermRepository $tagRepository
    ) {
        $this->netteUser = $netteUser;
        $this->groupRepository = $groupRepository;
        $this->termRepository = $tagRepository;
    }

    /**
     * @param int|Term $term
     */
    public function setTerm($term)
    {
        if (!($term instanceof Term)) {
            $term = $this->termRepository->getById($term);
        }

        $this->term = $term;
    }

    public function render()
    {
        if ($this->term) {
            $data = $this->term->getAsArray();
            $data['date'] = $this->term->getDate()->format('j.n.Y');
            $this['form']->setDefaults($data);
        }

        $this->template->group = $this->groupRepository->getById($this->netteUser->identity->group);
        parent::render();
    }

    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form;

        $form->addText('name', 'Název')
            ->setRequired('Vložte prosím jméno.');

        $form->addText('date', 'Datum')
            ->setRequired('Vložte prosím datum.');

        $form->addTextArea('description', 'Popis');

        $form->addCheckbox('public', 'Veřejný termín - mohou si jej zkopírovat ostatní vedoucí')
            ->setDefaultValue(true);

        $form->addHidden('id');

        $form->addSubmit('submit', 'Uložit');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        $values['date'] = new DateTime($values['date']);

        if (!isset($values['public'])) {
            $values['public'] = false;
        }

        if (empty($values['id'])) {
            $values['group'] = $this->groupRepository->getById($this->netteUser->identity->group);
            $report = new Term($values);
        }

        try {
            if (empty($values['id'])) {
                $this->termRepository->insert($report);
            } else {
                $this->termRepository->updateWhere($values, ['id' => $values['id']]);
            }
        } catch (Exception $e) {
            $this->onError($this, $e);
            Debugger::log($e);
            return;
        }

        $this->onSuccess($this);
    }
}
