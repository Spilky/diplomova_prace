<?php

namespace App\Components\Forms\AddEditFeedbackForm;

use App\Components\Forms\AbstractForm;
use App\Model\Entity\Report;
use App\Model\Mail\SendFeedbackMail;
use App\Model\Repository\ReportRepository;
use App\Model\Repository\UserRepository;
use Exception;
use Nette\Application\UI\Form;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Tracy\Debugger;
use Ublaboo\Mailing\MailFactory;

class AddEditFeedbackForm extends AbstractForm
{
    /** @var NetteUser */
    private $netteUser;
    /** @var ReportRepository */
    private $reportRepository;
    /** @var UserRepository */
    private $userRepository;
    /** @var MailFactory */
    private $mailFactory;
    /** @var Report */
    private $report;
    /** @var Html */
    private $cancelButton;

    /**
     * AddEditFeedbackForm constructor.
     * @param int|Report $report
     * @param string $cancelButton
     * @param NetteUser $netteUser
     * @param ReportRepository $reportRepository
     * @param UserRepository $userRepository
     * @param MailFactory $mailFactory
     */
    public function __construct(
        $report,
        $cancelButton,
        NetteUser $netteUser,
        ReportRepository $reportRepository,
        UserRepository $userRepository,
        MailFactory $mailFactory
    ) {
        if (!($report instanceof Report)) {
            $report = $reportRepository->getById($report);
        }
        $this->report = $report;
        $this->cancelButton = $cancelButton;
        $this->netteUser = $netteUser;
        $this->reportRepository = $reportRepository;
        $this->userRepository = $userRepository;
        $this->mailFactory = $mailFactory;
    }

    public function render()
    {
        $this['form']->setDefaults($this->report->getAsArray());
        $this->template->cancelButton = $this->cancelButton;

        parent::render();
    }

    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form;

        $form->addTextArea('feedback', 'Zpětná vazba');

        $form->addCheckbox('sent', 'Odeslat na e-mail řešitele?');

        $form->addHidden('id');

        $form->addSubmit('submit', 'Uložit');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        $report = $this->reportRepository->getById($values['id']);
        $report->setFeedback($values['feedback']);
        if ($values['sent']) {
            $sender = $this->userRepository->getById($this->netteUser->id);
            $receiver = $report->getProject()->getStudent();
        }

        try {
            $this->reportRepository->update($report);
            if (isset($sender) && isset($receiver)) {
                $this->mailFactory->createByType(SendFeedbackMail::class, [
                    'sender' => $sender,
                    'receiver' => $receiver,
                    'report' => $report
                ])->send();
            }
        } catch (Exception $e) {
            $this->onError($this, $e);
            Debugger::log($e);
            return;
        }

        $this->onSuccess($this);
    }
}
