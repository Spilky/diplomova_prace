<?php

namespace App\Components\Forms\AddEditDocumentForm;

use App\Components\Forms\AbstractForm;
use App\Model\Entity\Document;
use App\Model\Repository\DocumentRepository;
use App\Model\Repository\ProjectRepository;
use App\Model\Repository\UserRepository;
use App\Model\Service\FileManager;
use Exception;
use Nette\Application\UI\Form;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class AddEditDocumentForm extends AbstractForm
{
    const TYPE_LINK = 'link',
        TYPE_FILE = 'file';

    /** @var FileManager */
    private $fileManager;
    /** @var NetteUser */
    private $netteUser;
    /** @var DocumentRepository */
    private $documentRepository;
    /** @var ProjectRepository */
    private $projectRepository;
    /** @var UserRepository */
    private $userRepository;
    /** @var Document */
    private $document = null;

    /**
     * AddEditDocumentForm constructor.
     * @param FileManager $fileManager
     * @param NetteUser $netteUser
     * @param DocumentRepository $documentRepository
     * @param ProjectRepository $projectRepository
     * @param UserRepository $userRepository
     */
    public function __construct(
        FileManager $fileManager,
        NetteUser $netteUser,
        DocumentRepository $documentRepository,
        ProjectRepository $projectRepository,
        UserRepository $userRepository
    ) {
        $this->fileManager = $fileManager;
        $this->netteUser = $netteUser;
        $this->documentRepository = $documentRepository;
        $this->projectRepository = $projectRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Document $document
     */
    public function setDocument($document)
    {
        $this->document = $document;
    }

    public function render()
    {
        if ($this->document) {
            $data = $this->document->getAsArray();
            if ($this->document->getFile()) {
                $data['type'] = self::TYPE_FILE;
                $this->template->fileDocument = $this->document;
            }
            $data['public'] = intval($this->document->getPublic());
            $this['form']->setDefaults($data);
        }

        parent::render();
    }

    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form;

        $form->addText('name', 'Název')
            ->setRequired('Vložte prosím název.');

        $form->addTextArea('description', 'Krátký popis');

        $form->addRadioList('public', 'Zpřístupnit dokument ostatním uživatelům?', [true => 'Ano', false => 'Ne'])
            ->setDefaultValue(true)
            ->setRequired('Vyberte prosím, zda-li chcete dokument zpřístupnit ostatním uživatelům.');

        $form->addRadioList('type', null, [self::TYPE_LINK => 'Odkaz', self::TYPE_FILE => 'Soubor'])
            ->setDefaultValue(self::TYPE_LINK)
            ->setRequired('Vyberte, zda-li se jedná o odkaz nebo o nahravaný soubor.');

        $form->addText('link', 'Odkaz')
            ->addConditionOn($form['type'], Form::EQUAL, self::TYPE_LINK)
                ->setRequired('Vložte prosím odkaz na dokument.')
                ->addRule(Form::URL, 'Vložte prosím validní odkaz na dokument.');

        $fileInput = $form->addUpload('file', 'Soubor');

        if (!$this->document || ($this->document && $this->document->getLink())) {
            $fileInput->addConditionOn($form['type'], Form::EQUAL, self::TYPE_FILE)
                ->setRequired('Vložte prosím soubor.');
        }

        $form->addHidden('projectId');

        $form->addHidden('id');

        $form->addSubmit('submit', 'Uložit');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        $project = $this->projectRepository->getById($values['projectId']);
        unset($values['projectId']);

        if ($values['type'] == 'file') {
            $file = $values['file'];
            $values['link'] = null;
            if (empty($values['id']) || $file->getError() != UPLOAD_ERR_NO_FILE) {
                $values['file'] = $this->fileManager->saveFile($file);
                $values['fileName'] = $file->getSanitizedName();
            } else {
                unset($values['file']);
            }
        } else {
            $values['file'] = null;
            $values['fileName'] = null;
        }

        if (empty($values['id'])) {
            $values['project'] = $project;
            $document = new Document($values);
        } else {
            $document = $this->documentRepository->getById($values['id']);
        }

        try {
            if (empty($values['id'])) {
                $this->documentRepository->insert($document);
            } else {
                $oldFile = $document->getFile();
                $this->documentRepository->updateWhere($values, ['id' => $document->getId()]);
                if ($oldFile && (isset($values['file']) || isset($values['link']))) {
                    $this->fileManager->delete($oldFile);
                }
            }
        } catch (Exception $e) {
            $this->onError($this, $e);
            Debugger::log($e);
            return;
        }

        $this->onSuccess($this);
    }
}
