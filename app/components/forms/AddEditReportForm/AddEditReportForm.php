<?php

namespace App\Components\Forms\AddEditReportForm;

use App\Components\Forms\AbstractForm;
use App\Model\Entity\Project;
use App\Model\Entity\Report;
use App\Model\Repository\GroupRepository;
use App\Model\Repository\ProjectRepository;
use App\Model\Repository\ReportRepository;
use DateTime;
use Exception;
use Nette\Application\UI\Form;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Tracy\Debugger;

class AddEditReportForm extends AbstractForm
{
    /** @var NetteUser */
    private $netteUser;
    /** @var ProjectRepository */
    private $projectRepository;
    /** @var ReportRepository */
    private $reportRepository;
    /** @var Project */
    private $project;
    /** @var Report */
    private $report = null;
    /** string[] */
    private $weeks;
    /** @var Html */
    private $cancelButton = null;

    /**
     * AddEditReportForm constructor.
     * @param Project $project
     * @param NetteUser $netteUser
     * @param ProjectRepository $projectRepository
     * @param ReportRepository $timeLineItemRepository
     */
    public function __construct(
        Project $project,
        NetteUser $netteUser,
        ProjectRepository $projectRepository,
        ReportRepository $timeLineItemRepository
    ) {
        $this->project = $project;
        $this->netteUser = $netteUser;
        $this->projectRepository = $projectRepository;
        $this->reportRepository = $timeLineItemRepository;
        $this->weeks = GroupRepository::getListOfWeeksByGroup($this->project->getGroup());
    }

    /**
     * @param int|Report $report
     */
    public function setReport($report)
    {
        if (!($report instanceof Report)) {
            $report = $this->reportRepository->getById($report);
        }

        $this->report = $report;
    }

    /**
     * @param Html $cancelButton
     */
    public function setCancelButton($cancelButton)
    {
        $this->cancelButton = $cancelButton;
    }

    /**
     * @param array $defaults
     */
    public function setDefaults($defaults)
    {
        if (isset($defaults['date'])) {
            if (!in_array($defaults['date'], array_keys($this->weeks))) {
                unset($defaults['date']);
            }
        }
        parent::setDefaults($defaults);
    }

    public function render()
    {
        if ($this->report) {
            $begin = $this->report->getDate();
            $end = clone $begin;
            $end->modify('+7 days');
            $this['form']['date']->setItems([
                $begin->format('Y-m-d') => $begin->format('j.n.Y') . ' - ' . $end->format('j.n.Y')
            ])->setDisabled();
            $data = $this->report->getAsArray();
            unset($data['date']);
            $data['projectId'] = $this->report->getProject()->getId();
            $this['form']['progress']->setAttribute('data-slider-value', $this->report->getProgress());
        }

        $data['projectId'] = $this->project->getId();
        $this['form']->setDefaults($data);

        if ($this->cancelButton) {
            $this->template->cancelButton = $this->cancelButton;
        }

        parent::render();
    }

    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form;

        $date = new DateTime();
        while ($date->format('w') != 1) {
            $date->modify( '-1 day' );
        }
        $form->addSelect('date', 'Týden', $this->weeks)
            ->setRequired('Vyberte si prosím týden.')
            ->setDefaultValue(in_array($date->format('Y-m-d'), array_keys($this->weeks)) ? $date->format('Y-m-d') : null);

        $smileys = ReportRepository::getSmileys();
        $form->addRadioList('smiley', 'Spokojenost s vlastní prací', $smileys)
            ->setRequired('Vyberte prosím spokojenost s vlastní prací.')
            ->setDefaultValue(Report::SMILEY_NEUTRAL);

        $form->addText('progress', 'Stav plnění v rámci milníku')
            ->setRequired('Vyberte prosím stav plnění v rámci milníku.')
            ->addRule(Form::MIN, 'Stav plnění v rámci milníku musí být minimálně 0%.', 0)
            ->addRule(Form::MAX, 'Stav plnění v rámci milníku musí být maximálně 100%.', 100)
            ->setDefaultValue(50)
            ->setAttribute('data-slider-min', 0)
            ->setAttribute('data-slider-max', 100)
            ->setAttribute('data-slider-step', 5)
            ->setAttribute('data-slider-value', 50);

        $form->addTextArea('lastWeek', 'Co jsem udělal tento týden');

        $form->addTextArea('nextWeek', 'Co udělám příští týden');

        $form->addHidden('projectId');

        $form->addHidden('id');

        $form->addSubmit('submit', 'Uložit');

        $form->onSuccess[] = array($this, 'formSucceeded');
        $form->onValidate[] = array($this, 'formValidate');
        return $form;
    }

    /**
     * @param Form $form
     */
    public function formValidate(Form $form)
    {
        if ($this->report) {
            $form->getComponent('date')->cleanErrors();
        }
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        $project = $this->projectRepository->getById($values['projectId']);
        unset($values['projectId']);

        if (empty($values['id'])) {
            $values['date'] = new DateTime($values['date']);
            $values['project'] = $project;
            $report = new Report($values);
        } else {
            unset($values['date']);
        }

        try {
            if (empty($values['id'])) {
                $this->reportRepository->insert($report);
            } else {
                $this->reportRepository->updateWhere($values, ['id' => $values['id']]);
            }
        } catch (Exception $e) {
            $this->onError($this, $e);
            Debugger::log($e);
            return;
        }

        $this->onSuccess($this);
    }
}
