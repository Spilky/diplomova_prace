<?php

namespace App\Presenters;

use App\Components\Forms\ReportProblemForm\Factories\IReportProblemFormFactory;
use App\Components\Forms\ReportProblemForm\ReportProblemForm;
use App\Model\Entity\User;
use App\Model\Mail\AbstractMail;
use App\Model\Repository\GroupRepository;
use App\Model\Repository\ProjectRepository;
use App\Model\Repository\UserRepository;
use Nette\Application\ForbiddenRequestException;
use Nette\Application\UI\Presenter;
use Nette\Http\Session;
use Tracy\Debugger;

abstract class BasePresenter extends Presenter
{
    /** @var Session @inject */
    public $session;
    /** @var GroupRepository @inject */
    public $groupRepository;
    /** @var ProjectRepository @inject */
    public $projectRepository;
    /** @var UserRepository @inject */
    public $userRepository;
    /** @var IReportProblemFormFactory @inject */
    public $reportProblemFormFactory;

    public function startup()
    {
        parent::startup();
        if (!$this->user->isLoggedIn()) {
            if (($this->getName() == 'Sign' &&
                    in_array($this->getAction(), ['in', 'out', 'forgottenPassword'])) ||
                ($this->getName() == 'Consultation' &&
                    in_array($this->getAction(), ['public', 'publicLogin', 'publicLogout']) &&
                    !empty($this->getParameter('supervisorId')) &&
                    !empty($this->getParameter('token')))) {
                return;
            } else {
                $url = $this->getHttpRequest()->getUrl();
                $this->redirect('Sign:in', ['returnUrl' => $url->getAbsoluteUrl()]);
                return;
            }
        }

        if (!$this->user->isAllowed($this->getName(), $this->getAction())) {
            throw new ForbiddenRequestException;
        }

        if (!empty($this->user->identity->group)) {
            $group = $this->groupRepository->getById($this->user->identity->group);
            $this->user->identity->group = is_null($group) || $this->user->id != $group->getSupervisor()->getId() ? null : $group->getId();
        }

        if (empty($this->user->identity->group)) {
            $user = $this->userRepository->getById($this->user->id);
            $group = $user->getGroup();
            if (empty($group)) {
                if ($this->user->isInRole(User::ROLE_STUDENT)) {
                    $project = $this->projectRepository->getOneByParameters(['student' => $this->user->id]);
                    $group = is_null($project) ? null : $project->getGroup();
                } else {
                    $group = $this->groupRepository->getOneByParameters(['supervisor' => $this->user->id]);
                }
            } else {
                if (!$this->user->isInRole(User::ROLE_STUDENT) && $group->getSupervisor()->getId() != $this->user->id) {
                    $group = $this->groupRepository->getOneByParameters(['supervisor' => $this->user->id]);
                }

                if ($this->user->isInRole(User::ROLE_STUDENT)) {
                    $project = $this->projectRepository->getOneByParameters(['student' => $this->user->id, 'group' => $group->getId()]);
                    if (!$project) {
                        $project = $this->projectRepository->getOneByParameters(['student' => $this->user->id]);
                        $group = is_null($project) ? null : $project->getGroup();
                    }
                }
            }

            if (is_null($group) &&
                !$this->isLinkCurrent('Sign:*') &&
                !$this->isLinkCurrent('Homepage:noGroup') &&
                !$this->isLinkCurrent('Group:*') &&
                !$this->isLinkCurrent('User:*') &&
                !$this->isLinkCurrent('Consultation:default') &&
                !$this->isLinkCurrent('Consultation:addBlocks')){
                $this->redirect('Homepage:noGroup');
            }

            $this->user->identity->group = is_null($group) ? null : $group->getId();
        }

        $section = $this->session->getSection('template');
        if (!isset($section['collapsedSidebar'])) {
            $section['collapsedSidebar'] = false;
        }
        $this->template->templateSession = $section;
    }

    public function beforeRender()
    {
        $this->template->productionMode = Debugger::$productionMode;

        if ($this->user->isInRole(User::ROLE_STUDENT)) {
            $projects = $this->projectRepository->getByParameters(['student' => $this->user->id]);
            $this->template->groups = [];
            $this->template->myProjects = [];
            foreach ($projects as $project) {
                $this->template->groups[$project->getGroup()->getId()] = $project->getGroup();
                if ($project->getGroup()->getId() == $this->user->identity->group) {
                    $this->template->myProjects[] = $project;
                }
            }
        } else {
            $this->template->groups = $this->groupRepository->getByParameters(['supervisor' => $this->user->id]);
        }
    }

    public function handleCollapsedSidebarChanged()
    {
        $section = $this->session->getSection('template');
        if (isset($section['collapsedSidebar'])) {
            $section['collapsedSidebar'] = !$section['collapsedSidebar'];
        }
        $this->template->templateSession = $section;
    }


    /**
     * @param int $id
     */
    public function handleGroupChanged($id)
    {
        $user = $this->userRepository->getById($this->user->id);
        $group = $this->groupRepository->getById($id);
        if ($group) {
            $user->setGroup($group);
            $this->userRepository->update($user);
            $this->user->identity->group = $group->getId();
        }
        $this->redirect('this');
    }

    /**
     * @return ReportProblemForm
     */
    protected function createComponentReportProblemForm()
    {
        $form = $this->reportProblemFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage('Nahlášení problému proběhlo úspěšně.', 'success');
            $this->redirect('this');
        };
        $form->onError[] = function ($form, $error) {
            $this->flashMessage(
                'Nahlášení problému se nezdařilo. Bahlaste prosím problém na e-mailovou adresu ' .
                AbstractMail::ADMIN_MAIL . '. Děkujeme.', 'danger');
        };
        return $form;
    }
}
