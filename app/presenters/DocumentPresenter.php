<?php

namespace App\Presenters;

use App\Components\Forms\AddEditDocumentForm\AddEditDocumentForm;
use App\Components\Forms\AddEditDocumentForm\Factories\IAddEditDocumentFormFactory;
use App\Model\Entity\User;
use App\Model\Repository\DocumentRepository;
use App\Model\Service\FileManager;
use Exception;
use Nette\Application\BadRequestException;
use Nette\Application\ForbiddenRequestException;
use Nette\Application\Responses\FileResponse;
use Tracy\Debugger;

class DocumentPresenter extends BasePresenter
{
    /** @var FileManager @inject */
    public $fileManager;
    /** @var DocumentRepository @inject */
    public $documentRepository;
    /** @var IAddEditDocumentFormFactory @inject */
    public $addEditDocumentFormFactory;

    /**
     * @param int $projectId
     * @throws BadRequestException
     */
    public function actionAdd($projectId = 0)
    {
        $project = $this->projectRepository->getById($projectId);
        if (is_null($project)) {
            throw new BadRequestException;
        }

        $group = $project->getGroup();
        if (($this->user->isInRole(User::ROLE_STUDENT) && $project->getStudent()->getId() != $this->user->id) ||
            ($this->user->isInRole(User::ROLE_SUPERVISOR) && $group->getSupervisor()->getId() != $this->user->id)) {
            throw new ForbiddenRequestException;
        }

        $this['addEditDocumentForm']->setDefaults(['projectId' => $project->getId()]);
    }

    /**
     * @param int $id
     * @param int $projectId
     * @throws BadRequestException
     */
    public function actionEdit($id = 0, $projectId = 0)
    {
        $document = $this->documentRepository->getById($id);
        $project = $this->projectRepository->getById($projectId);
        if (is_null($project) || is_null($document)) {
            throw new BadRequestException;
        }

        $group = $project->getGroup();
        if ($project->getId() != $document->getProject()->getId() ||
            ($this->user->isInRole(User::ROLE_STUDENT) && $project->getStudent()->getId() != $this->user->id) ||
            ($this->user->isInRole(User::ROLE_SUPERVISOR) && $group->getSupervisor()->getId() != $this->user->id)) {
            throw new ForbiddenRequestException;
        }

        $this['addEditDocumentForm']->setDocument($document);
    }

    /**
     * @param int $id
     * @param int $projectId
     * @throws BadRequestException
     */
    public function actionDelete($id = 0, $projectId = 0)
    {
        $document = $this->documentRepository->getById($id);
        $project = $this->projectRepository->getById($projectId);
        if (is_null($project) || is_null($document)) {
            throw new BadRequestException;
        }

        $group = $project->getGroup();
        if ($project->getId() != $document->getProject()->getId() ||
            ($this->user->isInRole(User::ROLE_STUDENT) && $project->getStudent()->getId() != $this->user->id) ||
            ($this->user->isInRole(User::ROLE_SUPERVISOR) && $group->getSupervisor()->getId() != $this->user->id)) {
            throw new ForbiddenRequestException;
        }

        try {
            $file = $document->getFile();
            $this->documentRepository->delete($document);
            if ($file) {
                $this->fileManager->delete($file);
            }
        } catch (Exception $e) {
            Debugger::log($e);
            $this->flashMessage('Dokument se nepodařilo smazat. Zkuste to znovu prosím.', 'danger');
            return;
        }

        $this->flashMessage('Dokument byl úspěšně smazán.', 'success');
        $this->redirect('Project:detail', ['id' => $project->getId()]);
    }

    /**
     * @param int $id
     * @throws BadRequestException
     * @throws ForbiddenRequestException
     */
    public function actionDownload($id = 0)
    {
        $document = $this->documentRepository->getById($id);
        if (is_null($document)) {
            throw new BadRequestException;
        }

        $project = $document->getProject();
        $group = $project->getGroup();
        if (!$document->getPublic() &&
            (($this->user->isInRole(User::ROLE_STUDENT) && $project->getStudent()->getId() != $this->user->id) ||
            ($this->user->isInRole(User::ROLE_SUPERVISOR) && $group->getSupervisor()->getId() != $this->user->id))) {
            throw new ForbiddenRequestException;
        }

        $file = $this->fileManager->getDirectory() . $document->getFile();
        $fileResponse = new FileResponse($file, $document->getFileName(), null, false);

        $this->sendResponse($fileResponse);
    }

    /**
     * @return AddEditDocumentForm
     */
    protected function createComponentAddEditDocumentForm()
    {
        $form = $this->addEditDocumentFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'Dokument byla úspěšně ' .
                ($this->getAction() == 'add' ?  'vložen.' : 'editován.'), 'success');
            $this->redirect('Project:detail', ['id' => $this->getParameter('projectId')]);
        };
        $form->onError[] = function ($form, $error) {
            $this->flashMessage(
                'Dokument se nepodařilo ' .
                ($this->getAction() == 'add' ?  'vložit.' : 'editovat.') .
                ' Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }
}
