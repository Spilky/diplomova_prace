<?php

namespace App\Model\Repository;

use App\Model\Entity\TimeLineItem;
use Kdyby\Doctrine\EntityManager;

class TimeLineItemRepository extends AbstractRepository
{
    /**
     * TimeLineItemRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entityRepository = $this->entityManager->getRepository(TimeLineItem::class);
    }

    /**
     * @return TimeLineItem[]
     */
    public function getAll()
    {
        return parent::getAll();
    }

    /**
     * @param $id
     * @return TimeLineItem
     */
    public function getById($id)
    {
        return parent::getById($id);
    }

    /**
     * @param $parameters
     * @return TimeLineItem[]
     */
    public function getByParameters($parameters)
    {
        return parent::getByParameters($parameters);
    }

    /**
     * @param $parameters
     * @return TimeLineItem
     */
    public function getOneByParameters($parameters)
    {
        return parent::getOneByParameters($parameters);
    }
}