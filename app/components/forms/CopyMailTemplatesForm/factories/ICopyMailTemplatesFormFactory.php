<?php

namespace App\Components\Forms\AddEditProjectForm\Factories;

use App\Components\Forms\AddEditProjectForm\CopyMailTemplatesForm;

interface ICopyMailTemplatesFormFactory
{
    /**
     * @return CopyMailTemplatesForm
     */
    public function create();
}
