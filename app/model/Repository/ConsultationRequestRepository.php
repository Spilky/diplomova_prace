<?php

namespace App\Model\Repository;

use App\Model\Entity\ConsultationRequest;
use Kdyby\Doctrine\EntityManager;

class ConsultationRequestRepository extends AbstractRepository
{
    /**
     * ConsultationRequestRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entityRepository = $this->entityManager->getRepository(ConsultationRequest::class);
    }

    /**
     * @return ConsultationRequest[]
     */
    public function getAll()
    {
        return parent::getAll();
    }

    /**
     * @param $id
     * @return ConsultationRequest
     */
    public function getById($id)
    {
        return parent::getById($id);
    }

    /**
     * @param $parameters
     * @return ConsultationRequest[]
     */
    public function getByParameters($parameters)
    {
        return parent::getByParameters($parameters);
    }

    /**
     * @param $parameters
     * @return ConsultationRequest
     */
    public function getOneByParameters($parameters)
    {
        return parent::getOneByParameters($parameters);
    }

    /**
     * @param $token
     * @return ConsultationRequest
     */
    public function getByTokenAndType($token, $type)
    {
        return $this->getQB('cr')
            ->join('cr.token', 'to')
            ->where('to.token = :token')
            ->andWhere('cr.type = :type')
            ->setParameter('token', $token)
            ->setParameter('type', $type)
            ->getQuery()
            ->getOneOrNullResult();
    }
}