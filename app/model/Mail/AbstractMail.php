<?php

namespace App\Model\Mail;

use Nette\Application\LinkGenerator;
use Nette\Application\UI\ITemplateFactory;
use Nette\Mail\IMailer;
use Nette\Mail\Message;
use ReflectionClass;
use Ublaboo\Mailing\ILogger;
use Ublaboo\Mailing\Mail;

abstract class AbstractMail extends Mail
{
    const NO_REPLY_MAIL = 'noreply@diplomkyfit.cz',
        ADMIN_MAIL = 'xspilk00@stud.fit.vutbr.cz';

    const SUBJECT_TAIL = ' | diplomkyfit.cz';

    public function __construct(
        $config,
        $mails,
        IMailer $mailer,
        Message $message,
        LinkGenerator $linkGenerator,
        ITemplateFactory $templateFactory,
        ILogger $logger,
        $args
    ) {
        parent::__construct($config, $mails, $mailer, $message, $linkGenerator, $templateFactory, $logger, $args);

        $rc = new ReflectionClass($this);
        if (is_file(__DIR__ . '/templates/' . $rc->getShortName() . '.latte')) {
            $this->setTemplateFile(__DIR__ . '/templates/' . $rc->getShortName() . '.latte');
        }
    }
}
