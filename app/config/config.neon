#
# WARNING: it is CRITICAL that this file & directory are NOT accessible directly via a web browser!
# https://nette.org/security-warning
#
parameters:

php:
    date.timezone: Europe/Prague

application:
    errorPresenter: Error
    mapping:
        *: App\*Module\Presenters\*Presenter

session:
    expiration: 14 days

extensions:
    console: Kdyby\Console\DI\ConsoleExtension
    events: Kdyby\Events\DI\EventsExtension
    annotations: Kdyby\Annotations\DI\AnnotationsExtension
    doctrine: Kdyby\Doctrine\DI\OrmExtension
    replicator: Kdyby\Replicator\DI\ReplicatorExtension
    mailing: Ublaboo\Mailing\DI\MailingExtension

services:
    - App\Components\DataGrids\ConsultationsDataGrid\Factories\IConsultationsDataGridFactory
    - App\Components\DataGrids\GroupsDataGrid\Factories\IGroupsDataGridFactory
    - App\Components\DataGrids\MailingDataGrid\Factories\IMailingDataGridFactory
    - App\Components\DataGrids\MailTemplatesDataGrid\Factories\IMailTemplatesDataGridFactory
    - App\Components\DataGrids\ProjectsDataGrid\Factories\IProjectsDataGridFactory
    - App\Components\DataGrids\TagsDataGrid\Factories\ITagsDataGridFactory
    - App\Components\DataGrids\TermsDataGrid\Factories\ITermsDataGridFactory
    - App\Components\DataGrids\UsersDataGrid\Factories\IUsersDataGridFactory

    - App\Components\Forms\AddEditConsultationForm\Factories\IAddEditConsultationFormFactory
    - App\Components\Forms\AddEditDocumentForm\Factories\IAddEditDocumentFormFactory
    - App\Components\Forms\AddEditFeedbackForm\Factories\IAddEditFeedbackFormFactory
    - App\Components\Forms\AddEditGroupForm\Factories\IAddEditGroupFormFactory
    - App\Components\Forms\AddEditMailTemplateForm\Factories\IAddEditMailTemplateFormFactory
    - App\Components\Forms\AddEditMilestoneForm\Factories\IAddEditMilestoneFormFactory
    - App\Components\Forms\AddEditProjectForm\Factories\IAddEditProjectFormFactory
    - App\Components\Forms\AddEditTagForm\Factories\IAddEditTagFormFactory
    - App\Components\Forms\AddEditTermForm\Factories\IAddEditTermFormFactory
    - App\Components\Forms\AddEditReportForm\Factories\IAddEditReportFormFactory
    - App\Components\Forms\AddEditUserForm\Factories\IAddEditUserFormFactory
    - App\Components\Forms\ConsultationSettingsForm\Factories\IConsultationSettingsFormFactory
    - App\Components\Forms\AddEditProjectForm\Factories\ICopyMailTemplatesFormFactory
    - App\Components\Forms\AddEditProjectForm\Factories\ICopyTermsFormFactory
    - App\Components\Forms\ForgottenPasswordForm\Factories\IForgottenPasswordFormFactory
    - App\Components\Forms\AddEditProjectForm\Factories\IImportProjectsFormFactory
    - App\Components\Forms\ReportProblemForm\Factories\IReportProblemFormFactory
    - App\Components\Forms\SendMessageForm\Factories\ISendMessageFormFactory
    - App\Components\Forms\SetNewPasswordForm\Factories\ISetNewPasswordFormFactory
    - App\Components\Forms\SignInForm\Factories\ISignInFormFactory

    - App\Model\UserManager

    - App\Model\Repository\ConsultationRepository
    - App\Model\Repository\ConsultationRequestRepository
    - App\Model\Repository\DocumentRepository
    - App\Model\Repository\GroupRepository
    - App\Model\Repository\MailRepository
    - App\Model\Repository\MailTemplateRepository
    - App\Model\Repository\ProjectRepository
    - App\Model\Repository\ReportRepository
    - App\Model\Repository\TagRepository
    - App\Model\Repository\TermRepository
    - App\Model\Repository\TimeLineItemRepository
    - App\Model\Repository\TokenRepository
    - App\Model\Repository\UserRepository

    - App\Model\Service\TokenService

    fileManager: App\Model\Service\FileManager('%appDir%/../files')

    router: App\RouterFactory::createRouter

    authorizator:
        class: Nette\Security\Permission
        setup:
            - addRole('student')
            - addRole('supervisor', 'student')
            - addRole('admin', 'supervisor')

            - addResource('Consultation')
            - addResource('Document')
            - addResource('Group')
            - addResource('Homepage')
            - addResource('Mailing')
            - addResource('Milestone')
            - addResource('Project')
            - addResource('Report')
            - addResource('Sign')
            - addResource('Tag')
            - addResource('Term')
            - addResource('User')

            - allow('student', 'Consultation', ['default'])
            - allow('student', 'Document', Nette\Security\Permission::ALL)
            - allow('student', 'Homepage', Nette\Security\Permission::ALL)
            - allow('student', 'Mailing', Nette\Security\Permission::ALL)
            - allow('student', 'Milestone', Nette\Security\Permission::ALL)
            - allow('student', 'Project', ['edit', 'detail'])
            - allow('student', 'Report', Nette\Security\Permission::ALL)
            - allow('student', 'Sign', Nette\Security\Permission::ALL)
            - allow('student', 'User', ['profile'])

            - allow('supervisor', 'Consultation', Nette\Security\Permission::ALL)
            - allow('supervisor', 'Group', Nette\Security\Permission::ALL)
            - allow('supervisor', 'Project', Nette\Security\Permission::ALL)
            - allow('supervisor', 'Tag', Nette\Security\Permission::ALL)
            - allow('supervisor', 'Term', Nette\Security\Permission::ALL)
            - allow('supervisor', 'User', 'profile')
            - deny('supervisor', 'Report', 'add')

            - allow('admin', 'User', Nette\Security\Permission::ALL)

mailing:
    do: both # log|send|both
    log_directory: '%appDir%/../log/mails' # this is default option
    mail_images_base_path: %wwwDir% # this is default option
    mails: []