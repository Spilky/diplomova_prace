<?php

namespace App\Model\Mail;

use App\Model\Entity\User;
use Nette\Mail\Message;
use Ublaboo\Mailing\IComposableMail;

class ConsultationLogoutMail extends AbstractMail implements IComposableMail
{
    /**
     * @param Message $message
     * @param null|array $params
     */
    public function compose(Message $message, $params = NULL)
    {
        /** @var User $user */
        $user = $params['user'];

        $message->setFrom(self::NO_REPLY_MAIL);
        $message->addReplyTo(self::NO_REPLY_MAIL);
        $message->addTo($user->getEmail(), $user->getName());
        $message->setSubject('Odhlášení z konzultace' . self::SUBJECT_TAIL);
    }
}
