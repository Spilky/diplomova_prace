<?php

namespace App\Model\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * @ORM\Entity
 * @ORM\Table(name="`report`", options={"collate"="utf8_czech_ci"})
 */
class Report extends AbstractEntity
{
    const SMILEY_HAPPY = 'happy',
        SMILEY_NEUTRAL = 'neutral',
        SMILEY_SAD = 'sad',
        SMILEY_HOLIDAYS = 'holidays';

    /**
     * @ORM\ManyToOne(targetEntity="Project")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $project;

    /**
     * @ORM\Column(type="text")
     */
    protected $lastWeek;

    /**
     * @ORM\Column(type="text")
     */
    protected $nextWeek;

    /**
     * @ORM\Column(type="date")
     */
    protected $date;

    /**
     * @ORM\Column(type="integer")
     */
    protected $progress;

    /**
     * @ORM\Column(type="string")
     */
    protected $smiley;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $feedback;

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return string
     */
    public function getLastWeek()
    {
        return $this->lastWeek;
    }

    /**
     * @param string $lastWeek
     */
    public function setLastWeek($lastWeek)
    {
        $this->lastWeek = $lastWeek;
    }

    /**
     * @return string
     */
    public function getNextWeek()
    {
        return $this->nextWeek;
    }

    /**
     * @param string $nextWeek
     */
    public function setNextWeek($nextWeek)
    {
        $this->nextWeek = $nextWeek;
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * @param int $progress
     */
    public function setProgress($progress)
    {
        $this->progress = $progress;
    }

    /**
     * @return string
     */
    public function getSmiley()
    {
        return $this->smiley;
    }

    /**
     * @param string $smiley
     */
    public function setSmiley($smiley)
    {
        $smileys = array(self::SMILEY_HAPPY, self::SMILEY_NEUTRAL, self::SMILEY_SAD, self::SMILEY_HOLIDAYS);
        if (!in_array($smiley, $smileys)) {
            throw new InvalidArgumentException("Invalid smiley");
        }
        $this->smiley = $smiley;
    }

    /**
     * @return string
     */
    public function getFeedback()
    {
        return $this->feedback;
    }

    /**
     * @param string $feedback
     */
    public function setFeedback($feedback)
    {
        $this->feedback = $feedback;
    }
}
