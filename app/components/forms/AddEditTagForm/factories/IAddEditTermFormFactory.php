<?php

namespace App\Components\Forms\AddEditTagForm\Factories;

use App\Components\Forms\AddEditTagForm\AddEditTagForm;

interface IAddEditTagFormFactory
{
    /**
     * @return AddEditTagForm
     */
    public function create();
}
