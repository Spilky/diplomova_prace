<?php

namespace App\Components\Forms\ForgottenPasswordForm;

use App\Components\Forms\AbstractForm;
use App\Model\Mail\ForgottenPasswordMail;
use App\Model\Repository\UserRepository;
use App\Model\Service\TokenService;
use Exception;
use Nette\Application\UI\Form;
use Tracy\Debugger;
use Ublaboo\Mailing\MailFactory;

class ForgottenPasswordForm extends AbstractForm
{
    /** @var UserRepository */
    private $userRepository;
    /** @var TokenService */
    private $tokenService;
    /** @var MailFactory */
    private $mailFactory;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository, TokenService $tokenService, MailFactory $mailFactory)
    {
        $this->userRepository = $userRepository;
        $this->tokenService = $tokenService;
        $this->mailFactory = $mailFactory;
    }

    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form();

        $form->addText('email', 'E-mailová adresa:')
            ->setRequired('Vložte prosím vaš e-mail.');

        $form->addSubmit('submit', 'Odeslat odkaz k obnovení');

        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    /**
     * @param Form $form
     * @param $values
     */
    public function formSucceeded(Form $form, $values)
    {
        $user = $this->userRepository->getOneByParameters(array('email' => $values['email']));
        if (is_null($user)) {
            $this->onError($this, 'Účet s takovýmto e-mailem v systému neexistuje. Zkuste e-mail zadat znovu.');
            return;
        }

        try {
            $token = $this->tokenService->makeForgottenPasswordToken($user);
            $this->mailFactory->createByType(ForgottenPasswordMail::class, ['token' => $token])->send();
        } catch (Exception $e) {
            $this->onError($this, $e);
            Debugger::log($e);
            return;
        }

        $this->onSuccess($this);
    }
}
