<?php

namespace App\Components\DataGrids\MailingDataGrid\Factories;

use App\Components\DataGrids\MailingDataGrid\MailingDataGrid;

interface IMailingDataGridFactory
{
    /**
     * @return MailingDataGrid
     */
    public function create();
}
