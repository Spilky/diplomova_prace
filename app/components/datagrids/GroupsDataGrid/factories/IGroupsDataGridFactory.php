<?php

namespace App\Components\DataGrids\GroupsDataGrid\Factories;

use App\Components\DataGrids\GroupsDataGrid\GroupsDataGrid;

interface IGroupsDataGridFactory
{
    /**
     * @return GroupsDataGrid
     */
    public function create();
}
