<?php

namespace App\Components\Forms\AddEditMailTemplateForm\Factories;

use App\Components\Forms\AddEditMailTemplateForm\AddEditMailTemplateForm;

interface IAddEditMailTemplateFormFactory
{
    /**
     * @return AddEditMailTemplateForm
     */
    public function create();
}
