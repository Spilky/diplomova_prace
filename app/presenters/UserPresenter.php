<?php

namespace App\Presenters;

use App\Components\DataGrids\UsersDataGrid\Factories\IUsersDataGridFactory;
use App\Components\DataGrids\UsersDataGrid\UsersDataGrid;
use App\Components\Forms\AddEditUserForm\AddEditUserForm;
use App\Components\Forms\AddEditUserForm\Factories\IAddEditUserFormFactory;
use App\Model\Entity\User;
use App\Model\Repository\UserRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Nette\Application\BadRequestException;
use Nette\Application\ForbiddenRequestException;

class UserPresenter extends BasePresenter
{
    /** @var UserRepository @inject */
    public $userRepository;
    /** @var IUsersDataGridFactory @inject */
    public $usersDataGridFactory;
    /** @var IAddEditUserFormFactory @inject */
    public $addEditUserFormFactory;

    /**
     * @param int $id
     * @throws BadRequestException
     * @throws ForbiddenRequestException
     */
    public function actionEdit($id = 0)
    {
        $user = $this->userRepository->getById($id);
        if (is_null($user)) {
            throw new BadRequestException;
        }

        if (!$this->user->isInRole(User::ROLE_ADMIN)) {
            throw new ForbiddenRequestException;
        }

        $this['addEditUserForm']->setUser($user);
    }

    public function actionProfile()
    {
        $user = $this->userRepository->getById($this->user->id);

        $this['addEditUserForm']->setUser($user);
    }


    /**
     * @return UsersDataGrid
     */
    protected function createComponentUsersDataGrid()
    {
        $control = $this->usersDataGridFactory->create();
        return $control;
    }

    /**
     * @return AddEditUserForm
     */
    protected function createComponentAddEditUserForm()
    {
        $form = $this->addEditUserFormFactory->create();
        $form->onSuccess[] = function ($form) {
            if ($this->getAction() == 'add') {
                $this->flashMessage('Uživatel byl úspěšně vložen.', 'success');
                $this->redirect('User:');
            } else {
                $this->flashMessage('Uživatel byl úspěšně editován.', 'success');
                $this->redirect('this');
            }
        };
        $form->onError[] = function ($form, $error) {
            if ($error instanceof UniqueConstraintViolationException) {
                $this->flashMessage(
                    'Uživatel s tímto e-mailem již existuje, použijte jiný.'
                    , 'warning');
            } else {
                $this->flashMessage(
                    'Uživatele se nepodařilo ' .
                    ($this->getAction() == 'add' ?  'vložit.' : 'editovat.') .
                    ' Zkuste to prosím znovu.'
                    , 'danger');
            }
        };
        return $form;
    }
}
