<?php

namespace App\Components\Forms\AddEditProjectForm;

use App\Components\Forms\AbstractForm;
use App\Model\Entity\Project;
use App\Model\Entity\Tag;
use App\Model\Entity\User;
use App\Model\Mail\AddUserMail;
use App\Model\Mail\ProjectAssignedMail;
use App\Model\Repository\GroupRepository;
use App\Model\Repository\ProjectRepository;
use App\Model\Repository\TagRepository;
use App\Model\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use Nette\Application\UI\Form;
use Nette\Security\Passwords;
use Nette\Security\User as NetteUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Nette\Utils\Random;
use Tracy\Debugger;
use Ublaboo\Mailing\MailFactory;

class AddEditProjectForm extends AbstractForm
{
    /** @var NetteUser */
    private $netteUser;
    /** @var GroupRepository */
    private $groupRepository;
    /** @var ProjectRepository */
    private $projectRepository;
    /** @var TagRepository */
    private $tagRepository;
    /** @var UserRepository */
    private $userRepository;
    /** @var MailFactory */
    private $mailFactory;

    /** @var boolean */
    private $editing = false;

    /**
     * AddEditProjectForm constructor.
     * @param NetteUser $netteUser
     * @param GroupRepository $groupRepository
     * @param ProjectRepository $projectRepository
     * @param TagRepository $tagRepository
     * @param UserRepository $userRepository
     * @param MailFactory $mailFactory
     */
    public function __construct(
        NetteUser $netteUser,
        GroupRepository $groupRepository,
        ProjectRepository $projectRepository,
        TagRepository $tagRepository,
        UserRepository $userRepository,
        MailFactory $mailFactory
    ) {
        $this->netteUser = $netteUser;
        $this->groupRepository = $groupRepository;
        $this->projectRepository = $projectRepository;
        $this->tagRepository = $tagRepository;
        $this->userRepository = $userRepository;
        $this->mailFactory = $mailFactory;
    }

    /**
     * @param boolean $editing
     */
    public function setEditing($editing = true)
    {
        $this->editing = $editing;
    }

    public function render()
    {
        $this->template->editing = $this->editing;

        parent::render();
    }

    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form;

        if (!$this->editing) {
            $students = $this->userRepository->getStudentsBySupervisor($this->netteUser->id);
            $studentsOptions = [];
            foreach ($students as $student) {
                $studentsOptions[$student->getName() . '|' . $student->getId()] = Html::el('option')
                    ->setText($student->getName())
                    ->data('name', $student->getName())
                    ->data('email', $student->getEmail());
            }
            ksort($studentsOptions);
            $options = [];
            foreach ($studentsOptions as $key => $option) {
                $explode = explode('|', $key);
                $id = $explode[count($explode)-1];
                $options[$id] = $option;
            }
            $form->addSelect('students', 'Student', ['' => ''] + $options);

            $studentContainer = $form->addContainer('student');
            $studentContainer->addText('name', 'Jméno a příjmení:')
                ->setRequired('Vložte prosím jméno.');

            $studentContainer->addText('email', 'E-mail:')
                ->setRequired('Vložte prosím e-mail.')
                ->addRule(Form::EMAIL, 'Vložte prosím validní e-mail.');
        }

        $projectContainer = $form->addContainer('project');
        $projectContainer->addText('name', 'Název:')
            ->setRequired('Vložte prosím název.');

        $projectContainer->addTextArea('task', 'Zadání:');

        $projectContainer->addTextArea('literature', 'Doporučená literatura:');

        $tags = TagRepository::getIdIndexedArrayOfNames(
            $this->tagRepository->getByParameters(['group' => $this->netteUser->identity->group])
        );
        $projectContainer->addMultiSelect('tags', 'Tagy:', $tags);

        $projectContainer->addRadioList('type', 'Typ:', [
            Project::TYPE_BACHELOR_THESIS => 'Bakalářská práce',
            Project::TYPE_MASTER_THESIS => 'Diplomová práce',
            Project::TYPE_PROJECT => 'Projekt'
        ])
            ->setDefaultValue(Project::TYPE_BACHELOR_THESIS)
            ->setRequired('Vyberte prosím typ projektu.');

        $projectContainer->addRadioList('secretTask', 'Viditelnost:', [
            false => 'Veřejná práce - detail vidí všichni ve stejné projektové skupině',
            true => 'Tajná práce - detail vidí pouze řešitel a vedoucí'
        ])
            ->setDefaultValue(intval(false))
            ->setRequired('Vyberte prosím typ projektu.');

        $projectContainer->addRadioList('notActive', 'Aktivita:', [
            false => 'Aktivní - na projektu se pracuje',
            true => 'Neaktivní - práce na projektu již nepokračuje'
        ])
            ->setDefaultValue(intval(false))
            ->setRequired('Vyberte prosím aktivitu projektu.');

        $form->addHidden('id');

        $form->addSubmit('submit', 'Uložit');

        $form->onSuccess[] = array($this, 'formSucceeded');
        $form->onValidate[] = array($this, 'formValidate');
        return $form;
    }

    /**
     * @param Form $form
     */
    public function formValidate(Form $form)
    {
        if ($this->editing) {
            $form->getComponent('students')->cleanErrors();
            $student = $form->getComponent('student');
            $student->getComponent('name')->cleanErrors();
            $student->getComponent('email')->cleanErrors();
        }
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        if (empty($values['id'])) {
            $student = $this->userRepository->getOneByParameters(['email' => $values['student']['email']]);
            if (is_null($student)) {
                $values['student']['role'] = User::ROLE_STUDENT;
                $newStudentPassword = Random::generate();
                $values['student']['Password'] = Passwords::hash($newStudentPassword);
                $newStudent = new User($values['student']);
                $values['project']['student'] = $newStudent;
            } else {
                if ($student->getRole() == User::ROLE_UNREGISTERED) {
                    $newStudent = $student;
                    $newStudentPassword = Random::generate();
                    $newStudent->setName($values['student']['name']);
                    $newStudent->setRole(User::ROLE_STUDENT);
                    $newStudent->setPassword(Passwords::hash($newStudentPassword));
                }
                $values['project']['student'] = $student;
            }
            $values['project']['group'] = $this->groupRepository->getById($this->netteUser->identity->group);
            $project = new Project($values['project']);
        } else {
            $project = $this->projectRepository->getById($values['id']);
        }

        $tags = new ArrayCollection();
        $insertedTags = [];
        if (isset($form->getHttpData()['project']['tags'])) {
            foreach ($form->getHttpData()['project']['tags'] as $tagText) {
                $tagParts = explode('|', $tagText);
                if ($tagParts[0] < 0) {
                    unset($tagParts[0]);
                    $tag = new Tag([
                        'group' => $project->getGroup(),
                        'name' => implode('|', $tagParts)
                    ]);
                    $insertedTags[] = $tag;
                    $tags->add($tag);
                } else {
                    $tag = $this->tagRepository->getById($tagParts[0]);
                    $tags->add($tag);
                }
            }
        }

        if (empty($values['id'])) {
            $project->setTags($tags);
        } else {
            $values['project']['tags'] = $tags;
        }

        try {
            if (isset($newStudent)) {
                if ($newStudent->getId()) {
                    $this->userRepository->update($newStudent);
                } else {
                    $this->userRepository->insert($newStudent);
                }
                $this->mailFactory->createByType(AddUserMail::class, [
                    'user' => $newStudent,
                    'password' => $newStudentPassword
                    ])->send();
            }

            $this->tagRepository->insert($insertedTags);
            if (empty($values['id'])) {
                $this->projectRepository->insert($project);
                $this->mailFactory->createByType(ProjectAssignedMail::class, ['project' => $project])->send();
            } else {
                $this->projectRepository->updateWhere($values['project'], ['id' => $values['id']]);
            }
        } catch (Exception $e) {
            $this->onError($this, $e);
            Debugger::log($e);
            return;
        }

        $this->onSuccess($this);
    }
}
