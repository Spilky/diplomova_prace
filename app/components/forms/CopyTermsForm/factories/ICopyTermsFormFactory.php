<?php

namespace App\Components\Forms\AddEditProjectForm\Factories;

use App\Components\Forms\AddEditProjectForm\CopyTermsForm;

interface ICopyTermsFormFactory
{
    /**
     * @return CopyTermsForm
     */
    public function create();
}
