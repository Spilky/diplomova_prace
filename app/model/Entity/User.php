<?php

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * @ORM\Entity
 * @ORM\Table(name="`user`", options={"collate"="utf8_czech_ci"})
 */
class User extends AbstractEntity
{
    const ROLE_ADMIN = 'admin',
        ROLE_SUPERVISOR = 'supervisor',
        ROLE_STUDENT = 'student',
        ROLE_UNREGISTERED = 'unregistered';

    const TIME_LINE_SORT_ASC = 'asc',
        TIME_LINE_SORT_DESC = 'desc';

    /**
     * @ORM\ManyToOne(targetEntity="Group")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $group;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $password;

    /**
     * @ORM\Column(type="string")
     */
    protected $role;

    /**
     * @ORM\Column(type="string")
     */
    protected $timeLineSort = self::TIME_LINE_SORT_ASC;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $timeLineHidden;

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param Group $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
        if (!in_array($role, array(self::ROLE_ADMIN, self::ROLE_SUPERVISOR, self::ROLE_STUDENT, self::ROLE_UNREGISTERED))) {
            throw new InvalidArgumentException("Invalid role");
        }
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function getTimeLineSort()
    {
        return $this->timeLineSort;
    }

    /**
     * @param string $timeLineSort
     */
    public function setTimeLineSort($timeLineSort)
    {
        if (!in_array($timeLineSort, array(self::TIME_LINE_SORT_ASC, self::TIME_LINE_SORT_DESC))) {
            throw new InvalidArgumentException("Invalid time line sort");
        }
        $this->timeLineSort = $timeLineSort;
    }

    /**
     * @return string[]
     */
    public function getTimeLineHidden()
    {
        return $this->timeLineHidden;
    }

    /**
     * @param string[] $timeLineHidden
     */
    public function setTimeLineHidden($timeLineHidden)
    {
        $this->timeLineHidden = $timeLineHidden;
    }
}
