<?php

namespace App\Model\Service;

use App\Model\Entity\Token;
use App\Model\Entity\User;
use App\Model\Repository\TokenRepository;
use DateInterval;
use DateTime;
use Exception;
use Nette\Http\Request;
use Nette\Utils\Random;
use Tracy\Debugger;

class TokenService
{
    const TOKEN_DEFAULT_LENGTH = 32;
    const TOKEN_CHARACTER_LIST = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    const TOKEN_INVALID = 1;
    const TOKEN_EXPIRED = 2;
    const TOKEN_VALID = 3;

    const FORGOTTEN_PASSWORD_VALID_HOURS = 1;
    const FORGOTTEN_PASSWORD_NUMBER_USE = 1;

    const CONSULTATION_LOGIN_VALID_HOURS = 1;
    const CONSULTATION_LOGIN_NUMBER_USE = 1;

    const CONSULTATION_LOGOUT_VALID_HOURS = 1;
    const CONSULTATION_LOGOUT_NUMBER_USE = 1;

    /** @var TokenRepository */
    private $tokenRepository;
    /** @var Request */
    private $request;

    /**
     * @param TokenRepository $tokenRepository
     * @param Request $request
     */
    public function __construct(TokenRepository $tokenRepository, Request $request)
    {
        $this->tokenRepository = $tokenRepository;
        $this->request = $request;
    }

    /**
     * @param string $type type of token
     * @param User $user
     * @param int $validHours how long is token valid in HOURS, defaults to 7 days
     * @param int $numberUse how many times can be token used before id deleted, default is 1
     *
     * @return Token token string
     */
    public function makeToken($type, $user = null, $validHours = 168, $numberUse = 1)
    {
        // create entity
        $entity = new Token();

        $token = $this->getUniqueToken();
        $dateCreated = new DateTime();
        $dateValid = clone $dateCreated;
        $dateValid->add((new DateInterval("PT{$validHours}H")));

        $entity->setToken($token);
        $entity->setDateCreated($dateCreated);
        $entity->setDateValidUntil($dateValid);
        $entity->setNumberUse($numberUse);
        $entity->setIp($this->request->getRemoteAddress());
        $entity->setHostname($this->request->getRemoteHost());
        $entity->setUserAgent($this->request->getHeader('User-Agent'));
        $entity->setType($type);
        $entity->setUser($user);

        if ($user) {
            $oldToken = $this->tokenRepository->getOneByParameters(['type' => $type, 'user' => $user]);
            if (!is_null($oldToken)) {
                $this->deleteToken($oldToken);
            }
        }

        $this->tokenRepository->insert($entity);

        return $entity;
    }

    /**
     * @param User $user
     * @return Token token entity
     */
    public function makeForgottenPasswordToken($user)
    {
        return $this->makeToken(
            Token::TYPE_FORGOTTEN_PASSWORD,
            $user,
            self::FORGOTTEN_PASSWORD_VALID_HOURS,
            self::FORGOTTEN_PASSWORD_NUMBER_USE
        );
    }

    /**
     * @return Token
     */
    public function makeConsultationLoginToken()
    {
        return $this->makeToken(
            Token::TYPE_CONSULTATION_LOGIN,
            null,
            self::CONSULTATION_LOGIN_VALID_HOURS,
            self::CONSULTATION_LOGIN_NUMBER_USE
        );
    }

    /**
     * @return Token
     */
    public function makeConsultationLogoutToken()
    {
        return $this->makeToken(
            Token::TYPE_CONSULTATION_LOGOUT,
            null,
            self::CONSULTATION_LOGOUT_VALID_HOURS,
            self::CONSULTATION_LOGOUT_NUMBER_USE
        );
    }

    /**
     * @param string $token
     * @param string $type
     * @return bool is token valid?
     */
    public function checkTokenValid($token, $type)
    {
        $tokenEntity = $this->tokenRepository->getOneByParameters(['token' => $token]);

        // check token exist
        if (empty($tokenEntity)) {
            return self::TOKEN_INVALID;
        }

        if (!is_null($type)) {
            if ($tokenEntity->getType() != $type) {
                return self::TOKEN_INVALID;
            }
        }

        // check is valid and has number_use > 0
        if ($tokenEntity->getDateValidUntil() < (new DateTime()) ||
            (!is_null($tokenEntity->getNumberUse()) && $tokenEntity->getNumberUse() <= 0)) {
            return self::TOKEN_EXPIRED;
        }

        return self::TOKEN_VALID;
    }

    /**
     * @param string $token
     */
    public function useToken($token)
    {
        $tokenEntity = $this->tokenRepository->getOneByParameters(['token' => $token]);

        if ($tokenEntity->getNumberUse() == 1) {
            $this->deleteToken($tokenEntity);
        } else {
            $tokenEntity->setNumberUse($tokenEntity->getNumberUse() - 1);
            $tokenEntity->setDateUsed((new DateTime()));

            $this->tokenRepository->update($tokenEntity);
        }
    }

    /**
     * @param string $token
     * @return bool is token unique?
     */
    public function isTokenUnique($token)
    {
        return is_null($this->tokenRepository->getOneByParameters(['token' => $token]));
    }

    /**
     * @return string random token string
     */
    public function getRandomToken()
    {
        return Random::generate(self::TOKEN_DEFAULT_LENGTH, self::TOKEN_CHARACTER_LIST);
    }

    /**
     * @return string random unique token string
     */
    public function getUniqueToken()
    {
        $token = null;
        do {
            $token = $this->getRandomToken();
        } while (!$this->isTokenUnique($token));

        return $token;
    }

    /**
     * @param Token|string $token
     */
    public function deleteToken($token)
    {
        if (is_string($token)) {
            $token = $this->tokenRepository->getOneByParameters(['token' => $token]);
        }

        try {
            $this->tokenRepository->delete($token);
        } catch (Exception $e) {
            Debugger::log($e);
        }
    }
}