<?php

namespace App\Components\Forms\SetNewPasswordForm;

use App\Components\Forms\AbstractForm;
use App\Model\Entity\Token;
use App\Model\Repository\TokenRepository;
use App\Model\Repository\UserRepository;
use App\Model\Service\TokenService;
use Exception;
use Nette\Application\UI\Form;
use Nette\Security\Passwords;
use Tracy\Debugger;

class SetNewPasswordForm extends AbstractForm
{
    /** @var UserRepository */
    private $userRepository;
    /** @var TokenRepository */
    private $tokenRepository;
    /** @var TokenService */
    private $tokenService;

    /**
     * @param UserRepository $userRepository
     * @param TokenRepository $tokenRepository
     * @param TokenService $tokenService
     */
    public function __construct(
        UserRepository $userRepository,
        TokenRepository $tokenRepository,
        TokenService $tokenService
    ) {
        $this->userRepository = $userRepository;
        $this->tokenRepository = $tokenRepository;
        $this->tokenService = $tokenService;
    }

    /**
     * @return Form
     */
    public function createComponentForm()
    {
        $form = new Form();

        $form->addPassword('newPassword', 'Zadejte nové heslo')
            ->addRule(Form::MIN_LENGTH, 'Heslo musí být minimálně 6 znaků dlouhé.', 6)
            ->setRequired('Vložte prosím nové heslo.');

        $form->addPassword('newPasswordAgain', 'Zadejte nové heslo ještě jednou pro kontrolu')
            ->setRequired('Vložte prosím nové heslo ještě jednou pro kontrolu.')
            ->addRule(Form::EQUAL, 'Nová hesla se neshodují, vložte prosím stejná hesla.', $form['newPassword']);

        $form->addHidden('token');

        $form->addSubmit('submit', 'Změnit heslo');

        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    /**
     * @param Form $form
     * @param $values
     */
    public function formSucceeded(Form $form, $values)
    {
        $status = $this->tokenService->checkTokenValid(
            $values['token'],
            Token::TYPE_FORGOTTEN_PASSWORD
        );

        if ($status != TokenService::TOKEN_VALID) {
            $this->onError($this, 'Odkaz pro obnovu hesla je neplatný, vygenerute si prosím nový.');
            return;
        }

        $token = $this->tokenRepository->getOneByParameters(array('token' => $values['token']));
        $user = $token->getUser();
        $user->setPassword(Passwords::hash($values['newPassword']));

        try {
            $this->userRepository->update($user);
            $this->tokenService->useToken($values['token']);
        } catch (Exception $e) {
            $this->onError($this, $e);
            Debugger::log($e);
            return;
        }

        $this->onSuccess($this);
    }
}
